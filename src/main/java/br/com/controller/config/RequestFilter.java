/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.controller.config;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;

/**
 * <pre>
 *<b>author:</b> Luan
 *<b>date:  </b> 09/03/2015
 * </pre>
 *
 * <p>
 * Classe responsável por interceptar as requisições feitas aos web services
 * restfull. Somente é chamada se adcionada no AplicattionConfig do sistema,
 * módulo.
 */
public class RequestFilter implements ContainerRequestFilter {

    /**
     * <PRE>
     *<b>author:</b>  Luan
     *<b>date  :</b>  09/03/2015
     *<b>param :</b>  ContainerRequestContext
     *<b>return :</b>
     * </PRE>
     *
     * <p>
     * Trata as requisições do vinda do Html5 adicionando em uma session list os
     * usuarios e em qual banco ele está se conectando. Desenvolvido para
     * funcionar o Rhweb corporativo.
     *
     * @param crc
     * @throws java.io.IOException
     */
    @Override
    public void filter(ContainerRequestContext crc) throws IOException {

        if (crc != null
                && crc.getCookies() != null
                && !crc.getCookies().isEmpty()
                && crc.getCookies().get("JSESSIONID") != null) {
            //Recupera a session vinda na request.
            String SESSIONID = crc.getCookies().get("JSESSIONID").getValue();
        }
    }

}
