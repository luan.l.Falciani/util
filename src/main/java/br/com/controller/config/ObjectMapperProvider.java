/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.controller.config;

import javax.ws.rs.ext.ContextResolver;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 *
 * @author age20
 */
public class ObjectMapperProvider implements ContextResolver<ObjectMapper> {

    final ObjectMapper defaultObjectMapper;

    public ObjectMapperProvider() {
        defaultObjectMapper = createDefaultMapper();
    }

    @Override
    public ObjectMapper getContext(Class<?> type) {
        return defaultObjectMapper;
    }

    private static ObjectMapper createDefaultMapper() {
        final ObjectMapper result = new ObjectMapper();
        //result.configure(Feature.INDENT_OUTPUT, true);
        result.getSerializationConfig().setSerializationInclusion(Inclusion.NON_NULL);

        result.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);
        result.disable(SerializationConfig.Feature.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS);
        result.enable(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        return result;
    }

}
