package br.com.util;

import br.com.entitybean.Auditoria;
import org.hibernate.property.Setter;

import java.util.Date;

public class AuditoriaUtil {

    /**
     * Adiciona os valores de auditoria para inclusão da entidade
     *
     * @param obj
     * @param usuario
     * @throws Exception 
     */
    public static void inclusao(Object obj, Object usuario) throws Exception {

        String user = "Usuário Anônimo";
        if (usuario != null) {
            user = Util.gerarUsuarioAuditoria(usuario);
        }

        Date now = Util.getCurrentDateBrasil();

        Setter setter = Util.getSetter(obj, Auditoria.DATA_HORA_USUARIO_INC);
        setter.set(obj, now, null);
        setter = Util.getSetter(obj, Auditoria.USUARIO_INC);
        setter.set(obj, user, null);

        setter = Util.getSetter(obj, Auditoria.DATA_HORA_USUARIO_ALT);
        setter.set(obj, now, null);
        setter = Util.getSetter(obj, Auditoria.USUARIO_ALT);
        setter.set(obj, user, null);

        setter = Util.getSetter(obj, Auditoria.IS_EXCLUIDO);
        setter.set(obj, Constantes.NAO_ABREVIADO, null);
    }

    /**
     * Adiciona os valores de auditoria para alteração da entidade
     *
     * @param obj
     * @param usuario
     * @throws Exception 
     */
    public static void alteracao(Object obj, Object usuario) throws Exception {

        String user = "Usuário Anônimo";
        if (usuario != null) {
            user = Util.gerarUsuarioAuditoria(usuario);
        }

        Date now = Util.getCurrentDateBrasil();
        Setter setter = Util.getSetter(obj, Auditoria.DATA_HORA_USUARIO_ALT);
        setter.set(obj, now, null);

        setter = Util.getSetter(obj, Auditoria.USUARIO_ALT);
        setter.set(obj, user, null);
    }

    /**
     * Adiciona os valores de auditoria para exclusão da entidade
     *
     * @param obj
     * @param usuario
     * @throws Exception 
     */
    public static void exclusao(Object obj, Object usuario) throws Exception {

        String user = "Usuário Anônimo";
        if (usuario != null) {
            user = Util.gerarUsuarioAuditoria(usuario);
        }

        Date now = Util.getCurrentDateBrasil();

        Setter setter = Util.getSetter(obj, Auditoria.DATA_HORA_USUARIO_DEL);
        setter.set(obj, now, null);
        setter = Util.getSetter(obj, Auditoria.USUARIO_DEL);
        setter.set(obj, user, null);
        setter = Util.getSetter(obj, Auditoria.IS_EXCLUIDO);
        setter.set(obj, Constantes.SIM_ABREVIADO, null);
    }
}
