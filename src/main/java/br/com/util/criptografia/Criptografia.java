package br.com.util.criptografia;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.*;
import javax.crypto.spec.*;

public final class Criptografia
{
   private static final String passfrase="luan&Cript";

   /**
    * Criptografa o parâmetro recebido utilizando a técnica Blowfish.
    * @param str
    * @return 
    */
   public static final String cript(String str)
   {
      String strCript = str;

      try
      {
         Cipher ch = Cipher.getInstance("Blowfish");
         SecretKey k1 = new SecretKeySpec(passfrase.getBytes(), "Blowfish");

         //criptografando
         ch.init( Cipher.ENCRYPT_MODE, k1);
         byte b[] = ch.doFinal(strCript.getBytes());
         String s1 = Conversion.byteArrayToBase64String(b);
         strCript = s1;
      }
      catch( NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e)
      {
         System.out.println(e.getMessage());
         e.printStackTrace();
      }

      //Remove o "=" por que esta sobrando.
      strCript=strCript.replaceAll("\\=", "");
      return strCript;
   }   

   /**
    * Descriptografa o parâmetro recebido utilizando a técnica Blowfish.
    * @param str
    * @return 
    */
   public static final String decript(String str)
   {
      String strDecript = str;

      try
      {
         Cipher ch = Cipher.getInstance("Blowfish");
         SecretKey k1 = new SecretKeySpec(passfrase.getBytes(), "Blowfish");

         //decriptografando
         ch.init( Cipher.DECRYPT_MODE, k1);
         byte b[] = Conversion.base64StringToByteArray(strDecript);
         byte b1[] = ch.doFinal(b);
         String s1 = new String(b1);
         strDecript = s1;
      }
      catch( NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | NumberFormatException | IllegalBlockSizeException | BadPaddingException e)
      {
         System.out.println(e.getMessage());
         e.printStackTrace();
      }
      return strDecript;
   }
   
    /**
     * 
     * <p>Criptografa em MD5 a String passada como parametro, retornando 
    *     a string já criptografada.
     * @param str
     * @return 
     */
   public static String criptMD5(String str)
   {
      String strCript = str;

      try  {  
          if(strCript != null && !strCript.equals("")){
             MessageDigest md = MessageDigest.getInstance( "MD5" );  
           
             md.update( strCript.getBytes() );  
             byte[] hash = md.digest();   
             StringBuilder hexString = new StringBuilder();   
             for (int i = 0; i < hash.length; i++) {   
                if ((0xff & hash[i]) < 0x10)   
                    hexString.append("0").append(   
                      Integer.toHexString((0xFF & hash[i])));   
                else   
                   hexString.append(Integer.toHexString(0xFF & hash[i]));   
             }   
             strCript = hexString.toString();
          }
      }catch(NoSuchAlgorithmException e)  {  
         e.printStackTrace();  
      } 
      
      return strCript;
   }
}
