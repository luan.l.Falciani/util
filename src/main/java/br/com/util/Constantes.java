/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.util;

/**
 *
 * @author nextage
 */
public class Constantes {

    //Constantes utilizadas no nextageUtil
    public static final String NOME_PROJETO = "UTIL";
    public static final String MENSAGENS_UNDERLINE = "mensagens_";
    public static final int NO_LIMIT = -1;
    public static final String SIM_ABREVIADO = "S";
    public static final String NAO_ABREVIADO = "N";
    
    //UTILIZADO PARA A SESSION
    public static String USUARIOLOGADO = "UsuarioLogado";
    public static String USUARIOLOGADO_FLEX = "usuarioLogadoFlex";
    public static String USERNAME = "userName";
    public static String LOCALE = "locale";
    public static String ORIGIN = "origin";
    public static String REFERER = "referer";
    public static String HOST = "host";
    //campos
    //tipo de campos
    public static final String INTEGER = "Integer";
    public static final String STRING = "String";
    public static final String FLOAT = "Float";
    public static final String DOUBLE = "Double";
    public static final String DATE = "Date";
    public static final String OBJECT = "Object";
    public static final String BOOLEAN = "Boolean";
    //modo exibicao
    //tipo campo Object
    public static final String COMBO = "COMBO";
    public static final String COMBOAUX = "COMBOAUX";
    public static final String COMBOSIMNAO = "COMBOSIMNAO";
    public static final String AUTOCOMPLETE = "AUTOCOMPLETE";
    public static final String AUTOCOMPLETEPESSOA = "AUTOCOMPLETEPESSOA";
    public static final String TEXTAREA = "TEXTAREA";
    public static final String COMBOFLUXO = "COMBOFLUXO";
    public static final String AUTOCOMPLETEVAGA = "AUTOCOMPLETEVAGA";
    public static final String AUTOCOMPLETECODIGO = "AUTOCOMPLETECODIGO";
    public static final String COMBOCODIGO = "COMBOCODIGO";
    public static final String COMBOSTATUS = "COMBOSTATUS";
    //tipo campo Boolean
    public static final String NORMAL = "NORMAL";
    public static final String CHECKLIST = "CHECKLIST";
    //tipo Exibicao
    public static final String SIGlA_LABEL = "L";
    public static final String SIGlA_CAMPO = "C";
    //TIPO EXIBICAO
    public static final String MANUTENCAO = "M";
    public static final String AMBOS = "A";

    public static final String GROUP_LABEL_FUNCTION_SEXO = "labelFuncSexo";
    
     public static final String REGS_SALVOS_SUCESSO_I18N = "msg_registros_salvo_sucesso";
    public static final String REG_EXCLUSAO_ERRO_I18N = "msg_registro_exclusao_erro";
    public static final String REG_SALVO_ERRO_I18N = "msg_registro_salvo_erro";
    public static final String REG_EXCLUSAO_SUCESSO_I18N = "msg_registro_exclusao_sucesso";
    public static final String REG_SALVO_SUCESSO_I18N = "msg_registro_salvo_sucesso";
    public static final String SUCESSO_OPERACAO_COM_EMAIL_I18N = "msg_operacao_realizada_sucesso_com_email";
    public static final String SUCESSO_OPERACAO_SEM_EMAIL_I18N = "msg_operacao_realizada_sucesso_sem_email";
    public static final String MSG_CHAVE_EXPIRADA_I18N = "msg_licença_expirada";
    public static final String MSG_USUARIO_SENHA_INVALIDOS_I18N = "msg_usuario_senha_invalidos";
    public static final String ERRO_OPERACAO_I18N = "msg_erro_realizar_operacao";
    public static final String SUCESSO_OPERACAO_I18N = "msg_operacao_realizada_sucesso";
    public static final String MSG_REQUISICAO_CANCELADA_SUCESSO_I18N = "msg_requisicao_cancelada_sucesso";
    public static final String MSG_REQUISICAO_CANCELADA_ERRO_APROVADA_I18N = "msg_requisicao_cancelada_erro_aprovada";
    public static final String MSG_CRACHA_JA_EXISTE_I18N = "msg_cracha_ja_existe";
    public static final String MSG_CRACHA_JA_EXISTE_VISITANTE_I18N = "msg_cracha_ja_existe_visitante";
    public static final String MSG_DOC_JA_CADASTRADO_I18N = "label_cpf_ja_cadastrado";
    public static final String MSG_CADASTRAR_APROVADORES_I18N = "msg_favor_cadastrar_aprovadores";
}
