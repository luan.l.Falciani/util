package br.com.util.impressao;

import com.sun.pdfview.PDFFile;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;

/**
 * <p>Classe de serviço responsável por imprimir na impressora um documento 
 *      pre-gerado em PDF.
 */
public class PrintPdfService{

    private PrinterJob pjob = null;
    private PrintService impressora = null;

    public PrintPdfService(InputStream inputStream, String jobName) throws IOException, PrinterException {
        byte[] pdfContent = new byte[inputStream.available()];
        inputStream.read(pdfContent, 0, inputStream.available());
        initialize(pdfContent, jobName);
    }

    public PrintPdfService(byte[] content, String jobName) throws IOException, PrinterException {
        initialize(content, jobName);
    }

    /**
     * <p>Iinicializa Job de impressão do PDF para a impressora.
     */
    private void initialize(byte[] pdfContent, String jobName) throws IOException, PrinterException {
        ByteBuffer bb = ByteBuffer.wrap(pdfContent);

        PDFFile pdfFile = new PDFFile(bb);
        PrintPdfPage pages = new PrintPdfPage(pdfFile);

        impressora = PrintServiceLookup.lookupDefaultPrintService();
        
        if (impressora != null) {
            pjob = PrinterJob.getPrinterJob();
            pjob.setPrintService(impressora);

            PageFormat pf = PrinterJob.getPrinterJob().defaultPage();

            pjob.setJobName(jobName);
            Book book = new Book();
            book.append(pages, pf, pdfFile.getNumPages());
            pjob.setPageable(book);

            Paper paper = new Paper();

            paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());
            pf.setPaper(paper);
        }
    }

    /**
     * <p>Imprime o PDF passado por parametro em byte array diretamente 
     *      a impressora padrão.
     * @throws java.awt.print.PrinterException
     */
    public void print() throws PrinterException {
        if (impressora != null) {
            System.out.println("IMPRIMINDO....");
            pjob.print();
            System.out.println("IMPRIMIU");
        }

    }
}