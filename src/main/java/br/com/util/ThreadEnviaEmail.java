package br.com.util;

import java.util.List;
import javax.mail.MessagingException;

public class ThreadEnviaEmail extends Thread{
    
    private String enderecosEmail ;
    private String tituloEmail ;
    private String mensagemEmail;
    private List<AnexoEmail> anexos;
    private String emailHost;
    private String emailPorta; 
    private String emailOrigem;
    private String emailUser;
    private String emailPasswd;
    private String emailChave;
    private String emailHtml;
    private String enderecosEmailCco;
    private int timeSleep = 10000;
    private int repeatSendEmail = 20;
    
        /**
     * Método que realiza a chamada de envia de email de acordo com a chave 
     * escolhida para o mesmo na configuração, sendo autenticado ou sem autenticação.
     * 
     * Caso Ocorra alguma erro ao enviar email a Thread irá tentar enviar o email novamente,
     * esse número de vezes pode ser configurado assim como o tempo de espera entre uma tentativa e outra.
     * 
     *</PRE>
     */
    @Override
     public void run() {
         
        Boolean enviado = false;
        int cont = 0;

         while (!enviado && cont < repeatSendEmail) {     
            cont++;
            try{
                if (enderecosEmail.length() > 0) {
                        
                    if(emailHtml == null){
                        Email.enviaEmail(enderecosEmail, tituloEmail, mensagemEmail, anexos, emailHost,
                                    emailPorta, emailOrigem, emailUser, emailPasswd, emailChave);
                    }else{
                        Email.enviaEmail(enderecosEmail,enderecosEmailCco, tituloEmail, mensagemEmail, anexos, emailHost,
                                    emailPorta, emailOrigem, emailUser, emailPasswd, emailChave,emailHtml);
                    }                    
                    
                    enviado = true;    
                }
            }catch(MessagingException ex){
                ex.printStackTrace();
                try{
                    //Espera por 10 segundos para tentar enviar o email novamente
                    ThreadEnviaEmail.sleep(timeSleep);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
        
     }

    public String getEnderecosEmail() {
        return enderecosEmail;
    }

    public void setEnderecosEmail(String enderecosEmail) {
        this.enderecosEmail = enderecosEmail;
    }

    public String getTituloEmail() {
        return tituloEmail;
    }

    public void setTituloEmail(String tituloEmail) {
        this.tituloEmail = tituloEmail;
    }

    public String getMensagemEmail() {
        return mensagemEmail;
    }

    public void setMensagemEmail(String mensagemEmail) {
        this.mensagemEmail = mensagemEmail;
    }

    public List<AnexoEmail> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<AnexoEmail> anexos) {
        this.anexos = anexos;
    }

    public String getEmailHost() {
        return emailHost;
    }

    public void setEmailHost(String emailHost) {
        this.emailHost = emailHost;
    }

    public String getEmailPorta() {
        return emailPorta;
    }

    public void setEmailPorta(String emailPorta) {
        this.emailPorta = emailPorta;
    }

    public String getEmailOrigem() {
        return emailOrigem;
    }

    public void setEmailOrigem(String emailOrigem) {
        this.emailOrigem = emailOrigem;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public String getEmailPasswd() {
        return emailPasswd;
    }

    public void setEmailPasswd(String emailPasswd) {
        this.emailPasswd = emailPasswd;
    }

    public String getEmailChave() {
        return emailChave;
    }

    public void setEmailChave(String emailChave) {
        this.emailChave = emailChave;
    }

    public int getTimeSleep() {
        return timeSleep;
    }

    public void setTimeSleep(int timeSleep) {
        this.timeSleep = timeSleep;
    }

    public int getRepeatSendEmail() {
        return repeatSendEmail;
    }

    public void setRepeatSendEmail(int repeatSendEmail) {
        this.repeatSendEmail = repeatSendEmail;
    }

    public String getEmailHtml() {
        return emailHtml;
    }

    public void setEmailHtml(String emailHtml) {
        this.emailHtml = emailHtml;
    }

    public String getEnderecosEmailCco() {
        return enderecosEmailCco;
    }

    public void setEnderecosEmailCco(String enderecosEmailCco) {
        this.enderecosEmailCco = enderecosEmailCco;
    }
    
}
