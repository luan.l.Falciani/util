package br.com.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RelatorioBean<T> {

    public enum TipoRelatorio {

        PDF, XLS, XLSX, CSV, WORD
    };
    private String titulo;
    private String nome;
    private String caminhoJasper;
    private String assinaturas;
    private Object item;
    private Boolean temTotal = false;
    private byte[] logomarca;
    private TipoRelatorio tipo;
    private List<RelatorioBean> listaRelatorioBeanItens;
    private List<T> itens;
    private Map<String, String> mapaFiltro;
    private Map<String, String> mapaListaItens;
    private Map<String, Boolean> mapaTemTotal;
    private Map<String, String> mapaFormatadores;
    private HashMap parameters;

    /**
     * Lista utilizda para setar beans à serem utilizados ao gerar um relatório
     * em PDF através do Jasper sem a necessidade de uma conexão com o banco de
     * dados. [Marlos Morbis Novo]
     */
    private Object listaJasperBeanCollection;

    /**
     * Padrão dd/MM/aaaa
     */
    private String dateFormat;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCaminhoJasper() {
        return caminhoJasper;
    }

    public void setCaminhoJasper(String caminhoJasper) {
        this.caminhoJasper = caminhoJasper;
    }

    public String getAssinaturas() {
        return assinaturas;
    }

    public void setAssinaturas(String assinaturas) {
        this.assinaturas = assinaturas;
    }

    public Object getItem() {
        return item;
    }

    public void setItem(Object item) {
        this.item = item;
    }

    public Boolean getTemTotal() {
        return temTotal;
    }

    public void setTemTotal(Boolean temTotal) {
        this.temTotal = temTotal;
    }

    public byte[] getLogomarca() {
        return logomarca;
    }

    public void setLogomarca(byte[] logomarca) {
        this.logomarca = logomarca;
    }

    public TipoRelatorio getTipo() {
        return tipo;
    }

    public void setTipo(TipoRelatorio tipo) {
        this.tipo = tipo;
    }

    public List<RelatorioBean> getListaRelatorioBeanItens() {
        return listaRelatorioBeanItens;
    }

    public void setListaRelatorioBeanItens(List<RelatorioBean> listaRelatorioBeanItens) {
        this.listaRelatorioBeanItens = listaRelatorioBeanItens;
    }

    public List<T> getItens() {
        return itens;
    }

    public void setItens(List<T> itens) {
        this.itens = itens;
    }

    public Map<String, String> getMapaFiltro() {
        return mapaFiltro;
    }

    public void setMapaFiltro(Map<String, String> mapaFiltro) {
        this.mapaFiltro = mapaFiltro;
    }

    public Map<String, String> getMapaListaItens() {
        return mapaListaItens;
    }

    public void setMapaListaItens(Map<String, String> mapaListaItens) {
        this.mapaListaItens = mapaListaItens;
    }

    public Map<String, Boolean> getMapaTemTotal() {
        return mapaTemTotal;
    }

    public void setMapaTemTotal(Map<String, Boolean> mapaTemTotal) {
        this.mapaTemTotal = mapaTemTotal;
    }

    public Map<String, String> getMapaFormatadores() {
        return mapaFormatadores;
    }

    public void setMapaFormatadores(Map<String, String> mapaFormatadores) {
        this.mapaFormatadores = mapaFormatadores;
    }

    public HashMap getParameters() {
        return parameters;
    }

    public void setParameters(HashMap parameters) {
        this.parameters = parameters;
    }

    public Object getListaJasperBeanCollection() {
        return listaJasperBeanCollection;
    }

    public void setListaJasperBeanCollection(Object listaJasperBeanCollection) {
        this.listaJasperBeanCollection = listaJasperBeanCollection;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }
}
