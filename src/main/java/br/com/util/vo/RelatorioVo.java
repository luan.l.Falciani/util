package br.com.util.vo;

import jxl.format.Colour;

public class RelatorioVo {

    public static final String ARIAL = "ARIAL";
    public static final String TIMES = "TIMES";
    public static final String COURIER = "COURIER";
    ;
    public static final String TAHOMA = "TAHOMA";

    private int fontSize = 0;
    private String fontName = null;
    private boolean bold;
    private boolean italic;
    private Colour fontColor;

    private boolean aplicar;

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        if (fontSize > 0) {
            aplicar = true;
        }
        this.fontSize = fontSize;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        if (fontName != null) {
            aplicar = true;
        }
        this.fontName = fontName;
    }

    public Colour getFontColor() {
        return fontColor;
    }

    public void setFontColor(Colour fontColor) {
        this.fontColor = fontColor;
    }

    public boolean getBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        if (bold) {
            aplicar = true;
        }
        this.bold = bold;
    }

    public boolean getItalic() {
        return italic;
    }

    public void setItalic(boolean italic) {
        if (italic) {
            aplicar = true;
        }
        this.italic = italic;
    }

    public boolean getAplicar() {
        return aplicar;
    }

    public void setAplicar(boolean aplicar) {
        this.aplicar = aplicar;
    }

}
