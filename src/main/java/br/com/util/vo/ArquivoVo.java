package br.com.util.vo;

/**
 *
 * @author age20
 */
public class ArquivoVo {
    private byte[] arquivo;
    private String NmAnexo;

    public ArquivoVo() {
    }

    public byte[] getArquivo() {
        return arquivo;
    }

    public void setArquivo(byte[] arquivo) {
        this.arquivo = arquivo;
    }

    public String getNmAnexo() {
        return NmAnexo;
    }

    public void setNmAnexo(String NmAnexo) {
        this.NmAnexo = NmAnexo;
    }
    
}
