package br.com.util.vo;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

public class PeriodoVo {

    private Date de;
    private Date ate;
    private String stDe;
    private String stAte;

    public PeriodoVo(Date de, Date ate) {
        this.de = configuraDataDe(de);
        this.ate = configuraDataAte(ate);
    }

    public PeriodoVo() {
    }

    public Date getDe() {
        return de;
    }

    public void setDe(Date de) {
        this.de = configuraDataDe(de);
    }

    public Date getAte() {
        return ate;
    }

    public void setAte(Date ate) {
        this.ate = configuraDataAte(ate);
    }

    public String getStDe() {
        return stDe;
    }

    public void setStDe(String stDe) {
        this.stDe = stDe;
    }

    public String getStAte() {
        return stAte;
    }

    public void setStAte(String stAte) {
        this.stAte = stAte;
    }

    /**
     * Verifica se o periodo foi preenchido corretamente
     *
     * @return verdadeiro caso de e até estejam preenchidos.
     */
    public boolean preenchido() {
        return getDe() != null && getAte() != null;
    }

    /**
     * Verifica se o periodo não foi preenchido corretamente
     *
     * @return verdadeiro caso algum das datas não tenham sido preenchidas.
     */
    public boolean naoPreenchido() {
        return getDe() == null || getAte() == null;
    }

    /**
     * Verifica se uma das datas foi preenchida
     *
     * @return verdadeiro caso de ou até foi preenchido.
     */
    public boolean algumaPreenchida() {
        return getDe() != null || getAte() != null;
    }

    /**
     * Entre as datas
     *
     * @param data
     * @return
     */
    public boolean entreDatas(Date data) {
        return data.compareTo(getDe()) >= 0 && data.compareTo(getAte()) <= 0;
    }

    public static Date configuraDataDe(Date data) {
        if (data != null) {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(data);

            /**
             * Para setar uma hora, deve-se utilizar Calendar.HOUR_OF_DAY. Caso
             * utilize Calendar.HOUR, a hora não será substituída, mas sim,
             * somada a hora atual
             */
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            return c.getTime();
        }
        return null;
    }

    public static Date configuraDataAte(Date data) {
        if (data != null) {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(data);
            /**
             * Para setar uma hora, deve-se utilizar Calendar.HOUR_OF_DAY. Caso
             * utilize Calendar.HOUR, a hora não será substituída, mas sim,
             * somada a hora atual
             */
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 0);
            return c.getTime();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Periodo " + "De: " + de + ", Ate: " + ate + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.de);
        hash = 71 * hash + Objects.hashCode(this.ate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PeriodoVo other = (PeriodoVo) obj;
        if (!Objects.equals(this.de, other.de)) {
            return false;
        }
        return Objects.equals(this.ate, other.ate);
    }
}
