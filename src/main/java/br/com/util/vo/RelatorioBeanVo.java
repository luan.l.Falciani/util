package br.com.util.vo;

import br.com.util.RelatorioBean;

public class RelatorioBeanVo {

    private RelatorioBean relatorioBean;
    private String id;

    public RelatorioBean getRelatorioBean() {
        return relatorioBean;
    }

    public void setRelatorioBean(RelatorioBean relatorioBean) {
        this.relatorioBean = relatorioBean;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
