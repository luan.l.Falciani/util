package br.com.util.vo.grafico;

import java.util.List;


public class ListaGraficoVo<T> {
    
    private List<T> lista;
    private List<DataGraficoVo> grafico; 

    public List<T> getLista() {
        return lista;
    }

    public void setLista(List<T> lista) {
        this.lista = lista;
    }

    public List<DataGraficoVo> getGrafico() {
        return grafico;
    }

    public void setGrafico(List<DataGraficoVo> grafico) {
        this.grafico = grafico;
    }
}
