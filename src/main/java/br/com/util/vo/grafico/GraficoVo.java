package br.com.util.vo.grafico;

import java.util.ArrayList;
import java.util.List;

public class GraficoVo<T> {

    private List<Object> labels;
    private List<DataGraficoVo> datasets;
    private String tipo;
    private Boolean noResult;
    private List<T> itens;

    public GraficoVo() {
        labels = new ArrayList<>();
        datasets = new ArrayList<>();
        itens = new ArrayList<>();

    }

    public List<Object> getLabels() {
        return labels;
    }

    public void setLabels(List<Object> labels) {
        this.labels = labels;
    }

    public List<DataGraficoVo> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<DataGraficoVo> datasets) {
        this.datasets = datasets;
    }

    public List<T> getItens() {
        return itens;
    }

    public void setItens(List<T> itens) {
        this.itens = itens;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Boolean getNoResult() {
        return noResult;
    }

    public void setNoResult(Boolean noResult) {
        this.noResult = noResult;
    }
}
