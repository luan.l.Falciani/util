package br.com.util.vo.grafico;

import java.util.List;


public class DataGraficoVo {
    
    private String label;
    private String fillColor;
    private String strokeColor;
    private String highlightFill;
    private String highlightStroke;    
    private List<?> data;
    
    //ListaGraficoVo
    private double value;
    private String color;
    private String highlight;
    private String labelColor;
    private String labelFontSize;
    private String labelAlign;

    public DataGraficoVo() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    public String getStrokeColor() {
        return strokeColor;
    }

    public void setStrokeColor(String strokeColor) {
        this.strokeColor = strokeColor;
    }

    public String getHighlightFill() {
        return highlightFill;
    }

    public void setHighlightFill(String highlightFill) {
        this.highlightFill = highlightFill;
    }

    public String getHighlightStroke() {
        return highlightStroke;
    }

    public void setHighlightStroke(String highlightStroke) {
        this.highlightStroke = highlightStroke;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public String getLabelColor() {
        return labelColor;
    }

    public void setLabelColor(String labelColor) {
        this.labelColor = labelColor;
    }

    public String getLabelFontSize() {
        return labelFontSize;
    }

    public void setLabelFontSize(String labelFontSize) {
        this.labelFontSize = labelFontSize;
    }

    public String getLabelAlign() {
        return labelAlign;
    }

    public void setLabelAlign(String labelAlign) {
        this.labelAlign = labelAlign;
    }

}
