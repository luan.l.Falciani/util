package br.com.util.vo.grafico;

import java.util.List;


public class FiltroGraficoVo<T> {
    
    private List<T> itens;
    private String label;
    private String valor;
    private String groupColunas;
    private String groupLabel;
    private TypeGroupLabel typeGroupLabel;
    
    public enum TypeGroupLabel{
        MES,
        ANO
    }

    public FiltroGraficoVo() {
    }

    public FiltroGraficoVo(List<T> itens, String label, String valor) {
        this.itens = itens;
        this.label = label;
        this.valor = valor;
    }

    public FiltroGraficoVo(List<T> itens, String label, String valor, String groupColunas) {
        this.itens = itens;
        this.label = label;
        this.valor = valor;
        this.groupColunas = groupColunas;
    }

    public List<T> getItens() {
        return itens;
    }

    public void setItens(List<T> itens) {
        this.itens = itens;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getGroupColunas() {
        return groupColunas;
    }

    public void setGroupColunas(String groupColunas) {
        this.groupColunas = groupColunas;
    }

    public String getGroupLabel() {
        return groupLabel;
    }

    public void setGroupLabel(String groupLabel) {
        this.groupLabel = groupLabel;
    }

    public TypeGroupLabel getTypeGroupLabel() {
        return typeGroupLabel;
    }

    public void setTypeGroupLabel(TypeGroupLabel typeGroupLabel) {
        this.typeGroupLabel = typeGroupLabel;
    }
       
}
