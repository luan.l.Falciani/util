package br.com.util.servlet;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 *
 * Classe responsável por interceptar o inicio da aplicação e disponibilizar o
 * ServletContext para a Aplicação.
 *
 */
public class ApplicationContextFilter implements Filter {

    private static final String EMPRESA_NO_WEBXML = "empresa";
    private static final String EMPRESA_NO_TOMCAT = "Empresa";

    private static String empresa;
    private static FilterConfig config;
    private static ServletContext servletContext;

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        try {
            config = filterConfig;
            servletContext = filterConfig.getServletContext();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Retorna a empresa(cliente) do sistema.
     *
     * Primeiro Verifica se tem a variável -DEmpresa setada no WEB.XML, caso a
     * mesma não Exista ou esteja nula busca a informação no TOMCAT.
     *
     * @return
     */
    public static String getEmpresa() {

        if (empresa == null || "".equals(empresa)) {
            try {
                empresa = config.getInitParameter(EMPRESA_NO_WEBXML);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (empresa == null) {
            try {
                empresa = System.getProperty(EMPRESA_NO_TOMCAT);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return empresa;
    }

    /**
     * Retorna o Contexto da aplicação.
     *
     * @return
     */
    public static ServletContext getServletContext() {
        return servletContext;
    }
}
