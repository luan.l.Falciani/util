package br.com.util.adapter;

import java.lang.reflect.Field;

public class MappedProperty {

    private Field property;
    private MappedPropertyAdapter valueAdapter;
    private Boolean whenNullDiscardRecord;
    
    public MappedProperty(Field property) {
        this.property = property;
        this.whenNullDiscardRecord = Boolean.FALSE;
    }

    public MappedProperty(Field property, MappedPropertyAdapter adapter) {
        this.property = property;
        this.valueAdapter = adapter;
        this.whenNullDiscardRecord = Boolean.FALSE;
    }

    public MappedProperty() {
        this.whenNullDiscardRecord = Boolean.FALSE;
    }

    public Field getProperty() {
        return property;
    }

    public void setProperty(Field property) {
        this.property = property;
    }

    public MappedPropertyAdapter getValueAdapter() {
        return valueAdapter;
    }

    public void setValueAdapter(MappedPropertyAdapter valueAdapter) {
        this.valueAdapter = valueAdapter;
    }

    public Boolean getWhenNullDiscardRecord() {
        return whenNullDiscardRecord;
    }

    public void setWhenNullDiscardRecord(Boolean whenNullDiscardRecord) {
        this.whenNullDiscardRecord = whenNullDiscardRecord;
    }

}
