package br.com.util.adapter;

import org.apache.poi.ss.usermodel.DataFormatter;

public class EntityIdAdapter implements MappedPropertyAdapter {

    private final DataFormatter formatter;

    public EntityIdAdapter() {
        formatter = new DataFormatter();
    }

    @Override
    public Object adapt(Object value) throws Exception {
        Long id = null;
        if (value instanceof Double) {
            String dStr = formatter.formatRawCellContents(((Double) value).doubleValue(), -1, "#");
            try {
                id = Long.parseLong(dStr);
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        } else if (value instanceof Integer) {
            id = (Long) value;
        } else if (value instanceof String) {
            id = Long.parseLong((String) value);
        }

        return id;
    }
}
