package br.com.util.adapter;

import java.util.Set;
import java.util.SortedMap;

public class WorkbookDataAdapterConfiguration<T> {

    public static final int ALL_COLUMNS = 0;
    public static final int ALL_ROWS = 0;
    private Integer columns;
    private Integer rows;
    private Set<WorkbookRange> rangeSet;
    private Boolean setEmptyFieldsNull;
    private Boolean skipFirstLine;
    private SortedMap<Integer, MappedProperty> targetFieldMap;
    private Class<T> targetType;

    public WorkbookDataAdapterConfiguration(
            Class<T> targetType,
            Integer columns, Integer rows,
            Boolean setEmptyFieldsNull, Boolean skipFirstLine,
            SortedMap<Integer, MappedProperty> targetFieldMap) {
        this.targetType = targetType;
        this.columns = columns;
        this.rows = rows;
        this.setEmptyFieldsNull = setEmptyFieldsNull;
        this.skipFirstLine = skipFirstLine;
        this.targetFieldMap = targetFieldMap;
    }

    public WorkbookDataAdapterConfiguration() {
        this.setEmptyFieldsNull = Boolean.TRUE;
        this.skipFirstLine = Boolean.FALSE;
    }

    public Integer getColumns() {
        return columns;
    }

    public void setColumns(Integer columns) {
        this.columns = columns;
    }

    public Set<WorkbookRange> getRangeSet() {
        return rangeSet;
    }

    public void setRangeSet(Set<WorkbookRange> rangeSet) {
        this.rangeSet = rangeSet;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Boolean getSetEmptyFieldsNull() {
        return setEmptyFieldsNull;
    }

    public void setSetEmptyFieldsNull(Boolean setEmptyFieldsNull) {
        this.setEmptyFieldsNull = setEmptyFieldsNull;
    }

    public Boolean getSkipFirstLine() {
        return skipFirstLine;
    }

    public void setSkipFirstLine(Boolean skipFirstLine) {
        this.skipFirstLine = skipFirstLine;
    }

    public SortedMap<Integer, MappedProperty> getTargetFieldMap() {
        return targetFieldMap;
    }

    public void setTargetFieldMap(SortedMap<Integer, MappedProperty> targetFieldMap) {
        this.targetFieldMap = targetFieldMap;
    }

    public Class<T> getTargetType() {
        return targetType;
    }

    public void setTargetType(Class<T> targetType) {
        this.targetType = targetType;
    }
}
