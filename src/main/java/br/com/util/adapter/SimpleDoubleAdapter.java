
package br.com.util.adapter;

import br.com.util.Util;

public class SimpleDoubleAdapter implements MappedPropertyAdapter {

    @Override
    public Object adapt(Object value) throws Exception {
        Double retVal = null;
        if (value instanceof Double) {
            retVal = (Double) value;
        } else if (value instanceof Integer) {
            retVal = new Double((Integer)value);
        } else if (value instanceof String) {
            retVal = Util.parseStringToDouble(value.toString());
        }

        return retVal;
    }

}
