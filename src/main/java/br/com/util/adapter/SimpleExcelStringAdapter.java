package br.com.util.adapter;

import org.apache.poi.ss.usermodel.DataFormatter;

public class SimpleExcelStringAdapter implements MappedPropertyAdapter {

    private final DataFormatter formatter;

    public SimpleExcelStringAdapter() {
        formatter = new DataFormatter();
    }

    @Override
    public Object adapt(Object value) throws Exception {
        String retVal = null;
        if (value instanceof Double) {
            retVal = formatter.formatRawCellContents((Double) value, -1, "#");
        } else if (value instanceof Integer) {
            retVal = String.valueOf(value);
        } else if (value instanceof String) {
            retVal = String.class.cast(value);
        }

        return retVal;
    }

}
