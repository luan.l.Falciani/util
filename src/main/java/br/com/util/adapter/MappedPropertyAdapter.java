package br.com.util.adapter;

public interface MappedPropertyAdapter {
    public Object adapt(Object value) throws Exception;
}
