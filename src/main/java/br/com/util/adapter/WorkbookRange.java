package br.com.util.adapter;

public class WorkbookRange {
    private int rowStart;
    private int rowEnd;
    private int numColumns;

    public WorkbookRange() {
    }

    public WorkbookRange(int rowStart, int rowEnd, int numColumns) {
        this.rowStart = rowStart;
        this.rowEnd = rowEnd;
        this.numColumns = numColumns;
    }

    public int getNumColumns() {
        return numColumns;
    }

    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
    }

    public int getRowEnd() {
        return rowEnd;
    }

    public void setRowEnd(int rowEnd) {
        this.rowEnd = rowEnd;
    }

    public int getRowStart() {
        return rowStart;
    }

    public void setRowStart(int rowStart) {
        this.rowStart = rowStart;
    }

}
