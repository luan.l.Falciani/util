package br.com.util.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class WorkbookDataAdapter<T> implements Runnable {

    private WorkbookDataAdapterConfiguration configuration;
    protected BlockingQueue<T> queue;
    private final InputStream inputStream;
    private Map<Integer, String> errorLog;

    public WorkbookDataAdapter(InputStream is) {
        this.inputStream = is;
    }

    public WorkbookDataAdapter(
            BlockingQueue<T> q,
            InputStream inputStream,
            Map<Integer, String> errorLog) {
        this.queue = q;
        this.inputStream = inputStream;
        this.errorLog = errorLog;
    }

    public void loadRecords() throws IOException {
        if (configuration == null) {
            throw new IllegalStateException("WorkbookDataAdapter: Processo de importação não configurado.");
        }
        if (queue == null) {
            throw new IllegalStateException("WorkbookDataAdapter: A Fila de saída para armazenamento dos registros não foi configurada.");
        }

        Long startTime = System.currentTimeMillis();
        boolean discardRecord = false;
        int discardedRecordCount = 0;
        try {
            Workbook wb = WorkbookFactory.create(inputStream);
            Sheet sheet = wb.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.rowIterator();
            Map<Integer, MappedProperty> fieldMap = configuration.getTargetFieldMap();

            if (configuration.getSkipFirstLine()) {
                rowIterator.next();
            }
            String errorMsg = null;
            MappedProperty mp;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                discardRecord = false;
                T newBean = (T) configuration.getTargetType().newInstance();

                for (Entry<Integer, MappedProperty> e : fieldMap.entrySet()) {
                    Cell cell = row.getCell(e.getKey());
                    mp = e.getValue();
                    Object pValue = null;
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_NUMERIC:
                            pValue = mp.getValueAdapter().adapt(cell.getNumericCellValue());
                            break;
                        case Cell.CELL_TYPE_BOOLEAN:
                        case Cell.CELL_TYPE_STRING:
                            pValue = mp.getValueAdapter().adapt(cell.getRichStringCellValue().getString());
                            break;
                        default:
                            break;
                    }

                    if (mp.getWhenNullDiscardRecord() && pValue == null) {
                        discardRecord = true;
                        discardedRecordCount++;
                        errorMsg = "Linha " + row.getRowNum() + ": Propriedade obrigatória está vazia: " + mp.getProperty().getName();
                    } else {
                        try {
                            PropertyUtils.setProperty(newBean, mp.getProperty().getName(), pValue);
                        } catch (IllegalArgumentException iae) {
                            discardRecord = true;
                            discardedRecordCount++;
                            errorMsg = "Linha " + row.getRowNum() + ": Tentando atribuir propriedade " + mp.getProperty().getName() + " do tipo "
                                    + mp.getProperty().getType().getName() + " com valor '"
                                    + pValue.toString() + "' do tipo "
                                    + pValue.getClass().getCanonicalName() + " resultou em ClassCastException";
                            Logger.getLogger(WorkbookDataAdapter.class.getName()).log(Level.SEVERE, errorMsg);
                        }
                    }
                }
                if (!discardRecord) {
                    //System.out.println("Adding bean: " + newBean.toString());
                    queue.add(newBean);
                } else {
                    if (errorLog != null) {
                        errorLog.put(row.getRowNum(), errorMsg);
                    }
                }
            }
            queue.add((T) configuration.getTargetType().newInstance()); //poison
        } catch (Exception ex) {
            Logger.getLogger(WorkbookDataAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Elapsed time: " + (int) ((System.currentTimeMillis() - startTime) / 1000) + "s");
        System.out.println(String.format("Returning bean list with %d entries (%d discarded records).", queue.size(), discardedRecordCount));
    }

    public void readWorkbookToList(InputStream inputStream, List<List<Object>> outputList)
            throws IOException, InvalidFormatException {

        if (outputList == null) {
            outputList = new ArrayList<>();
        }
        Workbook workBook = WorkbookFactory.create(inputStream);
        Sheet sheet = workBook.getSheetAt(0);

        Iterator rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = (Row) rowIterator.next();
            Iterator iterator = row.cellIterator();
            List<Object> rowObjectList = new ArrayList<>();


            while (iterator.hasNext()) {
                Cell cell = (Cell) iterator.next();

                if (DateUtil.isCellDateFormatted(cell)) {
                    rowObjectList.add(cell.getDateCellValue());
                } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    rowObjectList.add(new Double(cell.getNumericCellValue()));
                } else {
                    rowObjectList.add(cell.getRichStringCellValue().getString());
                }
            }
            outputList.add(rowObjectList);
        }
    }

    public WorkbookDataAdapterConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(WorkbookDataAdapterConfiguration configuration) {
        this.configuration = configuration;
    }

    public void run() {
        try {
            loadRecords();
        } catch (IOException ex) {
            Logger.getLogger(WorkbookDataAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
