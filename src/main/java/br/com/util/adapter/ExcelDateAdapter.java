package br.com.util.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.poi.ss.usermodel.DateUtil;

public class ExcelDateAdapter implements MappedPropertyAdapter {

    private static final String[] DATE_FORMAT_STRINGS = {"M/y", "d/M/y", "d-M-y"};

    @Override
    public Object adapt(Object value) {
        Date d = null;

        if (value instanceof Double) {
            d = DateUtil.getJavaDate((Double) value);
        } else if (value instanceof String) {
            d = tryParseDate((String) value);
        }

        return d;
    }

    private Date tryParseDate(String dateString) {
        Date d = null;
        for (String formatString : DATE_FORMAT_STRINGS) {
            try {
                d = new SimpleDateFormat(formatString).parse(dateString);
                break;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return d;
    }
}
