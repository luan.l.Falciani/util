/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.util;

/**
 * @author Juliano A. Felipe
 *
 * Classe com as mensagens de Sucesso e Erro que serão enviados para o Flex.
 *
 */
public class Info {

    /******************* Constantes com os códigos dos erros ******************/

    /** 
     * Sucesso :
     *	    Quando a ação é executada com sucesso.
    **/
    public static final String INFO_001 = "001";

    /**
     * Erro ao executar uma operação :
     */
     public static final String INFO_002 = "002";

     /**
     * "Warning" ao executar uma operação :
     */
     public static final String INFO_004 = "004";
     
     /**************************************************************************/
     
    /******************* Constantes com os códigos dos erros HTML5 ******************/
    public static String TIPO_MSG_WARNING = "warning";
    public static String TIPO_MSG_DANGER = "danger";
    public static String TIPO_MSG_SUCCESS = "success";

    /**************************************************************************/

    /** Atributos da classe **/
    private String codigo;
    private String mensagem;
    private String erro;
    private String warning;
    private Integer idObjetoSalvo;
    private String tipo;
    private Object objeto;
     
    /**
     * Construtor
     */
    public Info() {
	
    }
     

    /** Getters e Setters **/
    public String getCodigo() {
	return codigo;
    }

    public void setCodigo(String codigo) {
	this.codigo = codigo;
    }

    public String getMensagem() {
	return mensagem;
    }

    public void setMensagem(String mensagem) {
	this.mensagem = mensagem;
    }

    public String getErro() {
	return erro;
    }

    public void setErro(String erro) {
	this.erro = erro;
    }

    public Integer getIdObjetoSalvo() {
        return idObjetoSalvo;
    }

    public void setIdObjetoSalvo(Integer idObjetoSalvo) {
        this.idObjetoSalvo = idObjetoSalvo;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }   

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }
    
}
