package br.com.util;

import br.com.persistence_2.util.HibernateUtil;
import br.com.util.vo.RelatorioVo;
import edu.emory.mathcs.backport.java.util.Arrays;
import jxl.Cell;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Time;
import java.util.*;

public class RelatorioUtil {

    HashMap<String, WritableCellFormat> hashMapStyles = new HashMap<>();

    class CabecalhoColunaExcel {

        Integer indice;
        String nomeCabecalhoColuna;
        String chave;

        public CabecalhoColunaExcel(Integer _indice, String _nomeCabecalhoColuna, String _chave) {
            this.indice = _indice;
            this.nomeCabecalhoColuna = _nomeCabecalhoColuna;
            this.chave = _chave;
        }

        public String getNomeCabecalhoColuna() {
            return nomeCabecalhoColuna;
        }

        public void setNomeCabecalhoColuna(String nomeCabecalhoColuna) {
            this.nomeCabecalhoColuna = nomeCabecalhoColuna;
        }

        public String getChave() {
            return chave;
        }

        public void setChave(String chave) {
            this.chave = chave;
        }

        public Integer getIndice() {
            return indice;
        }

        public void setIndice(Integer indice) {
            this.indice = indice;
        }

    }

    class FiltroExcel {

        Integer indice;
        String descricao;
        String valor;

        public FiltroExcel(Integer _indice, String _descricao, String _valor) {
            this.indice = _indice;
            this.descricao = _descricao;
            this.valor = _valor;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }

        public String getValor() {
            return valor;
        }

        public void setValor(String valor) {
            this.valor = valor;
        }

        public Integer getIndice() {
            return indice;
        }

        public void setIndice(Integer indice) {
            this.indice = indice;
        }

    }

    public byte[] geraRelatorio(RelatorioBean relatorioBean) {
        byte[] bytes = null;
        switch (relatorioBean.getTipo()) {
            case XLS:
                if (relatorioBean.getListaRelatorioBeanItens() != null && relatorioBean.getListaRelatorioBeanItens().size() > 0) {
                    bytes = gerarXLSMultiplasListas(relatorioBean);
                } else {
                    bytes = gerarXLS(relatorioBean);
                }

                break;
            case PDF:
                if (relatorioBean.getListaRelatorioBeanItens() != null) {
                    bytes = geraPdfMultiplosItens(relatorioBean);
                } else if (relatorioBean.getListaJasperBeanCollection() != null) {
                    bytes = geraPdfCollectionDataSource(relatorioBean);
                } else {
                    bytes = geraPdf(relatorioBean);
                }

                break;

            case WORD:
                bytes = geraWord(relatorioBean);
        }
        return bytes;
    }

    /**
     * Monta o relatorio no formato em EXCEL
     *
     * @param relatorioBean
     * @return byte[]
     */
    private byte[] gerarXLS(RelatorioBean relatorioBean) {
        HashMap<String, Object> valorTotal = new HashMap<>();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            WorkbookSettings ws = new WorkbookSettings();
            ws.setLocale(new Locale("pt", "BR"));
            WritableWorkbook workbook = Workbook.createWorkbook(baos);
            WritableSheet sheet;
            if (relatorioBean.getTitulo().length() > 30) {
                sheet = workbook.createSheet(relatorioBean.getTitulo().substring(0, 27) + "...", 0);
            } else {
                sheet = workbook.createSheet(relatorioBean.getTitulo(), 0);
            }
            WritableFont arial12Bold = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);

            DateFormat customDateFormat = new DateFormat("dd/MM/yyyy");
            if (relatorioBean.getDateFormat() != null && !relatorioBean.getDateFormat().equals("")) {
                customDateFormat = new DateFormat(relatorioBean.getDateFormat());
            }

            //Formata NÃºmero.
            NumberFormat custonNumberFormat = new NumberFormat("#,##0.00");
            WritableCellFormat numberFormat = new WritableCellFormat(custonNumberFormat);
            numberFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            numberFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

            WritableCellFormat dateFormat = new WritableCellFormat(customDateFormat);
            dateFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignment6 = Alignment.CENTRE;
            dateFormat.setAlignment(alignment6);

            DateFormat customTimeFormat = new DateFormat("HH:mm");
            WritableCellFormat timeFormat = new WritableCellFormat(customTimeFormat);
            timeFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            timeFormat.setAlignment(alignment6);

            WritableCellFormat cellString = new WritableCellFormat();
            cellString.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignment = Alignment.LEFT;
            cellString.setAlignment(alignment);

            WritableCellFormat cellStringWrap = new WritableCellFormat();
            cellStringWrap.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cellStringWrap.setAlignment(alignment);
            cellStringWrap.setWrap(true);

            WritableCellFormat cellInteger = new WritableCellFormat();
            cellInteger.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignmentInt = Alignment.RIGHT;
            cellInteger.setAlignment(alignmentInt);

            WritableCellFormat cellDouble = new WritableCellFormat(custonNumberFormat);
            cellDouble.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignment1 = Alignment.RIGHT;
            cellDouble.setAlignment(alignment1);

            WritableCellFormat cellCenter = new WritableCellFormat();
            Alignment alignment7 = Alignment.CENTRE;
            cellCenter.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cellCenter.setAlignment(alignment7);

            WritableCellFormat titulo = new WritableCellFormat();
            Alignment alignment2 = Alignment.CENTRE;
            titulo.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            titulo.setAlignment(alignment2);

            WritableCellFormat cabecalho = new WritableCellFormat();
            Colour color = Colour.WHITE;
            cabecalho.setBackground(color);
            cabecalho.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cabecalho.setFont(arial12Bold);
            cabecalho.setWrap(true);
            cabecalho.setAlignment(Alignment.CENTRE);
            cabecalho.setVerticalAlignment(VerticalAlignment.CENTRE);

            WritableCellFormat cabecalhoPrincipal = new WritableCellFormat();
            Colour colorCab = Colour.GREY_25_PERCENT;
            cabecalhoPrincipal.setBackground(colorCab);
            cabecalhoPrincipal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cabecalhoPrincipal.setFont(arial12Bold);
            cabecalhoPrincipal.setWrap(true);
            cabecalhoPrincipal.setAlignment(Alignment.CENTRE);
            cabecalhoPrincipal.setVerticalAlignment(VerticalAlignment.CENTRE);

            WritableCellFormat cabecalhoCenter = new WritableCellFormat();
            Alignment alignment8 = Alignment.CENTRE;
            Colour color1 = Colour.GREY_25_PERCENT;
            cabecalhoCenter.setBackground(color1);
            cabecalhoCenter.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cabecalhoCenter.setAlignment(alignment8);

            //Formato para Totais.
            WritableFont arial10Bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

            WritableCellFormat cellIntegerTotal = new WritableCellFormat();
            Colour colorDoubleTotal = Colour.GREY_25_PERCENT;
            cellIntegerTotal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignmentIntTotal = Alignment.CENTRE;
            cellIntegerTotal.setAlignment(alignmentIntTotal);
            cellIntegerTotal.setFont(arial10Bold);
            cellIntegerTotal.setBackground(colorDoubleTotal);

            WritableCellFormat cellDoubleTotal = new WritableCellFormat(custonNumberFormat);
            Colour colorIntTotal = Colour.GREY_25_PERCENT;
            cellDoubleTotal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignmentDoubleTotal = Alignment.RIGHT;
            cellDoubleTotal.setAlignment(alignmentDoubleTotal);
            cellDoubleTotal.setFont(arial10Bold);
            cellDoubleTotal.setBackground(colorIntTotal);

            WritableCellFormat cellNullTotal = new WritableCellFormat();
            Colour colorNullTotal = Colour.GREY_25_PERCENT;
            cellNullTotal.setBackground(colorNullTotal);
            cellNullTotal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cellNullTotal.setFont(arial10Bold);
            cellNullTotal.setAlignment(Alignment.CENTRE);
            cellNullTotal.setVerticalAlignment(VerticalAlignment.CENTRE);

            /**
             * CellView para Largura da coluna == auto size
             *
             */
            CellView cvAutoSize = new CellView();
            cvAutoSize.setAutosize(true);

            CellView cvSize22000 = new CellView();
            cvSize22000.setSize(22000);

            CellView cvSize2000 = new CellView();
            cvSize2000.setSize(2000);

            int linha = 0;
            int coluna = 0;
            int qtdeColunas = relatorioBean.getMapaListaItens().size() - 1;
            Label label;
            jxl.write.Number number;
            DateTime dateCell;
            WritableImage writableImage;

            /**
             * LOGOMARCA DO RELATÃ“RIO
             */
            if (relatorioBean.getLogomarca() != null) {

                writableImage = new WritableImage(linha, coluna, 1, 4, relatorioBean.getLogomarca());

                sheet.addImage(writableImage);

                label = new Label(coluna, linha, relatorioBean.getTitulo().toUpperCase());
                label.setCellFormat(cabecalhoPrincipal);
                sheet.addCell(label);
                sheet.mergeCells(coluna, linha, qtdeColunas, linha + 3);
                linha += 4;
            } /**
             * TITULO DO RELATÃ“RIO *
             */
            else {
                label = new Label(coluna, linha, relatorioBean.getTitulo().toUpperCase());
                label.setCellFormat(cabecalhoPrincipal);
                sheet.addCell(label);
                sheet.mergeCells(coluna, linha, qtdeColunas, linha + 1);
                linha += 2;
            }

            //LINHA EM BRANCO
            label = new Label(coluna, linha, "");
            label.setCellFormat(cellString);
            sheet.addCell(label);
            sheet.mergeCells(coluna, linha, qtdeColunas, linha);
            linha++;

            List<FiltroExcel> filtrosExcel = montaFiltroExcelOrdenado(relatorioBean);

            //FILTROS
            for (FiltroExcel filtroExcel : filtrosExcel) {
                label = new Label(coluna, linha, filtroExcel.getDescricao() + ": " + filtroExcel.getValor());
                label.setCellFormat(cellString);
                sheet.addCell(label);
                sheet.mergeCells(coluna, linha, qtdeColunas, linha);
                linha++;
            }

            //SE TIVER FILTRO IMPRIME LINHA EM BRANCO
            if (filtrosExcel != null && filtrosExcel.size() > 0) {
                label = new Label(coluna, linha, "");
                label.setCellFormat(cellString);
                sheet.addCell(label);
                sheet.mergeCells(coluna, linha, qtdeColunas, linha);
                linha++;
            }

            List<CabecalhoColunaExcel> cabecalhosColuna = montaCabecalhoColunasOrdenado(relatorioBean);
            //CABEÃ‡ALHO-COLUNAS
            for (CabecalhoColunaExcel cabecalhoColuna : cabecalhosColuna) {
                label = new Label(coluna, linha, cabecalhoColuna.getNomeCabecalhoColuna());
                sheet.addCell(label);
                label.setCellFormat(cabecalho);
                coluna++;
            }

            linha++;

            HashMap<Integer, Integer> colunasWrap = new HashMap<>();
            String nomeColuna;
            String[] caminho;
            Object itemColuna;
            boolean aplicarStyle;
            //ITENS
            if (relatorioBean.getItens() != null) {
                for (Object item : relatorioBean.getItens()) {
                    if (item.getClass().getSuperclass() != null && item.getClass().getSuperclass().getSimpleName().equals("RelatorioVo")) {
                        aplicarStyle = ((RelatorioVo) item).getAplicar();
                    } else {
                        aplicarStyle = false;
                    }

                    coluna = 0;
                    //PREENCHE OS VALORES DAS COLUNAS COM OS VALORES DO OBJETO
                    for (CabecalhoColunaExcel cabecalhoColuna : cabecalhosColuna) {
                        nomeColuna = cabecalhoColuna.getChave();
                        itemColuna = item;

                        //PERCORE O CAMINHO ATE ACHAR O OBJETO
                        if (nomeColuna.contains(".")) {
                            caminho = nomeColuna.split("\\.");
                            for (String caminho1 : caminho) {
                                if (!(itemColuna == null)) {
                                    itemColuna = PropertyUtils.getProperty(itemColuna, caminho1);
                                }
                            }
                        } else {
                            itemColuna = PropertyUtils.getProperty(itemColuna, nomeColuna + "");
                        }
                        //NULL
                        if (itemColuna == null) {
                            label = new Label(coluna, linha, "");
                            label.setCellFormat(aplicarStyle ? getStyle(cellCenter, (RelatorioVo) item, null, false) : cellCenter);
                            sheet.addCell(label);
                            //STRING
                        } else if (itemColuna instanceof String) {
                            label = new Label(coluna, linha, itemColuna + "");
                            if ((itemColuna + "").length() < 100) {
                                label.setCellFormat(aplicarStyle ? getStyle(cellString, (RelatorioVo) item, null, false) : cellString);
                            } else {
                                label.setCellFormat(aplicarStyle ? getStyle(cellStringWrap, (RelatorioVo) item, null, false) : cellStringWrap);
                                colunasWrap.put(coluna, coluna);
                            }
                            sheet.addCell(label);
                            //DATE
                        } else if (itemColuna instanceof Date) {
                            if (relatorioBean.getMapaFormatadores() != null
                                    && relatorioBean.getMapaFormatadores().get(cabecalhoColuna.getChave()) != null) {
                                String mascara = ((String) relatorioBean.getMapaFormatadores().get(cabecalhoColuna.getChave()));

                                DateFormat formatoData = new DateFormat(mascara);
                                WritableCellFormat dateMascaraFormat = new WritableCellFormat(formatoData);
                                dateMascaraFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
                                dateMascaraFormat.setAlignment(alignment6);

                                dateCell = new DateTime(coluna, linha, (Date) itemColuna, dateMascaraFormat);
                                dateCell.setCellFormat(aplicarStyle ? getStyle(dateMascaraFormat, (RelatorioVo) item, mascara, true) : dateMascaraFormat);
                                sheet.addCell(dateCell);
                            } else {
                                if (itemColuna instanceof Time) {
                                    dateCell = new DateTime(coluna, linha, (Date) itemColuna, timeFormat);
                                    dateCell.setCellFormat(aplicarStyle ? getStyle(timeFormat, (RelatorioVo) item, "HH:mm", true) : timeFormat);
                                    sheet.addCell(dateCell);
                                } else {
                                    dateCell = new DateTime(coluna, linha, (Date) itemColuna, dateFormat);
                                    dateCell.setCellFormat(aplicarStyle ? getStyle(dateFormat, (RelatorioVo) item, "dd/MM/yyyy", true) : dateFormat);
                                    sheet.addCell(dateCell);
                                }
                            }
                            //DOUBLE
                        } else if (itemColuna instanceof Double) {

                            //ImplementaÃ§Ã£o adicionar a mascara de moeda no campo double
                            if (relatorioBean.getMapaFormatadores() != null
                                    && relatorioBean.getMapaFormatadores().get(cabecalhoColuna.getChave()) != null
                                    && ((String) relatorioBean.getMapaFormatadores().get(cabecalhoColuna.getChave())).split(":")[0].equals("moeda")) {
                                String mascara = ((String) relatorioBean.getMapaFormatadores().get(cabecalhoColuna.getChave())).split(":")[1];

                                NumberFormat moedaFormat = new NumberFormat(mascara + " ###,###.00", NumberFormat.COMPLEX_FORMAT);
                                WritableCellFormat WCMoedaFormat = new WritableCellFormat(moedaFormat);
                                WCMoedaFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
                                Alignment alignment12 = Alignment.RIGHT;
                                WCMoedaFormat.setAlignment(alignment12);

                                number = new jxl.write.Number(coluna, linha, (Double) itemColuna, WCMoedaFormat);
                                number.setCellFormat(aplicarStyle ? getStyle(WCMoedaFormat, (RelatorioVo) item, (mascara + " ###,###.00"), false) : WCMoedaFormat);

                                sheet.addCell(number);
                            } else {
                                number = new jxl.write.Number(coluna, linha, (Double) itemColuna, numberFormat);
                                number.setCellFormat(aplicarStyle ? getStyle(cellDouble, (RelatorioVo) item, "#,##0.00", false) : cellDouble);
                                sheet.addCell(number);
                            }

                            if (relatorioBean.getTemTotal() && relatorioBean.getMapaTemTotal().get(cabecalhoColuna.getChave()) != null && (Boolean) relatorioBean.getMapaTemTotal().get(cabecalhoColuna.getChave())) {
                                if (valorTotal.get(cabecalhoColuna.getChave()) != null) {
                                    valorTotal.put(cabecalhoColuna.getChave(), (Double) valorTotal.get(cabecalhoColuna.getChave()) + (Double) itemColuna);
                                } else {
                                    valorTotal.put(cabecalhoColuna.getChave(), (Double) itemColuna);
                                }
                            }

                            //INTEGER
                        } else if (itemColuna instanceof Integer) {
                            number = new jxl.write.Number(coluna, linha, (Integer) itemColuna);
                            number.setCellFormat(aplicarStyle ? getStyle(cellInteger, (RelatorioVo) item, null, false) : cellInteger);
                            sheet.addCell(number);

                            if (relatorioBean.getTemTotal() && relatorioBean.getMapaTemTotal().get(cabecalhoColuna.getChave()) != null && (Boolean) relatorioBean.getMapaTemTotal().get(cabecalhoColuna.getChave())) {
                                if (valorTotal.get(cabecalhoColuna.getChave()) != null) {
                                    valorTotal.put(cabecalhoColuna.getChave(), (Integer) valorTotal.get(cabecalhoColuna.getChave()) + (Integer) itemColuna);
                                } else {
                                    valorTotal.put(cabecalhoColuna.getChave(), (Integer) itemColuna);
                                }
                            }
                            //BYTE[]
                        } else if (itemColuna instanceof byte[]) {
                            //Adiciona uma celula vazia para servir de borda.
                            label = new Label(coluna, linha, "");
                            label.setCellFormat(aplicarStyle ? getStyle(cellCenter, (RelatorioVo) item, null, false) : cellCenter);
                            sheet.addCell(label);
                            /*Adiciona um array de byte como imagem. 
                             90% do tamanho da celula.
                             5% a direita para centralizar a imagem.
                             Seta a altura da linha com imagem.
                             */
                            writableImage = new WritableImage(coluna + 0.05, linha + 0.05, 0.9, 0.9, (byte[]) itemColuna);
                            sheet.addImage(writableImage);
                            sheet.setRowView(linha, cvSize2000);
                            //BOOLEAN
                        } else if (itemColuna instanceof Boolean) {
                            label = new Label(coluna, linha, (Boolean) itemColuna ? "Sim" : "Não");
                            label.setCellFormat(aplicarStyle ? getStyle(cellString, (RelatorioVo) item, null, false) : cellString);
                            sheet.addCell(label);
                        }
                        //PROXIMA COLUNA
                        coluna++;
                    }
                    //PROXIMA LINHA
                    linha++;
                }

                if (relatorioBean.getTemTotal()) {
                    Object total;
                    coluna = 0;

                    //PREENCHE OS VALORES DAS COLUNAS COM OS VALORES DO OBJETO
                    for (CabecalhoColunaExcel cabecalhoColuna : cabecalhosColuna) {
                        total = valorTotal.get(cabecalhoColuna.getChave());

                        //verifica se o totalizador possui mascara
                        if (relatorioBean.getMapaFormatadores() != null
                                && relatorioBean.getMapaFormatadores().get(cabecalhoColuna.getChave()) != null
                                && ((String) relatorioBean.getMapaFormatadores().get(cabecalhoColuna.getChave())).split(":")[0].equals("moeda")) {

                            if (total == null) {
                                label = new Label(coluna, linha, "");
                                label.setCellFormat(cellNullTotal);
                                sheet.addCell(label);
                                //DOUBLE
                            } else if (total instanceof Double) {

                                String mascara = ((String) relatorioBean.getMapaFormatadores().get(cabecalhoColuna.getChave())).split(":")[1];

                                NumberFormat moedaFormat = new NumberFormat(mascara + " ###,###.00", NumberFormat.COMPLEX_FORMAT);
                                WritableCellFormat WCMoedaFormat = new WritableCellFormat(moedaFormat);
                                WCMoedaFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
                                Alignment alignment12 = Alignment.RIGHT;
                                WCMoedaFormat.setAlignment(alignment12);

                                number = new jxl.write.Number(coluna, linha, (Double) total, WCMoedaFormat);
                                number.setCellFormat(WCMoedaFormat);

                                sheet.addCell(number);
                            }
                            //caso nÃ£o tenha mascara, apenas adiciona o valor na celula
                        } else {
                            //NULL
                            if (total == null) {
                                label = new Label(coluna, linha, "");
                                label.setCellFormat(cellNullTotal);
                                sheet.addCell(label);
                                //DOUBLE
                            } else if (total instanceof Double) {
                                number = new jxl.write.Number(coluna, linha, (Double) total, numberFormat);
                                number.setCellFormat(cellDoubleTotal);
                                sheet.addCell(number);
                                //INTEGER
                            } else if (total instanceof Integer) {
                                label = new Label(coluna, linha, total + "");
                                label.setCellFormat(cellIntegerTotal);
                                sheet.addCell(label);
                            }
                        }
                        //PROXIMA COLUNA
                        coluna++;

                    }
                }
            }

            //TAMANHO DAS COLUNAS
            /* Caso possua assinaturas, algumas celulas são mescladas.
             * Quando existem celulas mescladas abaixo do relatorio, a propriedade 
             * "setAutosize(true)" calcula errado o tamanho das colunas, 
             * então é preciso calcular manualmente o tamanho de cada uma.
             *
             */
            if (relatorioBean.getAssinaturas() != null) {
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell[] cells = sheet.getColumn(i);
                    int longestStrLen;

                    if (cells.length == 0) {
                        continue;
                    }

                    CabecalhoColunaExcel cab = cabecalhosColuna.get(i);
                    longestStrLen = cab.getNomeCabecalhoColuna().length();

                    //Encontra a maior celula na coluna
                    for (Cell cell : cells) {
                        if (cell.getContents().length() > longestStrLen) {
                            String str = cell.getContents();
                            if (str == null || str.isEmpty()) {
                                continue;
                            }
                            longestStrLen = str.trim().length();
                        }
                    }

                    if (longestStrLen == cab.getNomeCabecalhoColuna().length()) {
                        longestStrLen = longestStrLen * 2 + 2;
                    }

                    //Se não encontrou, pula a coluna
                    if (longestStrLen == -1) {
                        continue;
                    }

                    //Se o tamanho estourar o limite maximo, seta o maior tamanho possivel
                    if (longestStrLen > 255) {
                        longestStrLen = 255;
                    }

                    CellView cv = sheet.getColumnView(i);
                    cv.setSize(longestStrLen * 256 + 150); //Cada caracter ocupa 256 de tamanho

                    sheet.setColumnView(i, cv);
                }
            } else {

                for (int i = 0; i < sheet.getColumns(); i++) {
                    if (colunasWrap.size() > 0 && colunasWrap.containsValue(i)) {
                        sheet.setColumnView(i, cvSize22000);
                    } else {
                        sheet.setColumnView(i, cvAutoSize);
                    }
                }
            }

            if (relatorioBean.getAssinaturas() != null) {
                WritableCellFormat assinaturaFormat = new WritableCellFormat();
                assinaturaFormat.setBorder(jxl.format.Border.NONE, BorderLineStyle.THIN);
                assinaturaFormat.setAlignment(alignment2);

                List<String> assinaturas;
                assinaturas = Arrays.asList(relatorioBean.getAssinaturas().split(";"));
                int qtdeAssinaturas = assinaturas.size();
                qtdeColunas = sheet.getColumns();
                int x = qtdeColunas / qtdeAssinaturas;

                linha += 4;
                coluna = 0;
                for (int i = 1; i <= qtdeAssinaturas; i++) {
                    String assinatura = assinaturas.get(i - 1).split("=")[0];
                    String stringsDepoisNome = "";
                    List<String> listaStringsDepoisNome = Arrays.asList(assinaturas.get(i - 1).split("="));
                    for (String str : listaStringsDepoisNome) {
                        stringsDepoisNome += str + '\n';
                    }

                    int tamanhoTraco = assinatura.length();
                    for (String str : listaStringsDepoisNome) {
                        if (str.length() > tamanhoTraco) {
                            tamanhoTraco = str.length();
                        }
                    }

                    String traco = "";
                    for (int j = 0; j < tamanhoTraco + 5; j++) {
                        traco += '_';
                    }

                    label = new Label(coluna, linha, traco + '\n' + stringsDepoisNome);
                    label.setCellFormat(assinaturaFormat);
                    sheet.addCell(label);
                    sheet.mergeCells(coluna, linha, (coluna + x) - 1, linha + 3);
                    coluna = (coluna + x);
                }
            }

            workbook.write();
            workbook.close();

        } catch (IOException | WriteException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return baos.toByteArray();
    }

    /**
     * Monta o relatorio no formato em EXCEL
     *
     * @param relatorioBean
     * @return byte[]
     */
    private byte[] gerarXLSMultiplasListas(RelatorioBean relatorioBean) {
        HashMap<String, Object> valorTotal = new HashMap<>();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            WorkbookSettings ws = new WorkbookSettings();
            ws.setLocale(new Locale("pt", "BR"));
            WritableWorkbook workbook = Workbook.createWorkbook(baos);
            WritableSheet sheet;
            if (relatorioBean.getTitulo().length() > 30) {
                sheet = workbook.createSheet(relatorioBean.getTitulo().substring(0, 27) + "...", 0);
            } else {
                sheet = workbook.createSheet(relatorioBean.getTitulo(), 0);
            }
            WritableFont arial12Bold = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);

            DateFormat customDateFormat = new DateFormat("dd/MM/yyyy");
            if (relatorioBean.getDateFormat() != null && !relatorioBean.getDateFormat().equals("")) {
                customDateFormat = new DateFormat(relatorioBean.getDateFormat());
            }

            WritableCellFormat dateFormat = new WritableCellFormat(customDateFormat);
            dateFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignment6 = Alignment.CENTRE;
            dateFormat.setAlignment(alignment6);

            DateFormat customTimeFormat = new DateFormat("HH:mm");
            WritableCellFormat timeFormat = new WritableCellFormat(customTimeFormat);
            timeFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            timeFormat.setAlignment(alignment6);

            WritableCellFormat cellString = new WritableCellFormat();
            cellString.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignment = Alignment.LEFT;
            cellString.setAlignment(alignment);

            WritableCellFormat cellStringWrap = new WritableCellFormat();
            cellStringWrap.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cellStringWrap.setAlignment(alignment);
            cellStringWrap.setWrap(true);

            WritableCellFormat cellInteger = new WritableCellFormat();
            cellInteger.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignmentInt = Alignment.RIGHT;
            cellInteger.setAlignment(alignmentInt);

            WritableCellFormat cellDouble = new WritableCellFormat();
            cellDouble.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignment1 = Alignment.RIGHT;
            cellDouble.setAlignment(alignment1);

            WritableCellFormat cellCenter = new WritableCellFormat();
            Alignment alignment7 = Alignment.CENTRE;
            cellCenter.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cellCenter.setAlignment(alignment7);

            WritableCellFormat titulo = new WritableCellFormat();
            Alignment alignment2 = Alignment.CENTRE;
            titulo.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            titulo.setAlignment(alignment2);

            WritableCellFormat cabecalho = new WritableCellFormat();
            Colour color = Colour.WHITE;
            cabecalho.setBackground(color);
            cabecalho.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cabecalho.setFont(arial12Bold);
            cabecalho.setWrap(true);
            cabecalho.setAlignment(Alignment.CENTRE);
            cabecalho.setVerticalAlignment(VerticalAlignment.CENTRE);

            WritableCellFormat cabecalhoPrincipal = new WritableCellFormat();
            Colour colorCab = Colour.GREY_40_PERCENT;
            cabecalhoPrincipal.setBackground(colorCab);
            cabecalhoPrincipal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cabecalhoPrincipal.setFont(arial12Bold);
            cabecalhoPrincipal.setWrap(true);
            cabecalhoPrincipal.setAlignment(Alignment.CENTRE);
            cabecalhoPrincipal.setVerticalAlignment(VerticalAlignment.CENTRE);

            WritableCellFormat subCabecalhoPrincipal = new WritableCellFormat();
            Colour colorSubCab = Colour.GRAY_25;
            subCabecalhoPrincipal.setBackground(colorSubCab);
            subCabecalhoPrincipal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            subCabecalhoPrincipal.setFont(arial12Bold);
            subCabecalhoPrincipal.setWrap(true);
            subCabecalhoPrincipal.setAlignment(Alignment.CENTRE);
            subCabecalhoPrincipal.setVerticalAlignment(VerticalAlignment.CENTRE);

            WritableCellFormat cabecalhoCenter = new WritableCellFormat();
            Alignment alignment8 = Alignment.CENTRE;
            Colour color1 = Colour.GREY_25_PERCENT;
            cabecalhoCenter.setBackground(color1);
            cabecalhoCenter.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cabecalhoCenter.setAlignment(alignment8);

            //Formata NÃºmero.
            NumberFormat custonNumberFormat = new NumberFormat("#,##0.00");
            WritableCellFormat numberFormat = new WritableCellFormat(custonNumberFormat);
            numberFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            numberFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

            //Formato para Totais.
            WritableFont arial10Bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

            WritableCellFormat cellIntegerTotal = new WritableCellFormat();
            Colour colorDoubleTotal = Colour.GREY_25_PERCENT;
            cellIntegerTotal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignmentIntTotal = Alignment.CENTRE;
            cellIntegerTotal.setAlignment(alignmentIntTotal);
            cellIntegerTotal.setFont(arial10Bold);
            cellIntegerTotal.setBackground(colorDoubleTotal);

            WritableCellFormat cellDoubleTotal = new WritableCellFormat(custonNumberFormat);
            Colour colorIntTotal = Colour.GREY_25_PERCENT;
            cellDoubleTotal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignmentDoubleTotal = Alignment.RIGHT;
            cellDoubleTotal.setAlignment(alignmentDoubleTotal);
            cellDoubleTotal.setFont(arial10Bold);
            cellDoubleTotal.setBackground(colorIntTotal);

            WritableCellFormat cellNullTotal = new WritableCellFormat();
            Colour colorNullTotal = Colour.GREY_25_PERCENT;
            cellNullTotal.setBackground(colorNullTotal);
            cellNullTotal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cellNullTotal.setFont(arial10Bold);
            cellNullTotal.setAlignment(Alignment.CENTRE);
            cellNullTotal.setVerticalAlignment(VerticalAlignment.CENTRE);

            /**
             * CellView para Largura da coluna == auto size
             *
             */
            CellView cvAutoSize = new CellView();
            cvAutoSize.setAutosize(true);

            CellView cvSize22000 = new CellView();
            cvSize22000.setSize(22000);

            int linha = 0;
            int coluna = 0;
            int qtdeColunas = relatorioBean.getListaRelatorioBeanItens() != null && relatorioBean.getListaRelatorioBeanItens().size() > 0 ? (((RelatorioBean) relatorioBean.getListaRelatorioBeanItens().get(0)).getMapaListaItens().size() - 1) : 10;
            Label label;
            jxl.write.Number number;
            DateTime dateCell;

            /**
             * TITULO DO RELATÃ“RIO *
             */
            label = new Label(coluna, linha, relatorioBean.getTitulo().toUpperCase());
            label.setCellFormat(cabecalhoPrincipal);
            sheet.addCell(label);
            sheet.mergeCells(coluna, linha, qtdeColunas, linha + 1);
            linha += 2;

            //LINHA EM BRANCO
            label = new Label(coluna, linha, "");
            label.setCellFormat(cellString);
            sheet.addCell(label);
            sheet.mergeCells(coluna, linha, qtdeColunas, linha);
            linha++;

            List<FiltroExcel> filtrosExcel = montaFiltroExcelOrdenadoMultiplasListas(relatorioBean);

            //FILTROS
            for (FiltroExcel filtroExcel : filtrosExcel) {
                label = new Label(coluna, linha, filtroExcel.getDescricao() + ": " + filtroExcel.getValor());
                label.setCellFormat(cellString);
                sheet.addCell(label);
                sheet.mergeCells(coluna, linha, qtdeColunas, linha);
                linha++;
            }

            //SE TIVER FILTRO IMPRIME LINHA EM BRANCO
            if (filtrosExcel != null && filtrosExcel.size() > 0) {
                label = new Label(coluna, linha, "");
                label.setCellFormat(cellString);
                sheet.addCell(label);
                sheet.mergeCells(coluna, linha, qtdeColunas, linha);
                linha++;
            }

            HashMap<Integer, Integer> colunasWrap = new HashMap<>();
            List<CabecalhoColunaExcel> cabecalhosColuna;

            /**/
            linha = linha - 2;
            //Monta as Listas comforme as tabela
            for (Object relBean1 : relatorioBean.getListaRelatorioBeanItens()) {
                RelatorioBean relBean = (RelatorioBean) relBean1;

                //Adiciona um espaÃ§o para separar as tabelas
                linha = linha + 2;
                qtdeColunas = relBean.getMapaListaItens().size() - 1;

                coluna = 0;
                /**
                 * TITULO DO LISTA *
                 */
                label = new Label(coluna, linha, relBean.getTitulo());
                label.setCellFormat(subCabecalhoPrincipal);
                sheet.addCell(label);
                sheet.mergeCells(coluna, linha, qtdeColunas, linha + 1);
                linha++;
                linha++;
                coluna = 0;

                //CABEÃ‡ALHO-COLUNAS
                cabecalhosColuna = montaCabecalhoColunasOrdenado(relBean);
                for (CabecalhoColunaExcel cabecalhoColuna : cabecalhosColuna) {
                    label = new Label(coluna, linha, cabecalhoColuna.getNomeCabecalhoColuna());
                    sheet.addCell(label);
                    label.setCellFormat(cabecalho);
                    coluna++;
                }

                linha++;

                String nomeColuna;
                String[] caminho;
                Object itemColuna;
                //ITENS
                if (relBean.getItens() != null) {
                    for (Object item : relBean.getItens()) {
                        coluna = 0;
                        //PREENCHE OS VALORES DAS COLUNAS COM OS VALORES DO OBJETO
                        for (CabecalhoColunaExcel cabecalhoColuna : cabecalhosColuna) {
                            nomeColuna = cabecalhoColuna.getChave();
                            itemColuna = item;

                            //PERCORE O CAMINHO ATE ACHAR O OBJETO
                            if (nomeColuna.contains(".")) {
                                caminho = nomeColuna.split("\\.");
                                for (String caminho1 : caminho) {
                                    if (!(itemColuna == null)) {
                                        itemColuna = PropertyUtils.getProperty(itemColuna, caminho1);
                                    }
                                }
                            } else {
                                itemColuna = PropertyUtils.getProperty(itemColuna, nomeColuna + "");
                            }
                            //NULL
                            if (itemColuna == null) {
                                label = new Label(coluna, linha, "");
                                label.setCellFormat(cellCenter);
                                sheet.addCell(label);
                                //STRING
                            } else if (itemColuna instanceof String) {
                                label = new Label(coluna, linha, itemColuna + "");
                                if ((itemColuna + "").length() < 100) {
                                    label.setCellFormat(cellString);
                                } else {
                                    label.setCellFormat(cellStringWrap);
                                    colunasWrap.put(coluna, coluna);
                                }
                                sheet.addCell(label);
                                //DATE
                            } else if (itemColuna instanceof Date) {
                                if (itemColuna instanceof Time) {
                                    dateCell = new DateTime(coluna, linha, (Date) itemColuna, timeFormat);
                                    dateCell.setCellFormat(timeFormat);
                                    sheet.addCell(dateCell);
                                } else {
                                    dateCell = new DateTime(coluna, linha, (Date) itemColuna, dateFormat);
                                    dateCell.setCellFormat(dateFormat);
                                    sheet.addCell(dateCell);
                                }
                                //DOUBLE
                            } else if (itemColuna instanceof Double) {
                                number = new jxl.write.Number(coluna, linha, (Double) itemColuna, numberFormat);
                                number.setCellFormat(cellDouble);
                                sheet.addCell(number);

                                if (relBean.getTemTotal() && relBean.getMapaTemTotal().get(cabecalhoColuna.getChave()) != null && (Boolean) relBean.getMapaTemTotal().get(cabecalhoColuna.getChave())) {
                                    if (valorTotal.get(cabecalhoColuna.getChave()) != null) {
                                        valorTotal.put(cabecalhoColuna.getChave(), (Double) valorTotal.get(cabecalhoColuna.getChave()) + (Double) itemColuna);
                                    } else {
                                        valorTotal.put(cabecalhoColuna.getChave(), (Double) itemColuna);
                                    }
                                }

                                //INTEGER
                            } else if (itemColuna instanceof Integer) {
                                number = new jxl.write.Number(coluna, linha, (Integer) itemColuna);
                                number.setCellFormat(cellInteger);
                                sheet.addCell(number);

                                if (relBean.getTemTotal() && relBean.getMapaTemTotal().get(cabecalhoColuna.getChave()) != null && (Boolean) relBean.getMapaTemTotal().get(cabecalhoColuna.getChave())) {
                                    if (valorTotal.get(cabecalhoColuna.getChave()) != null) {
                                        valorTotal.put(cabecalhoColuna.getChave(), (Integer) valorTotal.get(cabecalhoColuna.getChave()) + (Integer) itemColuna);
                                    } else {
                                        valorTotal.put(cabecalhoColuna.getChave(), (Integer) itemColuna);
                                    }
                                }
                            }
                            //PROXIMA COLUNA
                            coluna++;
                        }
                        //PROXIMA LINHA
                        linha++;
                    }
                    if (relBean.getTemTotal()) {
                        Object total;
                        coluna = 0;

                        //PREENCHE OS VALORES DAS COLUNAS COM OS VALORES DO OBJETO
                        for (CabecalhoColunaExcel cabecalhoColuna : cabecalhosColuna) {
                            total = valorTotal.get(cabecalhoColuna.getChave());

                            //NULL
                            if (total == null) {
                                label = new Label(coluna, linha, "");
                                label.setCellFormat(cellNullTotal);
                                sheet.addCell(label);
                                //DOUBLE
                            } else if (total instanceof Double) {
                                number = new jxl.write.Number(coluna, linha, (Double) total, numberFormat);
                                number.setCellFormat(cellDoubleTotal);
                                sheet.addCell(number);
                                //INTEGER
                            } else if (total instanceof Integer) {
                                label = new Label(coluna, linha, total + "");
                                label.setCellFormat(cellIntegerTotal);
                                sheet.addCell(label);
                            }
                            //PROXIMA COLUNA
                            coluna++;
                        }
                    }
                }
            }
            //TAMANHO DAS COLUNAS
            for (int i = 0; i < sheet.getColumns(); i++) {
                if (colunasWrap.size() > 0 && colunasWrap.containsValue(i)) {
                    sheet.setColumnView(i, cvSize22000);
                } else {
                    sheet.setColumnView(i, cvAutoSize);
                }
            }

            workbook.write();
            workbook.close();

        } catch (IOException | WriteException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return baos.toByteArray();
    }

    /**
     * Monta o relatorio no formato em EXCEL
     *
     * @param isHorizontal
     * @param relatorioBeans
     * @return byte[]
     */
    public byte[] gerarXLSMatriz(Boolean isHorizontal, RelatorioBean... relatorioBeans) {
        HashMap<String, Object> valorTotal = new HashMap<>();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            WritableFont arial12Bold = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
            DateFormat customDateFormat = new DateFormat("dd/MM/yyyy");

            //ESTILOS E FONTES
            WritableCellFormat dateFormat = new WritableCellFormat(customDateFormat);
            dateFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignment6 = Alignment.CENTRE;
            dateFormat.setAlignment(alignment6);

            DateFormat customTimeFormat = new DateFormat("HH:mm");
            WritableCellFormat timeFormat = new WritableCellFormat(customTimeFormat);
            timeFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            timeFormat.setAlignment(alignment6);

            WritableCellFormat cellString = new WritableCellFormat();
            cellString.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignment = Alignment.LEFT;
            cellString.setAlignment(alignment);

            WritableCellFormat cellStringWrap = new WritableCellFormat();
            cellStringWrap.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cellStringWrap.setAlignment(alignment);
            cellStringWrap.setWrap(true);

            WritableCellFormat cellInteger = new WritableCellFormat();
            cellInteger.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignmentInt = Alignment.RIGHT;
            cellInteger.setAlignment(alignmentInt);

            WritableCellFormat cellDouble = new WritableCellFormat();
            cellDouble.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignment1 = Alignment.RIGHT;
            cellDouble.setAlignment(alignment1);

            WritableCellFormat cellCenter = new WritableCellFormat();
            Alignment alignment7 = Alignment.CENTRE;
            cellCenter.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cellCenter.setAlignment(alignment7);

            WritableCellFormat titulo = new WritableCellFormat();
            Alignment alignment2 = Alignment.CENTRE;
            titulo.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            titulo.setAlignment(alignment2);

            WritableCellFormat cabecalho = new WritableCellFormat();
            Colour color = Colour.WHITE;
            cabecalho.setBackground(color);
            cabecalho.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cabecalho.setFont(arial12Bold);
            cabecalho.setWrap(true);
            cabecalho.setAlignment(Alignment.CENTRE);
            cabecalho.setVerticalAlignment(VerticalAlignment.CENTRE);

            WritableCellFormat cabecalhoPrincipal = new WritableCellFormat();
            Colour colorCab = Colour.GREY_40_PERCENT;
            cabecalhoPrincipal.setBackground(colorCab);
            cabecalhoPrincipal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cabecalhoPrincipal.setFont(arial12Bold);
            cabecalhoPrincipal.setWrap(true);
            cabecalhoPrincipal.setAlignment(Alignment.CENTRE);
            cabecalhoPrincipal.setVerticalAlignment(VerticalAlignment.CENTRE);

            WritableCellFormat subCabecalhoPrincipal = new WritableCellFormat();
            Colour colorSubCab = Colour.GRAY_25;
            subCabecalhoPrincipal.setBackground(colorSubCab);
            subCabecalhoPrincipal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            subCabecalhoPrincipal.setFont(arial12Bold);
            subCabecalhoPrincipal.setWrap(true);
            subCabecalhoPrincipal.setAlignment(Alignment.CENTRE);
            subCabecalhoPrincipal.setVerticalAlignment(VerticalAlignment.CENTRE);

            WritableCellFormat cabecalhoCenter = new WritableCellFormat();
            Alignment alignment8 = Alignment.CENTRE;
            Colour color1 = Colour.GREY_25_PERCENT;
            cabecalhoCenter.setBackground(color1);
            cabecalhoCenter.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cabecalhoCenter.setAlignment(alignment8);

            //Formata NÃºmero.
            NumberFormat custonNumberFormat = new NumberFormat("#,##0.00");
            WritableCellFormat numberFormat = new WritableCellFormat(custonNumberFormat);
            numberFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            numberFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

            //Formato para Totais.
            WritableFont arial10Bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

            WritableCellFormat cellIntegerTotal = new WritableCellFormat();
            Colour colorDoubleTotal = Colour.GREY_25_PERCENT;
            cellIntegerTotal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignmentIntTotal = Alignment.CENTRE;
            cellIntegerTotal.setAlignment(alignmentIntTotal);
            cellIntegerTotal.setFont(arial10Bold);
            cellIntegerTotal.setBackground(colorDoubleTotal);

            WritableCellFormat cellDoubleTotal = new WritableCellFormat(custonNumberFormat);
            Colour colorIntTotal = Colour.GREY_25_PERCENT;
            cellDoubleTotal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            Alignment alignmentDoubleTotal = Alignment.RIGHT;
            cellDoubleTotal.setAlignment(alignmentDoubleTotal);
            cellDoubleTotal.setFont(arial10Bold);
            cellDoubleTotal.setBackground(colorIntTotal);

            WritableCellFormat cellNullTotal = new WritableCellFormat();
            Colour colorNullTotal = Colour.GREY_25_PERCENT;
            cellNullTotal.setBackground(colorNullTotal);
            cellNullTotal.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
            cellNullTotal.setFont(arial10Bold);
            cellNullTotal.setAlignment(Alignment.CENTRE);
            cellNullTotal.setVerticalAlignment(VerticalAlignment.CENTRE);

            /**
             * CellView para Largura da coluna == auto size
             *
             */
            CellView cvAutoSize = new CellView();
            cvAutoSize.setAutosize(true);

            CellView cvSize22000 = new CellView();
            cvSize22000.setSize(22000);

            //Cria a Planilha
            WorkbookSettings ws = new WorkbookSettings();
            ws.setLocale(new Locale("pt", "BR"));
            WritableWorkbook workbook = Workbook.createWorkbook(baos);
            WritableSheet sheet;

            if (relatorioBeans[0].getTitulo() != null && relatorioBeans[0].getTitulo().length() > 30) {
                sheet = workbook.createSheet(relatorioBeans[0].getTitulo().substring(0, 27) + "...", 0);
            } else if (relatorioBeans[0].getTitulo() != null) {
                sheet = workbook.createSheet(relatorioBeans[0].getTitulo(), 0);
            } else {
                sheet = workbook.createSheet("RelatÃ³rio", 0);
            }

            int linha = 0;
            int coluna = 0;
            int linhaStartTable;
            int columStartTable;
            for (RelatorioBean relatorioBean : relatorioBeans) {
                int qtdeColunas = 0;
                if (isHorizontal) {
                    try {
                        for (int i = 0; i < relatorioBean.getListaRelatorioBeanItens().size(); i++) {
                            qtdeColunas += ((RelatorioBean) relatorioBean.getListaRelatorioBeanItens().get(i)).getMapaListaItens().size();
                            //Soma mais dois porque Ã© o spaÃ§o entre as colunas
                            qtdeColunas += 2;
                        }
                        qtdeColunas = qtdeColunas - 3;
                    } catch (Exception eex) {
                        qtdeColunas = 10;
                    }
                } else {
                    if (relatorioBean.getListaRelatorioBeanItens() != null && relatorioBean.getListaRelatorioBeanItens().size() > 0) {
                        qtdeColunas = ((RelatorioBean) relatorioBean.getListaRelatorioBeanItens().get(0)).getMapaListaItens().size() - 1;
                    } else {
                        qtdeColunas = 1;
                    }
                }
                Label label;
                jxl.write.Number number;
                DateTime dateCell;

                /**
                 * TITULO DO RELATÃ“RIO *
                 */
                label = new Label(coluna, linha, relatorioBean.getTitulo().toUpperCase());
                label.setCellFormat(cabecalhoPrincipal);
                sheet.addCell(label);
                sheet.mergeCells(coluna, linha, coluna + qtdeColunas, linha + 1);
                linha = linha + 2;

                //LINHA EM BRANCO
                label = new Label(coluna, linha, "");
                label.setCellFormat(cellString);
                sheet.addCell(label);
                sheet.mergeCells(coluna, linha, coluna + qtdeColunas, linha);
                linha++;

                List<FiltroExcel> filtrosExcel;
                if (relatorioBean.getMapaFiltro() != null) {
                    filtrosExcel = montaFiltroExcelOrdenadoMultiplasListas(relatorioBean);

                    //FILTROS
                    for (FiltroExcel filtroExcel : filtrosExcel) {
                        label = new Label(coluna, linha, filtroExcel.getDescricao() + ": " + filtroExcel.getValor());
                        label.setCellFormat(cellString);
                        sheet.addCell(label);
                        sheet.mergeCells(coluna, linha, coluna + qtdeColunas, linha);
                        linha++;
                    }

                    //SE TIVER FILTRO IMPRIME LINHA EM BRANCO
                    if (filtrosExcel != null && filtrosExcel.size() > 0) {
                        label = new Label(coluna, linha, "");
                        label.setCellFormat(cellString);
                        sheet.addCell(label);
                        sheet.mergeCells(coluna, linha, coluna + qtdeColunas, linha);
                        linha++;
                    }
                }

                HashMap<Integer, Integer> colunasWrap = new HashMap<>();
                List<CabecalhoColunaExcel> cabecalhosColuna;

                /**/
                linha = linha - 2;
                //Monta as Listas comforme as tabela
                for (Object relBean1 : relatorioBean.getListaRelatorioBeanItens()) {
                    RelatorioBean relBean = (RelatorioBean) relBean1;
                    linhaStartTable = linha;
                    columStartTable = coluna;

                    //Adiciona um espaÃ§o para separar as tabelas
                    linha = linha + 2;
                    qtdeColunas = relBean.getMapaListaItens().size() - 1;

                    /**
                     * TITULO DO LISTA *
                     */
                    sheet.mergeCells(coluna, linha, coluna + qtdeColunas, linha);
                    label = new Label(coluna, linha, relBean.getTitulo());
                    label.setCellFormat(subCabecalhoPrincipal);
                    sheet.addCell(label);
                    linha++;

                    //CABEÃ‡ALHO-COLUNAS
                    cabecalhosColuna = montaCabecalhoColunasOrdenado(relBean);
                    for (CabecalhoColunaExcel cabecalhoColuna : cabecalhosColuna) {
                        label = new Label(coluna, linha, cabecalhoColuna.getNomeCabecalhoColuna());
                        sheet.addCell(label);
                        label.setCellFormat(cabecalho);
                        coluna++;
                    }

                    linha++;

                    String nomeColuna;
                    String[] caminho;
                    Object itemColuna;
                    //ITENS
                    if (relBean.getItens() != null) {
                        for (Object item : relBean.getItens()) {
                            coluna = columStartTable;
                            //PREENCHE OS VALORES DAS COLUNAS COM OS VALORES DO OBJETO
                            for (CabecalhoColunaExcel cabecalhoColuna : cabecalhosColuna) {
                                nomeColuna = cabecalhoColuna.getChave();
                                itemColuna = item;

                                //PERCORE O CAMINHO ATE ACHAR O OBJETO
                                if (nomeColuna.contains(".")) {
                                    caminho = nomeColuna.split("\\.");
                                    for (String caminho1 : caminho) {
                                        if (!(itemColuna == null)) {
                                            itemColuna = PropertyUtils.getProperty(itemColuna, caminho1);
                                        }
                                    }
                                } else {
                                    itemColuna = PropertyUtils.getProperty(itemColuna, nomeColuna + "");
                                }
                                //NULL
                                if (itemColuna == null) {
                                    label = new Label(coluna, linha, "");
                                    label.setCellFormat(cellCenter);
                                    sheet.addCell(label);
                                    //STRING
                                } else if (itemColuna instanceof String) {
                                    label = new Label(coluna, linha, itemColuna + "");
                                    if ((itemColuna + "").length() < 100) {
                                        label.setCellFormat(cellString);
                                    } else {
                                        label.setCellFormat(cellStringWrap);
                                        colunasWrap.put(coluna, coluna);
                                    }
                                    sheet.addCell(label);
                                    //DATE
                                } else if (itemColuna instanceof Date) {
                                    if (itemColuna instanceof Time) {
                                        dateCell = new DateTime(coluna, linha, (Date) itemColuna, timeFormat);
                                        dateCell.setCellFormat(timeFormat);
                                        sheet.addCell(dateCell);
                                    } else {
                                        dateCell = new DateTime(coluna, linha, (Date) itemColuna, dateFormat);
                                        dateCell.setCellFormat(dateFormat);
                                        sheet.addCell(dateCell);
                                    }
                                    //DOUBLE
                                } else if (itemColuna instanceof Double) {
                                    number = new jxl.write.Number(coluna, linha, (Double) itemColuna, numberFormat);
                                    number.setCellFormat(cellDouble);
                                    sheet.addCell(number);

                                    if (relBean.getTemTotal() && relBean.getMapaTemTotal().get(cabecalhoColuna.getChave()) != null && (Boolean) relBean.getMapaTemTotal().get(cabecalhoColuna.getChave())) {
                                        if (valorTotal.get(cabecalhoColuna.getChave()) != null) {
                                            valorTotal.put(cabecalhoColuna.getChave(), (Double) valorTotal.get(cabecalhoColuna.getChave()) + (Double) itemColuna);
                                        } else {
                                            valorTotal.put(cabecalhoColuna.getChave(), (Double) itemColuna);
                                        }
                                    }

                                    //INTEGER
                                } else if (itemColuna instanceof Integer) {
                                    number = new jxl.write.Number(coluna, linha, (Integer) itemColuna);
                                    number.setCellFormat(cellInteger);
                                    sheet.addCell(number);

                                    if (relBean.getTemTotal() && relBean.getMapaTemTotal().get(cabecalhoColuna.getChave()) != null && (Boolean) relBean.getMapaTemTotal().get(cabecalhoColuna.getChave())) {
                                        if (valorTotal.get(cabecalhoColuna.getChave()) != null) {
                                            valorTotal.put(cabecalhoColuna.getChave(), (Integer) valorTotal.get(cabecalhoColuna.getChave()) + (Integer) itemColuna);
                                        } else {
                                            valorTotal.put(cabecalhoColuna.getChave(), (Integer) itemColuna);
                                        }
                                    }
                                }
                                //PROXIMA COLUNA
                                coluna++;
                            }
                            //PROXIMA LINHA
                            linha++;
                        }
                        if (relBean.getTemTotal()) {
                            Object total;
                            coluna = columStartTable;

                            //PREENCHE OS VALORES DAS COLUNAS COM OS VALORES DO OBJETO
                            for (CabecalhoColunaExcel cabecalhoColuna : cabecalhosColuna) {
                                total = valorTotal.get(cabecalhoColuna.getChave());

                                //NULL
                                if (total == null) {
                                    label = new Label(coluna, linha, "");
                                    label.setCellFormat(cellNullTotal);
                                    sheet.addCell(label);
                                    //DOUBLE
                                } else if (total instanceof Double) {
                                    number = new jxl.write.Number(coluna, linha, (Double) total, numberFormat);
                                    number.setCellFormat(cellDoubleTotal);
                                    sheet.addCell(number);
                                    //INTEGER
                                } else if (total instanceof Integer) {
                                    label = new Label(coluna, linha, total + "");
                                    label.setCellFormat(cellIntegerTotal);
                                    sheet.addCell(label);
                                }
                                //PROXIMA COLUNA
                                coluna++;
                            }
                        }
                    }
                    if (!relatorioBean.getListaRelatorioBeanItens().get(relatorioBean.getListaRelatorioBeanItens().size() - 1).equals(relBean)) {
                        if (isHorizontal) {
                            linha = linhaStartTable;
                            coluna = coluna + 2;
                        } else {
                            coluna = columStartTable;
                        }
                    }
                }//FIM FOR LISTA INTERNA

                if (isHorizontal) {
                    linha = linha + 4;
                    coluna = 0;
                } else {
                    linha = 0;
                    coluna = coluna + 2;
                }

                //TAMANHO DAS COLUNAS
                for (int i = 0; i < sheet.getColumns(); i++) {
                    if (colunasWrap.size() > 0 && colunasWrap.containsValue(i)) {
                        sheet.setColumnView(i, cvSize22000);
                    } else {
                        sheet.setColumnView(i, cvAutoSize);
                    }
                }

            }//FIM FOR RELATORIO BEAN
            workbook.write();
            workbook.close();

        } catch (WriteException | IOException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return baos.toByteArray();
    }

    private List<CabecalhoColunaExcel> montaCabecalhoColunasOrdenado(RelatorioBean relatorioBean) {
        List<CabecalhoColunaExcel> cabecalhoColunas = new ArrayList<>();
        try {
            if (relatorioBean.getMapaListaItens() != null) {
                //CABEÃ‡ALHO-COLUNAS
                for (Iterator it = relatorioBean.getMapaListaItens().keySet().iterator(); it.hasNext(); ) {
                    Object object = it.next();

                    String[] cabecalho = relatorioBean.getMapaListaItens().get(object).toString().split(":");

                    cabecalhoColunas.add(new CabecalhoColunaExcel(new Integer(cabecalho[0]), cabecalho[1], object.toString()));
                }

                Collections.sort(cabecalhoColunas, new Comparator() {

                    @Override
                    public int compare(Object o1, Object o2) {
                        int order;
                        CabecalhoColunaExcel bean1 = (CabecalhoColunaExcel) o1;
                        CabecalhoColunaExcel bean2 = (CabecalhoColunaExcel) o2;

                        order = ((bean1.getIndice() == null || bean2.getIndice() == null) ? 1
                                : bean1.getIndice() > bean2.getIndice() ? 1
                                : bean1.getIndice() < bean2.getIndice() ? -1 : 0);

                        return order;
                    }
                });

            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return cabecalhoColunas;
    }

    private List<FiltroExcel> montaFiltroExcelOrdenado(RelatorioBean relatorioBean) {
        List<FiltroExcel> filtrosExcel = new ArrayList<>();
        try {
            if (relatorioBean.getMapaListaItens() != null) {
                //CABEÃ‡ALHO-COLUNAS
                if (relatorioBean.getMapaFiltro() != null) {
                    for (Iterator it = relatorioBean.getMapaFiltro().keySet().iterator(); it.hasNext(); ) {
                        Object object = it.next();

                        String[] chave = object.toString().split("\\:");

                        filtrosExcel.add(new FiltroExcel(new Integer(chave[0]), chave[1], relatorioBean.getMapaFiltro().get(object).toString()));
                    }
                }

                Collections.sort(filtrosExcel, new Comparator() {

                    @Override
                    public int compare(Object o1, Object o2) {
                        int order;
                        FiltroExcel bean1 = (FiltroExcel) o1;
                        FiltroExcel bean2 = (FiltroExcel) o2;

                        order = ((bean1.getIndice() == null || bean2.getIndice() == null) ? 1
                                : bean1.getIndice() > bean2.getIndice() ? 1
                                : bean1.getIndice() < bean2.getIndice() ? -1 : 0);

                        return order;
                    }
                });

            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return filtrosExcel;
    }

    private List<FiltroExcel> montaFiltroExcelOrdenadoMultiplasListas(RelatorioBean relatorioBean) {
        List<FiltroExcel> filtrosExcel = new ArrayList<>();
        try {
            if (relatorioBean.getListaRelatorioBeanItens() != null && relatorioBean.getListaRelatorioBeanItens().size() > 0) {
                //CABEÃ‡ALHO-COLUNAS
                for (Iterator it = relatorioBean.getMapaFiltro().keySet().iterator(); it.hasNext(); ) {
                    Object object = it.next();

                    String[] chave = object.toString().split("\\:");

                    filtrosExcel.add(new FiltroExcel(new Integer(chave[0]), chave[1], relatorioBean.getMapaFiltro().get(object).toString()));
                }

                Collections.sort(filtrosExcel, new Comparator() {

                    @Override
                    public int compare(Object o1, Object o2) {
                        int order;
                        FiltroExcel bean1 = (FiltroExcel) o1;
                        FiltroExcel bean2 = (FiltroExcel) o2;

                        order = ((bean1.getIndice() == null || bean2.getIndice() == null) ? 1
                                : bean1.getIndice() > bean2.getIndice() ? 1
                                : bean1.getIndice() < bean2.getIndice() ? -1 : 0);

                        return order;
                    }
                });

            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return filtrosExcel;
    }

    /**
     * @param relatorioBean
     * @return
     */
    private byte[] geraPdf(RelatorioBean relatorioBean) {
        ByteArrayOutputStream streamDeSaida;

        Connection conn;

        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            conn = session.connection();

            if (conn == null || conn.isClosed()) {
                session.reconnect();
                conn = session.connection();
            }

            String reportPath = relatorioBean.getCaminhoJasper() + relatorioBean.getNome();

            // criando a impressora Jasper a partir do caminho para o arquivo .jasper,
            // dos atributos (um Map que pode ser passado ao retalorio e cujos valores podem ser acessados como paramentros no relatorio)
            // e da DataSource (definida a partir da Colecao dados)
            JasperPrint impressoraJasper = JasperFillManager.fillReport(reportPath, relatorioBean.getParameters(), conn);

            // pegando o stream que serÃƒÂ¡ utilizado na saÃƒÂ­da
            streamDeSaida = new ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, impressoraJasper);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, streamDeSaida);

            exporter.exportReport();
            return streamDeSaida.toByteArray();

        } catch (HibernateException | SQLException | JRException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param relatorioBean
     * @return
     * @author Marlos Morbis Novo
     */
    private byte[] geraPdfCollectionDataSource(RelatorioBean relatorioBean) {
        ByteArrayOutputStream streamDeSaida;

        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();

            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource((List<Object>) relatorioBean.getListaJasperBeanCollection());

            String reportPath = relatorioBean.getCaminhoJasper() + relatorioBean.getNome();

            // criando a impressora Jasper a partir do caminho para o arquivo .jasper,
            // dos atributos (um Map que pode ser passado ao retalorio e cujos valores podem ser acessados como paramentros no relatorio)
            // e da DataSource (definida a partir da Colecao dados)
            JasperPrint impressoraJasper = JasperFillManager.fillReport(reportPath, relatorioBean.getParameters(), dataSource);

            // pegando o stream que serÃƒÂ¡ utilizado na saÃƒÂ­da
            streamDeSaida = new ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, impressoraJasper);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, streamDeSaida);

            exporter.exportReport();

            return streamDeSaida.toByteArray();

        } catch (HibernateException | JRException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param relatorioBean
     * @return
     */
    private byte[] geraPdfMultiplosItens(RelatorioBean relatorioBean) {
        ByteArrayOutputStream streamDeSaida;
        Transaction trx = null;
        Connection conn;

        try {
            if (relatorioBean != null && relatorioBean.getListaRelatorioBeanItens() != null) {

                Session session = HibernateUtil.getSessionFactory().getCurrentSession();
                trx = session.beginTransaction();
                conn = session.connection();

                if (conn == null || conn.isClosed()) {
                    session.reconnect();
                    conn = session.connection();
                }

                List<JasperPrint> listaJasperPrint = new ArrayList<>();

                //Gera um JasperPrint para cada item da lista
                for (Object relBean1 : relatorioBean.getListaRelatorioBeanItens()) {
                    RelatorioBean rBean = (RelatorioBean) relBean1;

                    JasperPrint impressoraJasper;
                    String reportPath = rBean.getCaminhoJasper() + rBean.getNome();

                    if (rBean.getListaJasperBeanCollection() != null) {
                        //cria a impressora Jasper a partir do caminho para o arquivo .jasper, dos parÃ¢metros e da DataSource
                        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource((List<Object>) rBean.getListaJasperBeanCollection());
                        impressoraJasper = JasperFillManager.fillReport(reportPath, rBean.getParameters(), dataSource);
                    } else {
                        //cria a impressora Jasper a partir do caminho para o arquivo .jasper, dos parÃ¢metros e o Connection
                        impressoraJasper = JasperFillManager.fillReport(reportPath, rBean.getParameters(), conn);
                    }
                    listaJasperPrint.add(impressoraJasper);
                }

                //gera o stream que serÃ¡ utilizado na saÃ­da
                streamDeSaida = new ByteArrayOutputStream();
                //exporta os JasperPrint's gerados para um Ãºnico PDF
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, listaJasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, streamDeSaida);

                exporter.exportReport();

                trx.commit();

                return streamDeSaida.toByteArray();
            }
        } catch (HibernateException | SQLException | JRException e) {
            if (trx != null && trx.isActive()) {
                trx.rollback();
            }
            e.printStackTrace();
        }
        return null;
    }

    private byte[] geraWord(RelatorioBean relatorioBean) {
        ByteArrayOutputStream streamDeSaida;

        Connection conn;

        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            conn = session.connection();

            String reportPath = relatorioBean.getCaminhoJasper() + relatorioBean.getNome();

            // criando a impressora Jasper a partir do caminho para o arquivo .jasper,
            // dos atributos (um Map que pode ser passado ao retalorio e cujos valores podem ser acessados como paramentros no relatorio)
            // e da DataSource (definida a partir da Colecao dados)
            JasperPrint impressoraJasper = JasperFillManager.fillReport(reportPath, relatorioBean.getParameters(), conn);

            // pegando o stream que serÃƒÂ¡ utilizado na saÃƒÂ­da
            streamDeSaida = new ByteArrayOutputStream();

            JRRtfExporter exporter = new JRRtfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, impressoraJasper);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, streamDeSaida);

            exporter.exportReport();
            return streamDeSaida.toByteArray();

        } catch (HibernateException | JRException e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] geraExcelforFlex(RelatorioBean relatorioBean) {
        ByteArrayOutputStream streamDeSaida;

        Connection conn;

        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            conn = session.connection();

            String reportPath = relatorioBean.getCaminhoJasper() + relatorioBean.getNome();

            // criando a impressora Jasper a partir do caminho para o arquivo .jasper,
            // dos atributos (um Map que pode ser passado ao retalorio e cujos valores podem ser acessados como paramentros no relatorio)
            // e da DataSource (definida a partir da Colecao dados)
            JasperPrint impressoraJasper = JasperFillManager.fillReport(reportPath, relatorioBean.getParameters(), conn);

            // pegando o stream que serÃ¡Â¡ utilizado na saÃ­da
            streamDeSaida = new ByteArrayOutputStream();

            JRXlsExporter exporter = new JRXlsExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, impressoraJasper);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, streamDeSaida);

            exporter.exportReport();
            return streamDeSaida.toByteArray();

        } catch (HibernateException | JRException e) {
            e.printStackTrace();
        }
        return null;
    }

    public WritableCellFormat montaStyle(WritableCellFormat cellFormat, RelatorioVo vo, String format) throws WriteException {
        WritableFont.FontName fontName;
        if (vo.getFontName() != null) {
            fontName = WritableFont.createFont(vo.getFontName());
        } else {
            fontName = WritableFont.ARIAL;
        }

        //SETA FORMATO DEFAULT
        WritableFont wf = new WritableFont(fontName, WritableFont.DEFAULT_POINT_SIZE, WritableFont.NO_BOLD);
        if (vo.getBold()) {
            wf.setBoldStyle(WritableFont.BOLD);
        }
        if (vo.getFontSize() > 0) {
            wf.setPointSize(vo.getFontSize());
        }
        if (vo.getFontColor() != null) {
            wf.setColour(vo.getFontColor());
        }
        wf.setItalic(vo.getItalic());

        //MONTA FORMAT
        WritableCellFormat cf;
        if (format != null && format.equals("#,##0.00")) {
            //Formata NÃºmero.
            NumberFormat custonNumberFormat = new NumberFormat(format);
            cf = new WritableCellFormat(wf, custonNumberFormat);
        } else if (cellFormat.getDateFormat() != null) {
            DateFormat customDateFormat = new DateFormat(format);
            cf = new WritableCellFormat(wf, customDateFormat);
        } else {
            cf = new WritableCellFormat(wf);
        }
        cf.setAlignment(cellFormat.getAlignment());
        cf.setBackground(cellFormat.getBackgroundColour());
        cf.setIndentation(cellFormat.getIndentation());
        cf.setOrientation(cellFormat.getOrientation());
        cf.setVerticalAlignment(cellFormat.getVerticalAlignment());
        cf.setWrap(cellFormat.getWrap());
        cf.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
        cf.setBackground(Colour.WHITE);
        return cf;
    }

    public WritableCellFormat montaStyleDate(WritableCellFormat cellFormat, RelatorioVo vo, String format) throws WriteException {
        WritableFont.FontName fontName;
        if (vo.getFontName() != null) {
            fontName = WritableFont.createFont(vo.getFontName());
        } else {
            fontName = WritableFont.ARIAL;
        }

        //SETA FORMATO DEFAULT
        WritableFont wf = new WritableFont(fontName, WritableFont.DEFAULT_POINT_SIZE, WritableFont.NO_BOLD);
        if (vo.getBold()) {
            wf.setBoldStyle(WritableFont.BOLD);
        }
        if (vo.getFontSize() > 0) {
            wf.setPointSize(vo.getFontSize());
        }
        if (vo.getFontColor() != null) {
            wf.setColour(vo.getFontColor());
        }
        wf.setItalic(vo.getItalic());
        //wf.setColour(Colour.BLACK);

        //MONTA FORMAT
        WritableCellFormat cf;

        DateFormat customDateFormat = new DateFormat(format);
        cf = new WritableCellFormat(wf, customDateFormat);

        cf.setAlignment(cellFormat.getAlignment());
        cf.setBackground(cellFormat.getBackgroundColour());
        cf.setIndentation(cellFormat.getIndentation());
        cf.setOrientation(cellFormat.getOrientation());
        cf.setVerticalAlignment(cellFormat.getVerticalAlignment());
        cf.setWrap(cellFormat.getWrap());
        cf.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
        cf.setBackground(Colour.WHITE);
        return cf;
    }

    private WritableCellFormat getStyle(WritableCellFormat cellFormat, RelatorioVo vo, String format, boolean isDate) {
        WritableCellFormat writableCellFormat = new WritableCellFormat();

        String styleKey = "";

        if (vo.getFontName() != null && !vo.getFontName().equalsIgnoreCase("")) {
            styleKey += "" + vo.getFontName();
        } else {
            styleKey += "none";
        }

        if (vo.getFontSize() > 0) {
            styleKey += "|" + vo.getFontSize();
        } else {
            styleKey += "|none";
        }

        if (vo.getBold()) {
            styleKey += "|true";
        } else {
            styleKey += "|false";
        }

        if (vo.getItalic()) {
            styleKey += "|true";
        } else {
            styleKey += "|false";
        }

        if (vo.getFontColor() != null) {
            styleKey += "|" + vo.getFontColor().getValue();
        } else {
            styleKey += "|none";
        }

        if (format != null && !format.equalsIgnoreCase("")) {
            styleKey += "|" + format;
        } else {
            styleKey += "|none";
        }

        if (isDate) {
            styleKey += "|isDate";
        } else {
            styleKey += "|notDate";
        }

        if (hashMapStyles.get(styleKey) != null) {
            writableCellFormat = hashMapStyles.get(styleKey);
        } else {
            try {
                if (isDate) {
                    writableCellFormat = montaStyleDate(cellFormat, vo, format);
                } else {
                    writableCellFormat = montaStyle(cellFormat, vo, format);
                }
            } catch (WriteException e) {
                e.printStackTrace();
            }
            hashMapStyles.put(styleKey, writableCellFormat);
        }

        return writableCellFormat;
    }
}
