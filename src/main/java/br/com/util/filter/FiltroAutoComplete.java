package br.com.util.filter;

/**
 *
 * <p>Classe responsável por armazenar informações para filtro em autocomplete's
 * onde se tem duas propriedades;
 *
 * <pre>
 * - strFiltro : String onde irá vir o valor texto para filtro no autocomplete.
 * - limite : limitador para a consulta do auto complete.
 * </pre>
 *
 */
public class FiltroAutoComplete {

    private String strFiltro;
    private int limite;
    private Object objetoFiltro;

    public Object getObjetoFiltro() {
        return objetoFiltro;
    }

    public void setObjetoFiltro(Object objetoFiltro) {
        this.objetoFiltro = objetoFiltro;
    }

    public int getLimite() {
        return limite;
    }

    public void setLimite(int limite) {
        this.limite = limite;
    }

    public String getStrFiltro() {
        return strFiltro;
    }

    public void setStrFiltro(String strFiltro) {
        this.strFiltro = strFiltro;
    }

    @Override
    public String toString() {
        return "FiltroAutoComplete{" + "strFiltro=" + strFiltro + '}';
    }
    
    
   
}
