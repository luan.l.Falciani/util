package br.com.util.filter;

import br.com.persistence_2.vo.PaginacaoVo;

/**
 *  filtro com paginaçao vo dentro passar no extends para não necescitar criar um paginação em todos os filtros
 * @author Luan L F Domingues
 */
public class FiltroGeral {

    private PaginacaoVo paginacaoVo;

    public PaginacaoVo getPaginacaoVo() {
        return paginacaoVo;
    }

    public void setPaginacaoVo(PaginacaoVo paginacaoVo) {
        this.paginacaoVo = paginacaoVo;
    }

}
