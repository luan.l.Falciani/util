
package br.com.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Extenso {

    public static final String[][] UNIT_REAL = new String[][]{{"Real", "Reais"}, {"Centavo", "Centavos"}};
    public static final String[][] UNIT_METER = new String[][]{{"Metro", "Metros"}, {"Centímetro", "Centímetros"}};
    public static final String qualifiers[][] = {{"mil", "mil"}, {"milhão", "milhões"}, {"bilhão", "bilhões"}, {"trilhão", "trilhões"}, {"quatrilhão", "quatrilhões"}, {"quintilião", "quintiliões"}, {"sextilião", "sextiliões"}, {"septilião", "septiliões"}, {"octilião", "octiliões"}, {"nonilião", "noniliões"}};
    public static final String numerals[][] = {{"zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "catorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"}, {"vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"}, {"cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"}};
    public static final String conjunction_and = "e";
    public static final String conjunction_of = "de";
    private final int decimals;
    private String[][] unit = UNIT_REAL;

    public Extenso(int decimals, String[][] unit) {
        this(decimals);
        this.unit = unit;
    }

    public Extenso(int decimals) {
        this.decimals = decimals;
    }

    public String toExtenso(BigDecimal value) {
        String s = "";
        try {
            value = value.abs().setScale(decimals, RoundingMode.HALF_EVEN);
            BigInteger integer_part = value.abs().toBigInteger();
            BigInteger decimal_part = ((value.subtract(new BigDecimal(integer_part))).multiply(new BigDecimal(10).pow(decimals))).toBigInteger();
            if (integer_part.doubleValue() > 0) {
                s += ordersToString(getOrders(integer_part), unit[0]);
            }
            if (decimal_part.doubleValue() > 0) {
                s += s.length() > 0 ? " " + conjunction_and + " " : "";
                s += ordersToString(getOrders(decimal_part), unit[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    private List<Integer> getOrders(BigInteger value) {
        List<Integer> orders = new ArrayList();
        try {
            while (value.doubleValue() >= 1000) {
                orders.add(value.remainder(BigInteger.valueOf(1000)).intValue());
                value = value.divide(BigInteger.valueOf(1000));
            }
            orders.add(value.intValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return orders;
    }

    private String ordersToString(List<Integer> orders, String[] unit) {
        String s = "";
        try {
            Integer order0 = orders.get(0);
            if (order0 > 0) {
                s += orderToString(order0);
            }
            int last = 0;
            for (int i = 1; i < orders.size(); i++) {
                Integer value = orders.get(i);
                if (value != 0) {
                    String q = qualifiers.length >= i ? qualifiers[i - 1][value > 1 ? 1 : 0] : "?";
                    if (i >= 2 && last == 0 && order0 == 0) {
                        q += " " + conjunction_of + " ";
                    } else if (last == 0 && order0 > 0 && (order0 < 100 || order0 % 100 == 0)) {
                        q += " " + conjunction_and + " ";
                    } else if (i >= 2) {
                        q += ", ";
                    } else {
                        q += " ";
                    }
                    if (i == 1 && value == 1) {
                        s = q + s;
                    } else {
                        s = orderToString(value) + q + s;
                    }
                    last = i;
                }
            }
            if (orders.size() == 1 && order0 == 1) {
                s += unit[0];
            } else {
                s += unit[1];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    private String orderToString(Integer value) {
        String s = "";
        try {
            if (value < 20) {
                s += numerals[0][value] + " ";
            } else if (value < 100) {
                s += numerals[1][value / 10 - 2] + " ";
                if (value % 10 != 0) {
                    s += conjunction_and + " " + orderToString(value % 10);
                }
            } else if (value == 100) {
                s += numerals[2][0] + " ";
            } else if (value < 1000) {
                s += numerals[2][value / 100] + " ";
                if (value % 100 != 0) {
                    s += conjunction_and + " " + orderToString(value % 100);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }
}
