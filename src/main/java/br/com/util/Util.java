package br.com.util;

import br.com.persistence_2.classes.Filtro;
import br.com.persistence_2.classes.NxCriterion;
import br.com.persistence_2.classes.NxOrder;
import br.com.persistence_2.classes.Propriedade;
import br.com.persistence_2.dao.GenericDao;
import br.com.util.json.GsonAdapter;
import br.com.util.vo.UsuarioAuditoriaVo;
import com.google.gson.Gson;
import org.hibernate.property.Getter;
import org.hibernate.property.PropertyAccessorFactory;
import org.hibernate.property.Setter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.modelmapper.ModelMapper;

import javax.imageio.ImageIO;
import javax.persistence.Temporal;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.text.MaskFormatter;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.*;
import java.util.*;
import java.util.List;
import java.util.regex.Pattern;

public class Util {

    public static final String CONFIG = "Config";
    private static final String VAR_AMBIENTE = "ambiente";
    private static final String VAR_EMPRESA = "Empresa";
    private static final ModelMapper modelMapper = new ModelMapper();

    /**
     * Complementa com espa�os � Esquerda at� o tamanho especificado.
     *
     * @param str:     String para ser formatada
     * @param tamanho: Tamanho que deve conter a string formatada
     * @return String
     */
    public static String formataEspacosEsquerda(String str, int tamanho) {
        if (str == null) {
            str = "";
        }
        String formatada = str.trim();
        for (int i = (formatada.length() + 1); i <= tamanho; i++) {
            formatada = " " + formatada;
        }

        return formatada;
    }

    /**
     * Complementa com espa�os � direita at� o tamanho especificado.
     *
     * @param str:     String para ser formatada
     * @param tamanho: Tamanho que deve conter a string formatada
     * @return String
     */
    public static String formataEspacosDireita(String str, int tamanho) {
        if (str == null) {
            str = "";
        }
        String formatada = str.trim();
        for (int i = (formatada.length() + 1); i <= tamanho; i++) {
            formatada = formatada + " ";
        }

        return formatada;
    }

    /**
     * Complementa com zeros � esquerda at� o tamanho especificado.
     *
     * @param str:     String para ser formatada
     * @param tamanho: Tamanho que deve conter a string formatada
     * @return String
     */
    public static String formataZerosEsquerda(String str, int tamanho) {
        if (str == null) {
            str = "";
        }
        String formatada = str.trim();
        for (int i = (formatada.length() + 1); i <= tamanho; i++) {
            formatada = "0" + formatada;
        }

        return formatada;
    }

    /**
     * Complementa com zeros � direita at� o tamanho especificado.
     *
     * @param str:     String para ser formatada
     * @param tamanho: Tamanho que deve conter a string formatada
     * @return String
     */
    public static String formataZerosDireita(String str, int tamanho) {
        if (str == null) {
            str = "";
        }
        String formatada = str.trim();
        for (int i = (formatada.length() + 1); i <= tamanho; i++) {
            formatada = formatada + "0";
        }

        return formatada;
    }

    /**
     * @param valor
     * @return String
     */
    public static String formataValor(String valor) {

        String valorFormatado;
        String decimal;
        String inteiro;

        int tamanho;

        if (valor != null) {
            valor = valor.trim();
            tamanho = valor.length();
            if (tamanho > 2) {
                //Retirando a virgula
                decimal = valor.substring(tamanho - 2);
                inteiro = valor.substring(0, tamanho - 3);

                inteiro = formataZerosEsquerda(inteiro, 13);
                decimal = formataEspacosEsquerda(decimal, 2);

                valorFormatado = inteiro + decimal;
            } else {
                valorFormatado = formataZerosEsquerda(valor, 15);
            }
        } else {
            valorFormatado = formataZerosEsquerda("0", 15);
        }

        return valorFormatado;
    }

    /**
     * Adiciona um dia na data recebida por parametro
     *
     * @param data
     * @return Date
     * @throws Exception
     */
    public static Date addDia(Date data) throws Exception {
        Calendar calendar = null;
        try {
            calendar = Calendar.getInstance();
            calendar.setTime(data);
            calendar.add(Calendar.DATE, +1);
        } catch (Exception e) {
            throw new Exception("Erro no método addDia(Date data): " + e);
        }

        return new Date(calendar.getTime().getTime());

    }

    /**
     * Retorna o primeiro dia do mês da data recebida por parâmetro.
     *
     * @param data
     * @return
     * @throws java.lang.Exception
     */
    public static Date primeiroDiaDoMes(Date data) throws Exception {
        Calendar calendar = null;

        try {
            calendar = Calendar.getInstance();
            calendar.setTime(data);

            calendar.set(calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.getMinimum(Calendar.DAY_OF_MONTH));
        } catch (Exception e) {
            throw new Exception("Erro no método primeiroDiaDoMes(Date data): " + e);
        }

        return new Date(calendar.getTime().getTime());
    }

    /**
     * Retorna o ultimo dia de cada mês, no intervalo de datas passado por
     * parâmetro. Menos do dataFim, que ao invés do ultimo dia do mês, retorna a
     * própria dataFim
     *
     * @param dataIni
     * @param dataFim
     * @return
     */
    public static List<Date> ultimoDiaDeCadaMes(Date dataIni, Date dataFim) {
        GregorianCalendar gc = new GregorianCalendar();
        GregorianCalendar gcAux = new GregorianCalendar();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        List<Date> listaDatas = new ArrayList<>();

        gc.setTime(dataIni);
        gcAux.setTime(dataFim);

        while (gc.getTime().before(dataFim)) {
            // Pega o ultimo dia do mês 'corrente' (na iteração sobre os meses)
            gc.set(GregorianCalendar.DAY_OF_MONTH, gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH));
            listaDatas.add(gc.getTime());

            gc.add(GregorianCalendar.MONTH, 1);

            // Se o mês do gc 'corrente' (da iteração) é igual ao mês do gcAux
            // Então pega o dia da dataFim, e não o ultimo da do mês da dataFim
            if (gc.get(GregorianCalendar.MONTH) == gcAux.get(GregorianCalendar.MONTH)
                    && gc.get(GregorianCalendar.YEAR) == gcAux.get(GregorianCalendar.YEAR)) {

                listaDatas.add(gcAux.getTime());
            }
        }
        return listaDatas;
    }

    public static boolean isBetween(Date dataInicio, Date dataFim, Date toCompare) {
        if (dataInicio == null) {
            return false;
        }

        if (dataFim == null) {
            return false;
        }

        if (toCompare == null) {
            return false;
        }

        return toCompare.compareTo(dataInicio) >= 0 && toCompare.compareTo(dataFim) <= 0;
    }

    /**
     * Retorna a data menor entre as informadas
     *
     * @param date1
     * @param date2
     * @return
     */
    public static Date minDate(Date date1, Date date2) {
        if (date1 == null && date2 != null) {
            return date2;
        } else if (date2 == null && date1 != null) {
            return date1;
        } else if (date1 != null && date2 != null) {
            if (date1.before(date2)) {
                return date1;
            } else {
                return date2;
            }
        }
        return null;
    }

    public static Date resetHour(Date date) {
        if (date == null) {
            return date;
        }
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
        gc.set(GregorianCalendar.MINUTE, 0);
        gc.set(GregorianCalendar.SECOND, 0);
        gc.set(GregorianCalendar.MILLISECOND, 0);
        return gc.getTime();
    }

    /**
     * Retorna a data maior entre as informadas
     *
     * @param date1
     * @param date2
     * @return
     */
    public static Date maxDate(Date date1, Date date2) {
        if (date1 == null && date2 != null) {
            return date2;
        } else if (date2 == null && date1 != null) {
            return date1;
        } else if (date1 != null && date2 != null) {
            if (date1.after(date2)) {
                return date1;
            } else {
                return date2;
            }
        }
        return null;
    }

    /**
     * Calcula a diferença de dias entre as datas
     *
     * @param dataLow
     * @param dataHigh
     * @return
     */
    public static int diferencaDias(java.util.Date dataLow, java.util.Date dataHigh) {

        GregorianCalendar startTime = new GregorianCalendar();
        GregorianCalendar endTime = new GregorianCalendar();

        GregorianCalendar curTime = new GregorianCalendar();
        GregorianCalendar baseTime = new GregorianCalendar();

        startTime.setTime(dataLow);
        endTime.setTime(dataHigh);

        int dif_multiplier;

        // Verifica a ordem de inicio das datas
        if (dataLow.compareTo(dataHigh) < 0) {
            baseTime.setTime(dataHigh);
            curTime.setTime(dataLow);
            dif_multiplier = 1;
        } else {
            baseTime.setTime(dataLow);
            curTime.setTime(dataHigh);
            dif_multiplier = -1;
        }

        int result_years = 0;
        int result_months = 0;
        int result_days = 0;

        // Para cada mes e ano, vai de mes em mes pegar o ultimo dia para import acumulando
        // no total de dias. Ja leva em consideracao ano bissesto
        while (curTime.get(GregorianCalendar.YEAR) < baseTime.get(GregorianCalendar.YEAR)
                || curTime.get(GregorianCalendar.MONTH) < baseTime.get(GregorianCalendar.MONTH)) {

            int max_day = curTime.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
            result_months += max_day;
            curTime.add(GregorianCalendar.MONTH, 1);

        }

        // Marca que é um saldo negativo ou positivo
        result_months = result_months * dif_multiplier;

        // Retirna a diferenca de dias do total dos meses
        result_days += (endTime.get(GregorianCalendar.DAY_OF_MONTH) - startTime.get(GregorianCalendar.DAY_OF_MONTH));

        return result_years + result_months + result_days;
    }

    /**
     * Calcula a diferença de meses entre as datas.
     *
     * @param dataLow
     * @param dataHigh
     * @return
     */
    public static int diferencaMeses(java.util.Date dataLow, java.util.Date dataHigh) {
        int diferencaDias = 0;
        try {
            //chama método de diferencaDias para contar quanto dias entre as duas datas informadas.
            diferencaDias = diferencaDias(dataLow, dataHigh);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //pega numero de dias e dividi por 30 dias que se tem em média um mês.
        return diferencaDias / 30;
    }

    /**
     * Calcula a diferença de meses entre as datas.
     *
     * @param dataLow
     * @param dataHigh
     * @return
     */
    public static int diferencaAnos(java.util.Date dataLow, java.util.Date dataHigh) {
        int diferencaDias = 0;
        try {
            //chama método de diferencaDias para contar quanto dias entre as duas datas informadas.
            diferencaDias = diferencaDias(dataLow, dataHigh);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //pega numero de dias e dividi por 365 dias que se tem o ano.
        return diferencaDias / 365;
    }

    /**
     * Adiciona nrDias na data recebida por parametro
     *
     * @param data
     * @param nrDias
     * @return data + nrDias
     * @throws Exception
     */
    public static Date addDia(Date data, int nrDias) throws Exception {
        Calendar calendar = null;
        try {
            calendar = Calendar.getInstance();
            calendar.setTime(data);
            calendar.add(Calendar.DATE, +nrDias);
        } catch (Exception e) {
            throw new Exception("Erro no m�todo addDia(Date data, int nrDias): " + e);
        }
        return new Date(calendar.getTime().getTime());
    }

    /**
     * Transforma uma String em um java.sql.Date
     * <p>
     * Em caso de erro na conversão retorna null;
     *
     * @param data
     * @param formato
     * @return dtHr
     * @throws Exception
     */
    public static Date parseDtHr(String data, String formato) throws Exception {

        Locale locale = Locale.ENGLISH;
        SimpleDateFormat sdf
                = new SimpleDateFormat(formato, locale);

        Date dtHr = null;

        try {
            if (data != null && !data.equals("")) {
                dtHr = new Date(sdf.parse(data).getTime());
            }
        } catch (ParseException e) {
            dtHr = null;
            e.printStackTrace();
        }

        return dtHr;
    }

    /**
     * Transforma uma String em um java.sql.Date
     *
     * @param data
     * @param formato
     * @return dtHr
     * @throws Exception
     */
    public static Timestamp parseDtHrTimeStamp(String data, String formato)
            throws Exception {

        Locale locale = Locale.ENGLISH;
        SimpleDateFormat sdf
                = new SimpleDateFormat(formato, locale);

        java.sql.Date dtHr = null;

        try {

            dtHr = new java.sql.Date(sdf.parse(data).getTime());

        } catch (ParseException e) {
            throw new Exception("Erro ao formatar data."
                    + "\n Classe: Util"
                    + "\n M�todo: public static Date parseDtHr(String data, "
                    + "String formato)"
                    + "\n Data = " + data + " Formato = " + formato
                    + "\n " + e.getMessage());
        }

        return new Timestamp(dtHr.getTime());
    }

    /**
     * Transforma uma String em data
     *
     * @param sData
     * @param formato
     * @return java.sql.Date
     * @throws Exception
     */
    public static Date parseData(String sData, String formato) throws Exception {
        //TESTE
        Locale locale = Locale.ENGLISH;
        SimpleDateFormat sdf = new SimpleDateFormat(formato, locale);

        Date data = null;

        try {
            data = new Date(sdf.parse(sData).getTime());

        } catch (ParseException e) {
            throw new Exception("Erro ao formatar data."
                    + "\n Classe: Util"
                    + "\n M�todo: public static Date parseData(String sData, "
                    + "String formato)"
                    + "\n Data = " + sData + " Formato = " + formato
                    + "\n " + e.getMessage());
        }

        return data;
    }

    /**
     * Transforma uma String em data
     *
     * @param sData
     * @return java.sql.Date
     * @throws Exception
     */
    public static Date parseData(String sData) throws Exception {
        return parseData(sData, "dd/MM/yyyyy");
    }

    /**
     * M�todo para formatar o valor e colocar as casas decimais.
     *
     * @param valorLcto
     * @return
     */
    public static BigDecimal formataValorLcto(String valorLcto) {

        int tamanho = valorLcto.length();

        valorLcto = valorLcto.substring(0, tamanho - 2)
                + "." + valorLcto.substring(tamanho - 2, tamanho);

        return new BigDecimal(valorLcto);
    }

    /**
     * M�todo para formatar o valor e colocar as casas decimais, com v�rgula.
     *
     * @param valorLcto
     * @return
     */
    public static String formataValorLctoVirgula(String valorLcto) {
        String valor = String.valueOf(Integer.parseInt(valorLcto));
        int tamanho = valor.length();

        valorLcto = valor.substring(0, tamanho - 2)
                + "," + valor.substring(tamanho - 2, tamanho);

        return valorLcto;
    }

    /**
     * Transforma uma String para Double, caso ocorra algum erro na conversão
     * retorna null
     *
     * @param valorString
     * @return double ou null(Caso ocorra algum erro na conversão)
     */
    public static Double parseStringToDouble(String valorString) {
        Double valor;
        try {
            valorString = valorString.replace(".", "");
            valorString = valorString.replace(",", ".");

            valor = new Double(valorString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            valor = null;
        }
        return valor;
    }

    /**
     * Transforma uma String para Integer, caso ocorra algum erro na conversão
     * retorna null
     *
     * @param valorString
     * @return Integer ou null(Caso ocorra algum erro na conversão)
     */
    public static Integer parseStringToInteger(String valorString) {
        Integer valor;
        try {
            valor = new Integer(valorString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            valor = null;
        }
        return valor;
    }

    /**
     * Recebe uma data do tipo String e a formata de acordo com o formato
     * recebido por par�metro
     *
     * @param data
     * @param formatoAtual
     * @param formatoEsperado
     * @return
     * @throws Exception
     */
    public static String formataData(String data, String formatoAtual,
                                     String formatoEsperado) throws Exception {

        SimpleDateFormat sfAtual = new SimpleDateFormat(formatoAtual);
        SimpleDateFormat sfEsperado = new SimpleDateFormat(formatoEsperado);

        java.util.Date dt = null;
        try {
            dt = sfAtual.parse(data);
        } catch (ParseException e) {
            throw new Exception("Erro ao formatar data."
                    + "\n Classe: Util"
                    + "\n M�todo: public static String formataData(String data, "
                    + "String formato) { "
                    + "\n Data = " + data + " Formato = " + formatoEsperado
                    + "\n " + e.getMessage());
        }
        return sfEsperado.format(dt);
    }

    /**
     * Transforma um Date em uma String no formato desejado.
     *
     * @param data
     * @param formato
     * @return
     * @throws Exception
     */
    public static String dateToString(Date data, String formato) throws Exception {

        Locale locale = Locale.ENGLISH;
        SimpleDateFormat sdf
                = new SimpleDateFormat(formato, locale);

        //N�o incrementa a data, se vir m�s 13 por exemplo o default mudaria
        //para o m�s 1 do pr�ximo ano
        //Utilizando sdf.setLenient(false) gera exce��o.
        sdf.setLenient(false);

        String dt = null;

        try {

            dt = sdf.format(data);

        } catch (Exception e) {
            throw new Exception("Erro ao formatar data."
                    + "\n Classe: Util"
                    + "\n Metodo: public static String dateToString(Date data, "
                    + "String formato)"
                    + "\n Data = " + data + " Formato = " + formato
                    + "\n " + e.getMessage());
        }

        return dt;
    }

    /**
     * Transforma um Date em uma String no formato desejado.
     *
     * @param data
     * @return
     * @throws Exception
     */
    public static String dateToString(Date data) throws Exception {
        return dateToString(data, "dd/MM/yyyy");
    }

    /**
     * Retorna o total de horas.
     *
     * @param cargaHorasTreinamento
     * @param cargaMinutosTreinamento
     * @return caso algum dos parametros seja null retorna 0;
     */
    public static Double calculaHorasTreinamento(Integer cargaHorasTreinamento, Integer cargaMinutosTreinamento) {
        Double totalHomensHora = new Double(0);
        double totalHoras;
        double totalMinutos;

        try {
            if (cargaHorasTreinamento != null && cargaMinutosTreinamento != null) {

                totalHoras = cargaHorasTreinamento;
                totalMinutos = cargaMinutosTreinamento;

                totalHomensHora = totalHoras;
                totalHomensHora = totalHomensHora + (totalMinutos / (double) 60);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return totalHomensHora;

    }

    /**
     * Metodo que obtem o valor antecedente ao divisor.
     *
     * @param string  String
     * @param divisor String
     * @return String
     */
    public static String obtemValor(String string, String divisor) {
        String valor = "";
        if (string != null && divisor != null) {
            int pos = string.indexOf(divisor);
            valor = string.substring(0, pos);
        }
        return valor;
    }

    /**
     * Metodo que obtem o valor antecedente ao divisor.
     *
     * @param string  String
     * @param divisor String
     * @return String
     */
    public static String obtemPosDivisor(String string, String divisor) {
        String valor = "";
        if (string != null && divisor != null) {
            int pos = string.indexOf(divisor);
            valor = string.substring(pos + 1, string.length());
        }
        return valor;
    }

    /**
     * Está função irá tratar a String recebida por parâmetro para deixá-la de
     * acordo com o que o Excel aceita para nome das planilhas.
     * <p>
     * Nr máximo de caractres 30. Não pode ter /, \, * e ?
     *
     * @param nomeSheet
     * @return nomeSheetTratado
     */
    public static String trataNomeSheet(String nomeSheet) {
        String nomeSheetTratado = new String();
        try {
            if (nomeSheet != null) {
                if (nomeSheet.length() > 30) {
                    nomeSheetTratado = nomeSheet.substring(30);
                } else {
                    nomeSheetTratado = nomeSheet;
                }
                nomeSheetTratado = nomeSheetTratado.replace("/", "-");
                nomeSheetTratado = nomeSheetTratado.replace("\\", "-");
                nomeSheetTratado = nomeSheetTratado.replace("?", "");
                nomeSheetTratado = nomeSheetTratado.replace("*", "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return nomeSheetTratado;
    }

    /**
     * Altera o número de casas decimais de acordo com os parâmetros.
     *
     * @param valor
     * @param numeroCasasDecimais
     * @return caso algum dos parametros seja null retorna 0;
     */
    public static Double formataCasasDecimais(Double valor, Integer numeroCasasDecimais) {
        Double valorFormatado = new Double(0);
        try {
            if (valor != null && numeroCasasDecimais != null) {
                NumberFormat f = NumberFormat.getInstance(Locale.US);
                f.setMaximumFractionDigits(numeroCasasDecimais);
                f.setMinimumFractionDigits(numeroCasasDecimais);
                //Elimina as vírgulas da string e transforma em double.
                valorFormatado = Double.valueOf(f.format(valor).replaceAll("[,]", ""));
            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return valorFormatado;

    }

    /**
     * Calcula a idade.
     *
     * @param dataNascimento
     * @return
     */
    public static Integer calculaIdade(Date dataNascimento) {
        Integer dias;
        Integer idade = 0;
        try {
            dias = diferencaDias(dataNascimento, new Date());

            if (dias > 0) {
                idade = dias / 365;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return idade;
    }

    /**
     * Retorna o ultimo dia do mês da data recebida por parâmetro.
     *
     * @param data
     * @return
     * @throws java.lang.Exception
     */
    public static Date ultimoDiaDoMes(Date data) throws Exception {
        Calendar calendar = null;

        try {
            calendar = Calendar.getInstance();
            calendar.setTime(data);

            calendar.set(calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        } catch (Exception e) {
            throw new Exception("Erro no método primeiroDiaDoMes(Date data): " + e);
        }

        return new Date(calendar.getTime().getTime());

    }

    /**
     * Monta a data a partir do ano mes e dia.
     *
     * @param dia
     * @param mes
     * @param ano
     * @return
     * @throws Exception
     */
    public static Date montaData(int dia, int mes, int ano) throws Exception {
        GregorianCalendar calendar = new GregorianCalendar();
        try {
            calendar.set(Calendar.DAY_OF_MONTH, dia);
            calendar.set(Calendar.YEAR, ano);
            //mes-1 por que se passar 2 ele trez março em vez de fevereiro
            calendar.set(Calendar.MONTH, mes - 1);

        } catch (Exception e) {
            throw new Exception("Erro no método montaData: " + e);
        }
        return new Date(calendar.getTime().getTime());
    }

    public static String removePontosCpf(String cpf) {
        String novoCpf = new String();

        try {
            if (cpf != null && !"".equals(cpf)) {
                novoCpf = cpf.replaceAll("[.-]", "");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return novoCpf;

    }

    /**
     * Adiciona nrDias úteis na data recebida por parametro
     *
     * @param data
     * @param nrDias
     * @return data + nrDias
     * @throws Exception
     */
    public static Date addDiaUtil(Date data, int nrDias) throws Exception {
        Calendar calendar = null;
        try {
            calendar = Calendar.getInstance();
            calendar.setTime(data);

            for (int i = 0; i < nrDias; i++) {

                if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                    calendar.add(Calendar.DATE, 3);

                } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                    calendar.add(Calendar.DATE, 2);

                } else {
                    calendar.add(Calendar.DATE, 1);
                }
            }

        } catch (Exception e) {
            throw new Exception("Erro no m�todo addDiaUtil(Date data, int nrDias): " + e);
        }
        return new Date(calendar.getTime().getTime());
    }

    public static Boolean parseBooleanString(String value) {
        return (value != null
                && (value.equalsIgnoreCase("true")
                || value.equals("1")
                || value.startsWith("S")
                || value.startsWith("s")
                || value.startsWith("y")
                || value.startsWith("Y")));
    }

    /**
     * Recebe um xmlGregorianCalendar e retorna um java.util.Ddate
     *
     * @param xmlGregorianCalendar
     * @return
     */
    public static Date xmlGregorianCalendarToDate(XMLGregorianCalendar xmlGregorianCalendar) {
        Date data = new Date();
        Calendar calendar;
        try {
            if (xmlGregorianCalendar != null) {
                calendar = Calendar.getInstance();
                calendar.set(xmlGregorianCalendar.getYear(), xmlGregorianCalendar.getMonth(), xmlGregorianCalendar.getDay());
                return calendar.getTime();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retorna uma lista de datas existentes entre as duas datas passadas por
     * parâmetro.
     * <p>
     * Ambas as datas passadas por parâmetro NÃO podem ser nulas e a data final
     * deve ser posterior a data inicial.
     *
     * @param dataInicio
     * @param dataFinal
     * @return
     */
    public static List<Date> listaDatas(Date dataInicio, Date dataFinal) {
        List<Date> listaData = new ArrayList<>();
        Integer aux = 1;
        Date dataAux;

        try {

            //Verifica se as datas não são nulas e se a data final é posterior a data inicial.
            if (dataInicio != null && dataFinal != null && dataInicio.before(dataFinal)) {
                Date dtInicial = dataInicio;
                Date dtFinal = dataFinal;

                Calendar cal = new GregorianCalendar();

                //ADICIONA A DATA INICIAL AO GREGORIAN CALENDAR
                dataAux = dtInicial;
                cal.setTime(dataAux);
                listaData.add(dataAux);

                //ADICIONA AS DATAS A LISTA
                while (dataAux.before(dtFinal)) {
                    cal.add(Calendar.DAY_OF_MONTH, aux);
                    listaData.add(cal.getTime());
                    dataAux = cal.getTime();

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaData;
    }

    /**
     * Efetua a remoção de acentos e tremas. Também remove caracteres especiais
     * como (<>\\|/).
     *
     * @param passa
     * @return Retorna a String recebida com sem os caracteres especiais.
     */
    public static String trataCaracterEspecial(String passa) {
        if (passa != null) {
            passa = passa.replaceAll("[ÂÀÁÄÃ]", "A");
            passa = passa.replaceAll("[âãàáä]", "a");
            passa = passa.replaceAll("[ÊÈÉË]", "E");
            passa = passa.replaceAll("[êèéë]", "e");
            passa = passa.replaceAll("[ÎÍÌÏ]", "I");
            passa = passa.replaceAll("[îíìï]", "i");
            passa = passa.replaceAll("[ÔÕÒÓÖ]", "O");
            passa = passa.replaceAll("[ôõòóö]", "o");
            passa = passa.replaceAll("[ÛÙÚÜ]", "U");
            passa = passa.replaceAll("[ûúùü]", "u");
            passa = passa.replaceAll("Ç", "C");
            passa = passa.replaceAll("ç", "c");
            passa = passa.replaceAll("[ýÿ]", "y");
            passa = passa.replaceAll("Ý", "Y");
            passa = passa.replaceAll("ñ", "n");
            passa = passa.replaceAll("Ñ", "N");
            passa = passa.replaceAll("['<>\\|/]", "");
        }
        return passa;
    }

    public static int compare(int p1, int p2) {
        return p1 > p2 ? 1 : p1 < p2 ? -1 : 0;
    }

    /**
     * Verifica a semelhança entre duas datas nos níveis de DIA, MÊS e ANO.
     *
     * @param data1
     * @param data2
     * @return
     */
    public static Boolean comparesDateEquals(Date data1, Date data2) {
        Boolean sameDates = false;
        GregorianCalendar gc1 = new GregorianCalendar();
        GregorianCalendar gc2 = new GregorianCalendar();

        //As datas não podem ser nulas.
        if (data1 != null && data2 != null) {
            //Transforma os objetos Date em GregorianCalendar.
            gc1.setTime(data1);
            gc2.setTime(data2);
            //Verifica a semelhança entre as datas nos níveis de ANO, MÊS e DIA.
            if (gc1.get(GregorianCalendar.DAY_OF_MONTH) == gc2.get(GregorianCalendar.DAY_OF_MONTH)) {//compara os dias das datas
                if (gc1.get(GregorianCalendar.MONTH) == gc2.get(GregorianCalendar.MONTH)) {//compara os meses das datas
                    if (gc1.get(GregorianCalendar.YEAR) == gc2.get(GregorianCalendar.YEAR)) {//compara os anos das datas
                        sameDates = true;
                    }//Year
                }//Month
            }//Day
        }//Null
        return sameDates;
    }//END.

    /**
     * <p>
     * Retorna uma lista de meses de acordo com o periodo informado, onde nesta
     * lista os meses é um vetor de 2 posiçoes de Date, contendo DataInicial e
     * DataFinal:
     * <p>
     * Seguindo essa estrutura:
     * <PRE>
     * - ListaPeriodos:
     * - Vetor date:
     * - DataInicial
     * - DataFinal
     * - Vetor date:
     * - DataInicial
     * - DataFinal
     * ...
     * </PRE>
     *
     * @param dateLow
     * @param dateHigh
     * @return List<Date[]>
     * @date 05/12/2011
     */
    public static List<Date[]> listaMesesPeriodo(Date dateLow, Date dateHigh) {
        //Lista que conterá o vetor de dates a ser retornado.
        List<Date[]> listaMesesPeriodo = new ArrayList<>();

        //o Vetor será apenas de duas posições, contendo:
        //datas[0] = dataInicial do periodo do mes.
        //datas[1] = dataFinal do periodo do mes.
        Date[] datas;

        //Caso alguma data seja nula ou a dateLow > que a dateHigh, retorna a lista vazia
        if (dateLow == null || dateHigh == null || dateLow.after(dateHigh)) {
            return listaMesesPeriodo;
        }

        try {
            //Calendário Gregoriano a ser utilizado pela função, para trabalhar com as datas.
            GregorianCalendar calendar = new GregorianCalendar();

            //Pegando mes e ano inicial do perido informado.
            //seto a dateLow informada, como a data no meu GregorianCalendar, para pegar o mes e ano da data.
            Date ini = Util.primeiroDiaDoMes(dateLow);
            calendar.setTime(ini);
            int mesIni = calendar.get(GregorianCalendar.MONTH);
            int anoIni = calendar.get(GregorianCalendar.YEAR);

            //Pegando mes e ano final do periodo informado.
            //seto a dateHigh informada, como a data no meu GregorianCalendar, para pegar o mes e ano da data.
            Date fim = Util.ultimoDiaDoMes(dateHigh);
            calendar.setTime(fim);
            int mesFim = calendar.get(GregorianCalendar.MONTH);
            int anoFim = calendar.get(GregorianCalendar.YEAR);

            //Datas a serem adicionadas na lista.
            Date dataInicial;
            Date dataFinal;
            //Meses utilizados para controlar meses entre os anos.
            int mesIniControle;
            int mesFimControle;

            //Se o mes e o ano do periodo informado for o mesmo apenas adiciona a data informada na lista,
            //pois é apenas um mes.
            if (mesIni == mesFim && anoIni == anoFim) {
                //Instancio vetor de duas posições:
                //  - DataInicial
                //  - DataFinal
                datas = new Date[2];

                //adiciono então as datas do peiodo informado, como o intervalo de datas da lista.
                datas[0] = dateLow;
                datas[1] = dateHigh;

                //E adiciona na lista a ser retornada.
                listaMesesPeriodo.add(datas);
            } else {
                //Laço que itera sobre o mes inicial e mes final entre os anos.
                for (int ano = anoIni; ano <= anoFim; ano++) {
                    /* Obs.: Primeiro mes no GregorianCalendar é 0.
                     *       Ultimo mes no GregorianCalendar é 11.
                     */
                    //Se for Primerio Ano.
                    if (ano == anoIni) {//Seto mesIniControle a ser iterado como o mesIni que o primerio mes do periodo informado,
                        //e mesFimControle como 11 que é o ultimo mes do ano.
                        mesIniControle = mesIni;
                        //Se o ano inicial for == ao final, então assume o mes fim como o mesFim de controle.
                        if (anoIni == anoFim) {
                            mesFimControle = mesFim;
                        } else {//Se não coloca o ultimo mes do ano
                            mesFimControle = 11;
                        }
                    }//Se for Ultimo Ano.
                    else if (ano == anoFim) {//Seto mesIniControle a ser iterado como 0 que é o primeiro mes do ano,
                        //e mesFimControle com o mesFim do periodo informado.
                        mesIniControle = 0;
                        mesFimControle = mesFim;
                    } else {//Senão significa que é oum ano no intervalo de anos do periodo informado, então pega todos os meses.
                        //mesIniControle = 0  - Janeiro.
                        //mesFimControle = 11 - Dezembro.
                        mesIniControle = 0;
                        mesFimControle = 11;
                    }

                    //Itera sobre os meses do ano da iteração.
                    for (int mes = mesIniControle; (mes <= mesFimControle || ano != anoFim) && mes < 12; mes++) {

                        //Primerio mes.
                        if (ano == anoIni && mes == mesIni) {
                            //Se for primeiro mes, ja coloca dateLow como a dataInicial a ser adicionada na lista
                            //e dataFinal é o ultimo dia da dateLow.
                            dataInicial = dateLow;
                            dataFinal = Util.ultimoDiaDoMes(dateLow);
                        }//Ultimo Mes.
                        else if (ano == anoFim && mes == mesFim) {
                            //Se for ultimo mes do periodo informado, ja coloca a dateHigh como a data final
                            //e a data inicial é o primeiro dia da dateHigh.
                            dataInicial = Util.primeiroDiaDoMes(dateHigh);
                            dataFinal = dateHigh;
                        } else {//Mes(es) no meio do intervalo do periodo informado(dateLow e dateHigh).
                            //Inicio o GregorianCalendar com a data atual e seto mes e ano do intervalo atual, que está rodando no for.
                            calendar.setTime(new Date());
                            calendar.set(GregorianCalendar.MONTH, mes);
                            calendar.set(GregorianCalendar.YEAR, ano);

                            //Coloco como dataInicial o primeiro dia do mes/ano adicionado anteriormente.
                            //Coloco como dataFinal o ultimo dia do mes/ano adicionado anteriormente.
                            dataInicial = Util.primeiroDiaDoMes(calendar.getTime());
                            dataFinal = Util.ultimoDiaDoMes(calendar.getTime());
                        }

                        //Instancio vetor de duas posições:
                        //  - DataInicial
                        //  - DataFinal
                        datas = new Date[2];

                        //Insere as datas inicial e final, no meu vetor de dates,
                        //Sendo assim, adiciono o meu periodo mensal da iteração do for.
                        datas[0] = dataInicial;
                        datas[1] = dataFinal;
                        //Adiciono o periodo mensal da iteração do for, na minha lista a ser retornada.
                        listaMesesPeriodo.add(datas);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaMesesPeriodo;
    }

    /**
     * <pre>
     * As vezes ao concatenar Strings e gerado a quebra de linha automaticamente '\n'
     * aonde não e possivel remover nem com o replaceAll, foi criado esse metodo que
     * remove o '/n'.
     *
     * @param text
     * @return
     */
    public static String removerQuebraLinha(String text) {
        StringBuilder str = new StringBuilder();
        String[] resultados = text.split("\\n");
        for (String resultado : resultados) {
            str.append(resultado);
        }
        return str.toString();
    }

    public static String nomeDoMes(Date data) {
        String mes = "";
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(data);
        switch (calendar.get(GregorianCalendar.MONTH)) {
            case 0:
                mes = "Janeiro";
                break;
            case 1:
                mes = "Fevereiro";
                break;
            case 2:
                mes = "Março";
                break;
            case 3:
                mes = "Abril";
                break;
            case 4:
                mes = "Maio";
                break;
            case 5:
                mes = "Junho";
                break;
            case 6:
                mes = "Julho";
                break;
            case 7:
                mes = "Agosto";
                break;
            case 8:
                mes = "Setembro";
                break;
            case 9:
                mes = "Outubro";
                break;
            case 10:
                mes = "Novembro";
                break;
            case 11:
                mes = "Dezembro";
                break;
        }
        return mes;
    }

    /**
     * Seta na label os parametros informados.
     *
     * @param label
     * @param params
     * @return
     */
    public static String setParamsLabel(String label, Object... params) {
        String mensagemParametrizada = MessageFormat.format(label, params);
        return mensagemParametrizada;
    }

    /**
     * As vezes ao concatenar Strings e gerado a quebra de linha automaticamente
     * '\n' aonde não e possivel remover nem com o replaceAll, foi criado esse
     * metodo que remove o '/n'.
     *
     * @param cpf
     * @return
     * @throws ParseException
     */
    public static String formataCpf(String cpf) throws ParseException {
        if (cpf != null && !cpf.equals("") && !cpf.contains(".")) {
            //ADICIONA ZEROS NA FRENTE
            if (cpf.length() < 11) {
                for (int i = 0; cpf.length() < 11; i++) {
                    cpf = "0" + cpf;
                }
            }
            //FORMATA
            MaskFormatter mf = new MaskFormatter("###.###.###-##");
            mf.setValueContainsLiteralCharacters(false);
            cpf = mf.valueToString(cpf);
        }
        return cpf;
    }

    /**
     * @param text
     * @return Verifica se a string contem somente numeros. Ex: 00000000000 -> true
     * 0000000000a -> false
     * @since 15/12/2014
     */
    public static boolean validaSomenteNumeros(String text) {
        if (text == null) {
            return false;
        }
        if (text.trim().length() < 1) {
            return false;
        }
        String regex = "\\d+";
        return text.matches(regex);
    }

    public static String somenteNumeros(String text) {
        if (text == null) {
            return "";
        }
        return text.replaceAll("[^0-9]", "");
    }

    /**
     * Formata o CPF independentemente da forma digitada, isto é, formatada ou
     * não, e permite a busca através desse filtro, considerando a quantidade de
     * números digitados e listando a partir disso. Ex: 00000000000 ->
     * 000.000.000-00 Se digitar 078482 -> 078.482.
     *
     * @param cpf
     * @return
     * @throws ParseException
     */
    public static String formataCpfAteDigito(String cpf) throws ParseException {
        //só passa para formatação se o valor de parametro for números
        Pattern apenasNumeros = Pattern.compile("[0-9]+");
        if (apenasNumeros.matcher(cpf).find()) {
            if (cpf != null && !cpf.equals("") && !cpf.contains(".")) {

                int tamanhoCpfDig = cpf.length();

                //Considera os caracteres "." e "-" no tamanho
                if (tamanhoCpfDig > 9) {
                    tamanhoCpfDig = tamanhoCpfDig + 3;
                } else if (tamanhoCpfDig > 6) {
                    tamanhoCpfDig = tamanhoCpfDig + 2;
                } else if (tamanhoCpfDig > 3) {
                    tamanhoCpfDig = tamanhoCpfDig + 1;
                }
                //Adiciona zeros à direita
                if (cpf.length() < 11) {
                    for (int i = 0; cpf.length() < 11; i++) {
                        cpf = cpf + "0";
                    }
                }
                //Formata
                MaskFormatter mf = new MaskFormatter("###.###.###-##");
                mf.setValueContainsLiteralCharacters(false);
                cpf = mf.valueToString(cpf);

                //Filtra a partir da quantidade de números digitados 
                cpf = cpf.substring(0, tamanhoCpfDig);

            }
        }
        return cpf;
    }

    /**
     * Converte uma String em Data quando e utilizado o JSON do C# para o JAVA
     * ,como o json não consegue converter para data tem que passar como string
     * ,então foi criado este metodo para montar a data pela string do DateTime
     *
     * @param date
     * @return
     */
    public static Date convertJsonStringToDate(String date) {
        try {
            if (date != null) {
                //CONVERSÃO DE DATE EM STRING
                //TIME COMO VEM DO C# "/Date(1275879600000-0300)/"
                //TIME DO JAVA               1376062569595
                Date data = new Date();
                data.setTime(Long.parseLong(date.substring(6, date.lastIndexOf("-"))));
                return data;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Cria uma senha randomica com o tamanho passado por parametro
     *
     * @param tamanho
     * @return
     */
    public static String novaSenha(Integer tamanho) {
        String senha = null;
        char[] allChars = new char[62];
        Random random = new Random();

        try {
            for (int i = 48, j = 0; i < 123; i++) {
                if (Character.isLetterOrDigit(i)) {
                    allChars[j] = (char) i;
                    j++;
                }
            }

            char[] result = new char[tamanho];
            for (int i = 0; i < tamanho; i++) {
                result[i] = allChars[random.nextInt(allChars.length)];
            }

            senha = new String(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return senha;
    }

    /**
     * Retorna a Quantidade meses durante um intervalo de datas
     *
     * @param dataInicio
     * @param dataFim
     * @return
     */
    public static Integer qtdeMeses(Date dataInicio, Date dataFim) {
        Integer meses = 0;
        boolean dataIniMaior = false;
        GregorianCalendar gc1 = new GregorianCalendar();
        GregorianCalendar gc2 = new GregorianCalendar();

        gc1.setTime(dataInicio);
        gc2.setTime(dataFim);

        gc1.clear(Calendar.MILLISECOND);
        gc1.clear(Calendar.SECOND);
        gc1.clear(Calendar.MINUTE);
        gc1.clear(Calendar.HOUR_OF_DAY);
        gc1.clear(Calendar.DATE);

        gc2.clear(Calendar.MILLISECOND);
        gc2.clear(Calendar.SECOND);
        gc2.clear(Calendar.MINUTE);
        gc2.clear(Calendar.HOUR_OF_DAY);
        gc2.clear(Calendar.DATE);

        while (gc1.before(gc2)) {
            gc1.add(Calendar.MONTH, 1);
            meses = dataIniMaior ? --meses : ++meses;
        }

        return (meses == 0 || meses < 0) ? 0 : meses;
    }

    /**
     * <p>
     * Remove os números da String informado por parametro.
     *
     * @param valor
     * @return
     */
    public static String removeNumeros(String valor) {

        if (valor == null) {
            return null;
        }

        return valor.replaceAll("\\d", "");
    }

    /**
     * <p>
     * Conta quantos dias uteis tem no mes, descontando sabados e domingos
     * (feriados ficam por conta da tb_dia_especial)
     *
     * @param dataInicio
     * @param dataFim
     * @return
     */
    public static Integer diasUteisEntreDuasDatas(Date dataInicio, Date dataFim) {
        Calendar CalInicio;
        Calendar CalFim;
        CalInicio = Calendar.getInstance();
        CalInicio.setTime(dataInicio);
        CalFim = Calendar.getInstance();
        CalFim.setTime(dataFim);
        Integer diasUteis = 0;

        do {
            if (CalInicio.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && CalInicio.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                ++diasUteis;
            }
            CalInicio.add(Calendar.DAY_OF_MONTH, 1);
        } while (CalInicio.getTimeInMillis() < CalFim.getTimeInMillis());

        return diasUteis;
    }

    /**
     * @param horaEntrada "00:00"
     * @param horaSaida   "00:00"
     * @return metodo que retorna a diferenças entre duas horas entrada do tipo String
     * passando as horas com minutos concatenados com : exemplo: 12:25 retorna
     * um valor String com hora concatenada com : exemplo 6:25 - 2:05 = 04:00
     * pode retornar horas negativas exemplo 2:05 - 6:25 = -04:20
     */
    public String diferencaHoras(String horaEntrada, String horaSaida) {
        String horas = "00";
        String minutos = "00";
        String minutoEntrada;
        String minutoSaida;
        Integer subHoras;
        Integer subMinutos;
        Integer segundosEntr;
        Integer segundosSai;
        Integer sub;
        if (!horaEntrada.equals("") && !horaSaida.equals("")) {
            //seta os minutos
            minutoEntrada = horaSaida.substring(horaEntrada.indexOf(":"), horaEntrada.length());
            minutoSaida = horaSaida.substring(horaSaida.indexOf(":"), horaSaida.length());
            //transofrma em segundos
            segundosSai = (Integer.parseInt(horaSaida.substring(0, horaSaida.indexOf(":"))) * 3600) + (Integer.parseInt(minutoSaida) * 60);
            segundosEntr = (Integer.parseInt(horaEntrada.substring(0, horaEntrada.indexOf(":"))) * 3600) + (Integer.parseInt(minutoEntrada) * 60);

            sub = segundosEntr - segundosSai;
            //pega as horas 
            if (sub >= 3600) {
                subHoras = (sub - (sub % 3600)) / 3600;
                sub = sub - (subHoras * 3600);
                if (subHoras < 10) {
                    horas = "0" + Integer.toString(subHoras);
                } else {
                    horas = Integer.toString(subHoras);
                }
            }
            if (sub <= -3600) {
                subHoras = (sub - (sub % 3600)) / 3600;
                sub = sub - (subHoras * 3600);
                if (subHoras > -10) {
                    horas = "-0" + Integer.toString(subHoras * -1);
                } else {
                    horas = Integer.toString(subHoras);
                }
            }
            if (sub < 0) {
                sub = sub * -1;
            }
            //pega os minutos
            if (sub >= 60) {
                subMinutos = (sub - (sub % 60)) / 60;
                if (subMinutos < 10) {
                    minutos = "0" + Integer.toString(subMinutos);
                } else {
                    minutos = Integer.toString(subMinutos);
                }
            }

        }
        return horas + ":" + minutos;
    }

    /**
     * @param horaEntrada
     * @param minutoEntrada
     * @param horaSaida
     * @param minutoSaida
     * @return metodo que retorna a diferenças entre duas horas entrada do tipo String
     * passando as horas e minutos separadamente exemplo: 20 23 14 59 retorna um
     * valor String com hora concatenada com : exemplo 6:25 - 2:05 = 04:00 pode
     * retornar horas negativas exemplo 2:05 - 6:25 = -04:20
     */
    public String diferencaHoras(String horaEntrada, String minutoEntrada, String horaSaida, String minutoSaida) {

        String horas = "00";
        String minutos = "00";
        Integer subHoras;
        Integer subMinutos;
        Integer segundosEntr;
        Integer segundosSai;
        Integer sub;
        if (!horaEntrada.equals("") && !horaSaida.equals("") && !minutoEntrada.equals("") && !minutoSaida.equals("")) {
            segundosSai = (Integer.parseInt(horaSaida) * 3600) + (Integer.parseInt(minutoSaida) * 60);
            segundosEntr = (Integer.parseInt(horaEntrada) * 3600) + (Integer.parseInt(minutoEntrada) * 60);

            sub = segundosEntr - segundosSai;

            if (sub >= 3600) {
                subHoras = (sub - (sub % 3600)) / 3600;
                sub = sub - (subHoras * 3600);
                if (subHoras < 10) {
                    horas = "0" + Integer.toString(subHoras);
                } else {
                    horas = Integer.toString(subHoras);
                }
            }
            if (sub <= -3600) {
                subHoras = (sub - (sub % 3600)) / 3600;
                sub = sub - (subHoras * 3600);
                if (subHoras > -10) {
                    horas = "-0" + Integer.toString(subHoras * -1);
                } else {
                    horas = Integer.toString(subHoras);
                }
            }
            if (sub < 0) {
                sub = sub * -1;
            }
            if (sub >= 60) {
                subMinutos = (sub - (sub % 60)) / 60;
                if (subMinutos < 10) {
                    minutos = "0" + Integer.toString(subMinutos);
                } else {
                    minutos = Integer.toString(subMinutos);
                }
            }

        }
        return horas + ":" + minutos;
    }

    /**
     * @param dataIni
     * @return Converte a hora da data recebida por parametro para 00:00.00 para evitar
     * que nos filtros por between a data inicial tenha sua hora zerada, podendo
     * compreender o periodo por inteiro.
     */
    public static Date convertDateHrInicial(Date dataIni) {

        if (dataIni != null) {

            GregorianCalendar calendar = new GregorianCalendar();

            calendar.setTime(dataIni);
            calendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
            calendar.set(GregorianCalendar.MINUTE, 0);
            calendar.set(GregorianCalendar.SECOND, 0);
            return calendar.getTime();
        }
        return null;
    }

    /**
     * @param dataFim
     * @return Converte a hora da data recebida por parametro para 23:59.59 para evitar
     * que nos filtros por between a data final tenha sua hora setada para
     * ultima hora possível, podendo compreender o periodo por inteiro.
     * <p>
     * <PRE>
     * Ex: Permite filtrar por uma data espefícica ou por um período de data,
     * independente do horário na qual a requisição de um serviço foi
     * solicitada. Assim, será possível filtrar pela data corretamente.
     * </PRE>
     */
    public static Date convertDateHrFinal(Date dataFim) {

        if (dataFim != null) {

            GregorianCalendar calendar = new GregorianCalendar();

            calendar.setTime(dataFim);
            calendar.set(GregorianCalendar.HOUR_OF_DAY, 23);
            calendar.set(GregorianCalendar.MINUTE, 59);
            calendar.set(GregorianCalendar.SECOND, 59);
            return calendar.getTime();
        }
        return null;
    }

    /**
     * Retorna o nome do metodo atual. Utilizado para logs.
     *
     * @return
     */
    public static String getNomeMetodoAtual() {
        String nome = "";

        try {
            nome = Thread.currentThread().getStackTrace()[2].getMethodName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nome;
    }

    public static byte[] redimensionarImagem(byte[] fileData, int width, int height) {
        try {
            ByteArrayInputStream in = new ByteArrayInputStream(fileData);
            //Faz a leitura do byte[]
            BufferedImage img = ImageIO.read(in);

            //Caso a altura ou a largura seja zero calcula a proporção correta para fazer o redimensionamento
            if (height == 0) {
                height = (width * img.getHeight()) / img.getWidth();
            }
            if (width == 0) {
                width = (height * img.getWidth()) / img.getHeight();
            }

            //Redimensiona a imagem conforme os tamanhos passados por parametro
            Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);

            //Recria o BufferedImage apartir do tamamnho especificado
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

            //"Desenha" a imagem no buffer
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null);

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            ImageIO.write(imageBuff, "jpg", buffer);

            return buffer.toByteArray();
        } catch (IOException e) {

        }
        return fileData;
    }

    /**
     * @param urlCodificada
     * @return urlDecodificada
     * @author Luan L Domingues metodo que decodifica os caracteres ASCII da url
     * pois pode vir com um desse abaixo
     */
    static public String decodingUrl(String urlCodificada) {
        String urlDecodificada;
        urlDecodificada = urlCodificada.replaceAll("%20", " ");
        urlDecodificada = urlDecodificada.replaceAll("%21", "!");
        urlDecodificada = urlDecodificada.replaceAll("%22", "\"");
        urlDecodificada = urlDecodificada.replaceAll("%23", "#");
        urlDecodificada = urlDecodificada.replaceAll("%24", "$");
        urlDecodificada = urlDecodificada.replaceAll("%25", "%");
        urlDecodificada = urlDecodificada.replaceAll("%26", "&");
        urlDecodificada = urlDecodificada.replaceAll("%27", "'");
        urlDecodificada = urlDecodificada.replaceAll("%28", "(");
        urlDecodificada = urlDecodificada.replaceAll("%29", ")");
        urlDecodificada = urlDecodificada.replaceAll("%2A", "*");
        urlDecodificada = urlDecodificada.replaceAll("%2B", "+");
        urlDecodificada = urlDecodificada.replaceAll("%2C", ",");
        urlDecodificada = urlDecodificada.replaceAll("%2D", "-");
        urlDecodificada = urlDecodificada.replaceAll("%2E", ".");
        urlDecodificada = urlDecodificada.replaceAll("%2F", "/");
        urlDecodificada = urlDecodificada.replaceAll("%3A", ":");
        urlDecodificada = urlDecodificada.replaceAll("%3B", ";");
        urlDecodificada = urlDecodificada.replaceAll("%3C", "<");
        urlDecodificada = urlDecodificada.replaceAll("%3D", "=");
        urlDecodificada = urlDecodificada.replaceAll("%3E", ">");
        urlDecodificada = urlDecodificada.replaceAll("%3F", "?");
        urlDecodificada = urlDecodificada.replaceAll("%40", "@");
        urlDecodificada = urlDecodificada.replaceAll("%5B", "[");
        urlDecodificada = urlDecodificada.replaceAll("%5C", "\\");
        urlDecodificada = urlDecodificada.replaceAll("%5D", "]");
        urlDecodificada = urlDecodificada.replaceAll("%5E", "^");
        urlDecodificada = urlDecodificada.replaceAll("%7B", "{");
        urlDecodificada = urlDecodificada.replaceAll("%7C", "|");
        urlDecodificada = urlDecodificada.replaceAll("%7D", "}");
        return urlDecodificada;
    }

    /**
     * Verifica se a dataEu é maior ou menor que a dataEle<br>
     * <p>
     * -1 = "eu sou menor" <br>
     * 0 = "eu sou igual"<br>
     * 1 = "eu sou maior"<br>
     *
     * @param dataEu
     * @param dataEle
     * @return
     */
    public static int compareDateTo(Date dataEu, Date dataEle) {
        return dataEu.compareTo(dataEle);
    }

    public static Object resolverExpressao(String expressao) throws ScriptException {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        return engine.eval(expressao);
    }

    public static String removeCaracteresEspeciais(String text) {
        if (text == null) {
            return "";
        }
        text = Normalizer.normalize(text.toUpperCase(), Normalizer.Form.NFD);
        return text.replaceAll("[^\\p{ASCII}]", "");
    }

    public static String removePontuacaoEspacosNumeros(String text) {
        if (text == null) {
            return "";
        }
        text = removeCaracteresEspeciais(text);
        text = text.replaceAll("[0-9-\\s]", "");
        return text;
    }

    public static String primeiraMaiuscula(String strInicial) {
        String strFinal = null;
        if (strInicial != null && !strInicial.isEmpty()) {
            strFinal = strInicial.substring(0, 1).toUpperCase() + strInicial.substring(1);
        }
        return strFinal;
    }

    public static String primeiraMinuscula(String strInicial) {
        String strFinal = null;
        if (strInicial != null && !strInicial.isEmpty()) {
            strFinal = strInicial.substring(0, 1).toLowerCase() + strInicial.substring(1);
        }
        return strFinal;
    }

    /**
     * Soma duas quantidades de horas<br/>
     * Exemplos<br/>
     * <b>01:30 + 01:30 = 03:00</b><br/>
     * <b>15:30 + 15:30 = 31:00</b><br/>
     * <br/>
     * Também realiza a subtração de duas horas<br/>
     * Exemplos<br/>
     * <b>02:30 + -01:30 = 01:00</b><br/>
     * <b>01:00 + -02:30 = -01:30</b><br/>
     * <br/>
     * Os valores devem ser passados no formato <b>00:00</b><br/>
     * Para horas negativas <b>-00:00</b>
     *
     * @param strHora1
     * @param strHora2
     * @return
     * @throws Exception
     * @author Marlon B. Santana <m.santana@nextage.com.br>
     * @date 16/05/2015
     */
    public static String somaHorasMinutos(String strHora1, String strHora2) throws Exception {
        String totalHoras = "";
        try {
            if (!Pattern.matches("-?\\d+:\\d+", strHora1) || !Pattern.matches("-?\\d+:\\d+", strHora2)) {
                throw new Exception("As horas não estão no formato correto");
            }

            PeriodFormatter formatter = new PeriodFormatterBuilder()
                    .appendHours().appendSeparator(":")
                    .appendMinutes()
                    .toFormatter();

            Period localTime1 = Period.parse(strHora1.replace("-", ""), formatter);
            if (strHora1.startsWith("-")) {
                localTime1 = localTime1.negated();
            }

            Period localTime2 = Period.parse(strHora2.replace("-", ""), formatter);
            if (strHora2.startsWith("-")) {
                localTime2 = localTime2.negated();
            }

            Period soma = localTime1.plus(localTime2).normalizedStandard();

            totalHoras += soma.getHours() < 0 || soma.getMinutes() < 0 ? "-" : "";
            totalHoras += Math.abs(soma.getHours()) < 10 ? "0" : "";
            totalHoras += Math.abs(soma.getHours()) + ":";
            totalHoras += Math.abs(soma.getMinutes()) < 10 ? "0" : "";
            totalHoras += Math.abs(soma.getMinutes());

        } catch (Exception e) {
            Exception eAux = new Exception("Não é possível realizar o cálculo", e);
            throw eAux;
        }
        return totalHoras;
    }

    public static String somaHorasMinutos2(String inicio, String fim) {
        try {
            Double horaIni = Math.abs(new Double(inicio.split(":")[0]));
            horaIni += Math.abs(new Double(inicio.split(":")[1]) / 60);

            if (inicio.startsWith("-")) {
                horaIni *= (-1);
            }

            Double horaFim = Math.abs(new Double(fim.split(":")[0]));
            horaFim += Math.abs(new Double(fim.split(":")[1]) / 60);

            if (fim.startsWith("-")) {
                horaFim *= (-1);
            }

            Double valorHora = horaIni + horaFim;
            if (valorHora == 0) {
                return "00:00";
            }
            boolean negativo = false;
            if (valorHora < 0) {
                negativo = true;
                valorHora *= (-1);
            }
            int min = (int) Math.round(valorHora * 60);

            int hours = min / 60;
            int minutes = min % 60;

            String totalHoras = Math.abs(hours) < 10 ? "0" : "";
            totalHoras += Math.abs(hours) + ":";
            totalHoras += Math.abs(minutes) < 10 ? "0" : "";
            totalHoras += Math.abs(minutes);

            return negativo ? "-" + totalHoras : totalHoras;
        } catch (Exception e) {
            //Exception eAux = new Exception("Não é possível realizar o cálculo", e);
            throw e;
        }
    }

    /**
     * @param sexo
     * @param rb
     * @return
     */
    public static String labelFunctionSexo(String sexo, NxResourceBundle rb) {
        if (sexo != null) {
            switch (sexo) {
                case "m":
                case "M":
                    return rb.getString("label_masculino");
                case "f":
                case "F":
                    return rb.getString("label_feminino");
            }
        }
        return null;
    }

    /**
     * Metodo responsavel para Criar uma lista por bloco.<br>
     * Os primeiros 4 parametros são obrigatorios, e sem else não irá listar.
     * Caso precise filtar por uma lista e a lista for muito grande.<br>
     * Caso o listaReferencia não for instanceof List não irá listar.
     *
     * @param listaReferencia          <b>Object</b>Passar uma lista de parametros a
     *                                 serem pesquisados - a lista completa ex:Lista de Referencias/ lista de
     *                                 Pessoas.
     * @param classeLista              <b>Class</b> Classe do Objeto que a lista final irá
     *                                 adiquirir.
     * @param propriedadesRetornoLista <b>List Propriedade </b> Propriedades que
     *                                 irão ser listadas.
     * @param nomePropriedadeFiltro    <b>String</b> Nome da propriedade que
     *                                 precisa ser filtrada Ex: referencia; id; funcaoId.
     * @param aliasPropriedadeFiltro   <b>String</b> Caso for um objeto de 2°
     *                                 nivel ou superior precisa do alias senão deixar null; Ex Funcao dentro de
     *                                 Pessoa.
     * @param quantidadePorBloco       <b>Integer</b> Quantidade que cada bloco irá
     *                                 assumir, casso não seja passado irá assumir 50.
     * @param nxCriterionAdicional     <b>NxCriterion</b> NxCriterion Adicional para
     *                                 filtrar alem do IN.
     * @param nxOrders                 <b>List NxOrder </b> NxOrder para passar caso se queira
     *                                 que a lista seje ordenada.
     * @param isObject                 <b>boolean</b> Caso for passar uma lista de
     *                                 Objetos(ex:Pessoa) par parametro e não de Primitivos(ex:Integer) passar
     *                                 true para o metodo criar um getter da propriedade que se queira filtrar
     *                                 (nomePropriedadeFiltro).
     * @return lista <b>Object</b> Mesmo voltando uma lista irá voltar do Tipo
     * Object para facilitar a conversão na hora do parse.
     * @throws Exception Lança uma Exception para sim caso der erro ser tratado
     *                   no metodo acima.
     * @author Luan Luiz Freitas Domingues <b>l.domingues@nextage.com.br</b>
     */
    public static Object montaListaBloco(Object listaReferencia,
                                         Class classeLista,
                                         List<Propriedade> propriedadesRetornoLista,
                                         String nomePropriedadeFiltro,
                                         String aliasPropriedadeFiltro,
                                         Integer quantidadePorBloco,
                                         NxCriterion nxCriterionAdicional,
                                         List<NxOrder> nxOrders,
                                         boolean isObject) throws Exception {

        List<Object> lista = null;
        //valida se os primeiros 4 parametros são diretentes de null e diferentes de vazios
        //valida se a listaReferencia é uma lista para ser tansformada em seguida
        if (listaReferencia != null && listaReferencia instanceof List) {
            //transforma a listaReferencia de Object para listapara ser usada futuramente
            List<Object> listaReferenciaInter = (List<Object>) listaReferencia;
            if (!listaReferenciaInter.isEmpty()
                    && classeLista != null
                    && propriedadesRetornoLista != null && !propriedadesRetornoLista.isEmpty()
                    && nomePropriedadeFiltro != null && !nomePropriedadeFiltro.isEmpty()) {
                //caso a quantidade dor null ou zerada coloca a mesma a 50
                if (quantidadePorBloco == null || quantidadePorBloco == 0) {
                    quantidadePorBloco = 50;
                }
                //cria a lista auxiliar/
                List<Object> listaAux = new ArrayList<>();
                //cria o getter caso for um objeto
                Getter getter = null;
                if (isObject) {
                    Object objeto = listaReferenciaInter.get(0);
                    getter = PropertyAccessorFactory.getPropertyAccessor("field").getGetter(objeto.getClass(), nomePropriedadeFiltro);
                }
                //cria a flag de posição
                int posicao = 0;
                //cria a lista de referencias
                List<Object> referencias = new ArrayList<>();
                //inicializa o GenerciDao
                GenericDao genericDao = new GenericDao();
                //inicializa a Lista final
                lista = new ArrayList<>();
                //percorre a lista de referencias passada por parametro
                for (Object obj : listaReferenciaInter) {
                    //caso o geter não for null tenta buscar a propriedade detnro do objeto em questão
                    //senão irá setálo automaticamente
                    if (getter != null) {
                        referencias.add(getter.get(obj));
                    } else {
                        referencias.add(obj);
                    }
                    //aumeta a flag da posição
                    posicao++;
                    //caso a lista de referencias alcance o limita de bloco ou a posição alcance a final
                    //irá criar o filtro IN + o passado por parametro caso não for null
                    if (referencias.size() == quantidadePorBloco || posicao == listaReferenciaInter.size()) {
                        NxCriterion nxCriterionBloco = null;
                        //cria o filtro IN
                        if (aliasPropriedadeFiltro != null && !aliasPropriedadeFiltro.isEmpty()) {
                            nxCriterionBloco = NxCriterion.montaRestriction(new Filtro(nomePropriedadeFiltro, referencias, Filtro.IN, aliasPropriedadeFiltro));
                        } else {
                            nxCriterionBloco = NxCriterion.montaRestriction(new Filtro(nomePropriedadeFiltro, referencias, Filtro.IN));
                        }
                        //caso tenha o nxCriterionAdicional irá criar um AND
                        //não foi vista necessidade de criar um para o OR
                        if (nxCriterionAdicional != null) {
                            nxCriterionBloco = NxCriterion.and(nxCriterionAdicional, nxCriterionBloco);
                        }
                        //lista conforme o pasado anteriormente
                        //-1 = COnstantes.NO_LIMIT
                        listaAux = genericDao.listarByFilter(propriedadesRetornoLista, nxOrders, classeLista, -1, nxCriterionBloco);
                        //caso não for null ou vazia irá adiciona a lista final
                        if (listaAux != null && !listaAux.isEmpty()) {
                            lista.addAll(listaAux);
                        }
                        //limpa a lista de referencias
                        referencias = new ArrayList<>();
                    }
                }
            }
        }
        return lista;
    }

    /**
     * Metodo que cria o Getter da propriedade Passada por parametro do Objeto
     *
     * @param objeto
     * @param propriedade
     * @return Getter
     * @throws Exception
     */
    public static Getter getGetter(Object objeto, String propriedade) throws Exception {
        Class classe = objeto.getClass().equals(Class.class) ? (Class) objeto : objeto.getClass();
        return getGetter(classe, propriedade);
    }


    /**
     * Metodo que cria o Getter da propriedade Passada por parametro do Objeto
     *
     * @param classe      Class
     * @param propriedade String
     * @return Getter
     * @throws Exception e
     */
    public static Getter getGetter(Class classe, String propriedade) throws Exception {
        Getter getter = null;
        if (!classe.getSuperclass().equals(Object.class)) {
            try {
                getter = getGetter(classe.getSuperclass(), propriedade);
            } catch (Exception e) {
                getter = null;
            }
        }
        if (getter == null) {
            getter = (PropertyAccessorFactory.getPropertyAccessor("field").getGetter(classe, propriedade));
        }
        return getter;
    }

    /**
     * Metodo que cria o Setter da propriedade Passada por parametro do Objeto
     *
     * @param objeto
     * @param propriedade
     * @return Getter
     * @throws Exception
     */
    public static Setter getSetter(Object objeto, String propriedade) throws Exception {
        Class classe = objeto.getClass().equals(Class.class) ? (Class) objeto : objeto.getClass();
        return getSetter(classe, propriedade);
    }

    /**
     * Metodo que cria o Getter da propriedade Passada por parametro do Objeto
     *
     * @param classe      Class
     * @param propriedade String
     * @return Getter
     * @throws Exception e
     */
    public static Setter getSetter(Class classe, String propriedade) throws Exception {
        Setter setter = null;
        if (!classe.getSuperclass().equals(Object.class)) {
            try {
                setter = getSetter(classe.getSuperclass(), propriedade);
            } catch (Exception e) {
                setter = null;
            }
        }
        if (setter == null) {
            setter = (PropertyAccessorFactory.getPropertyAccessor("field").getSetter(classe, propriedade));
        }
        return setter;
    }

    /**
     * Converte o número passado por parametro para String, utilizando os
     * separadores do resourceBundle também passado por parametro, e exibindo a
     * quantidade de casas decimais desejada.
     *
     * @param numero        Número a ser transformado em String
     * @param rb            NxResourceBundle necessário para obter os separadoes das casas
     *                      de milhares e decimais.
     * @param casasDecimais Quantidade de casas decimais a serem exibidas.
     * @return Representação em String do número desejado.
     * @author Marlon B. Santana
     * @date 11/09/2015
     */
    public static String doubleToString(Double numero, NxResourceBundle rb, Integer casasDecimais) {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator(rb.getString("format_decimalSeparator").charAt(0));
        decimalFormatSymbols.setGroupingSeparator(rb.getString("format_thousandsSeparator").charAt(0));

        DecimalFormat numberFormat = new DecimalFormat();
        if (casasDecimais != null) {
            numberFormat.setMaximumFractionDigits(casasDecimais);
            numberFormat.setMinimumFractionDigits(casasDecimais);
        } else {
            numberFormat.setMaximumFractionDigits(2);
            numberFormat.setMinimumFractionDigits(2);
        }
        numberFormat.setDecimalFormatSymbols(decimalFormatSymbols);

        return numberFormat.format(numero);
    }

    /**
     * Converte um date para um XMLGregorianCalendar.
     * <p>
     * Utilizado para enviar datas por webService.
     *
     * @param data
     * @return
     */
    public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date data) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        XMLGregorianCalendar xmlGregorianCalendar = null;

        try {
            gregorianCalendar.setTime(data);
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlGregorianCalendar;
    }

    /**
     * Converte a hora numérica passada por parâmetro ex: 189<br>
     * para a hora em String "00:00"<br>
     *
     * @param hora
     * @return
     */
    public static String converterHoraNumericaParaString(Double hora) {
        boolean negativo = false;
        if (hora < 0) {
            hora *= (-1);
        }
        Double horaMarcacao = hora / 60;
        String strMarcacao = horaMarcacao < 10 ? "0" : "";
        strMarcacao += horaMarcacao.intValue();
        strMarcacao += ":";
        strMarcacao += new Double((horaMarcacao - horaMarcacao.intValue()) * 60).intValue() < 10 ? "0" : "";
        strMarcacao += new Double((horaMarcacao - horaMarcacao.intValue()) * 60).intValue();
        return negativo ? "-" + strMarcacao : strMarcacao;
    }

    /**
     * Converte a hora string passada por parâmetro ex: "00:00"<br>
     * para a hora numerica 189<br>
     *
     * @param hora
     * @return
     */
    public static String converterHoraStringParaNumerica(String hora) {
        boolean negativo = false;
        if (hora.startsWith("-")) {
            negativo = false;
        }
        Double valor = Math.abs(new Double(hora.split(":")[0]));
        valor += Math.abs(new Double(hora.split(":")[1]) / 60);
        return negativo ? "-" + valor.toString() : valor.toString();
    }

    /**
     * Seta o data dos objetos da lista em stDate
     *
     * @param listaParametro
     * @param nomeData
     * @param formato
     * @return
     * @throws Exception
     * @author Luan l domingues
     */
    public static Object setaStDateEmLista(Object listaParametro, String nomeData, String formato) throws Exception {
        if (listaParametro != null && listaParametro instanceof List) {
            List<Object> lista = (List<Object>) listaParametro;
            for (Object obj : lista) {
                String nomeStDate = "st" + primeiraMaiuscula(nomeData);
                Getter getter = getGetter(obj, nomeData);
                Setter setter = getSetter(obj, nomeStDate);
                Date data = (Date) getter.get(obj);
                String stDate = dateToString(data, formato);
                setter.set(obj, stDate, null);
            }
            listaParametro = lista;
        }
        return listaParametro;
    }

    /**
     * Seta o data dos objetos da lista em stDate
     *
     * @param listaParametro
     * @param nomeData
     * @return
     * @throws Exception
     * @author Luan l domingues
     */
    public static Object setaStDateEmLista(Object listaParametro, String nomeData) throws Exception {
        NxResourceBundle rb = new NxResourceBundle(Constantes.MENSAGENS_UNDERLINE + "pt_BR");
        String formato = rb.getString("format_date");
        return setaStDateEmLista(listaParametro, nomeData, formato);
    }

    /**
     * Seta o data dos objetos da lista em stDate
     *
     * @param listaParametro
     * @param nomeData
     * @param rb
     * @return
     * @throws Exception
     * @author Luan l domingues
     */
    public static Object setaStDateEmLista(Object listaParametro, String nomeData, NxResourceBundle rb) throws Exception {
        String formato = rb.getString("format_date");
        return setaStDateEmLista(listaParametro, nomeData, formato);
    }

    /**
     * Arruma setta os campos StData em Data
     *
     * @param obj
     * @param rb
     * @return
     * @author Luan l domingues
     */
    static public Object converteStDataToDate(Object obj, NxResourceBundle rb) {
        //<editor-fold defaultstate="collapsed" desc="Metodo arrumaDatas">
        if (obj != null) {
            Class classe = obj.getClass();
            try {
                List<String> listaNomesDatas = getListaNomeDatas(obj);
                //percorre as listas de nomes pega anteriormente
                for (String nomeData : listaNomesDatas) {
                    try {
                        //pega os nomes dos atributos
                        String nomeStData = "st" + Util.primeiraMaiuscula(nomeData);
                        Getter getter = getGetter(obj, nomeStData);
                        //pega o valor do atributo ata ex dataAux1
                        String conteudoStData = (String) getter.get(obj);
                        //verifica se é nullo
                        if (conteudoStData != null && !conteudoStData.trim().isEmpty()) {
                            Setter setter = getSetter(obj, nomeData);
                            setter.set(obj, parseData(conteudoStData, rb.getString("format_date")), null);
                        }
                    } catch (Exception e) {
                        System.err.println("/// Erro no Campo: " + nomeData + " da classe: " + classe.getName());
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                System.err.println("/// Erro no metodo: " + getNomeMetodoAtual() + " - " + e.getMessage());
                e.printStackTrace();
            }
        }
        return obj;
        //</editor-fold>
    }

    /**
     * Arruma setta os campos Data em StData
     *
     * @param obj
     * @param rb
     * @return
     * @author Luan l domingues
     */
    static public Object converteDateToStDatas(Object obj, NxResourceBundle rb) {
        //<editor-fold defaultstate="collapsed" desc="Metodo arrumaStDatas">
        if (obj != null) {
            Class classe = obj.getClass();
            try {
                List<String> listaNomesDatas = getListaNomeDatas(obj);
                //percorre as listas de nomes pega anteriormente
                for (String nomeData : listaNomesDatas) {
                    try {
                        //pega os nomes dos atributos
                        String nomeStData = "st" + primeiraMaiuscula(nomeData);
                        Getter getter = getGetter(obj, nomeData);
                        //pega o valor do atributo ata ex dataAux1
                        Date conteudoData = (Date) getter.get(obj);
                        //verifica se é nullo
                        if (conteudoData != null) {
                            Setter setter = getSetter(obj, nomeStData);
                            setter.set(obj, dateToString(conteudoData, rb.getString("format_date")), null);
                        }
                    } catch (Exception e) {
                        System.err.println("/// Erro no Campo: " + nomeData + " da classe: " + classe.getName());
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                System.err.println("/// Erro no metodo: " + getNomeMetodoAtual() + " - " + e.getMessage());
                e.printStackTrace();
            }
        }
        return obj;
        //</editor-fold>
    }

    static public List<String> getListaNomeDatas(Object obj) {
        //<editor-fold defaultstate="collapsed" desc="Metodo getListaNomeDatas">
        List<String> listaNomesDatas = new ArrayList<>();
        Class classe = obj.getClass();
        Class superClass = !classe.getSuperclass().equals(Object.class) ? classe.getSuperclass() : null;
        try {
            if (superClass != null) {
                try {
                    if (superClass.getDeclaredFields() != null && superClass.getDeclaredFields().length > 0) {
                        for (Field field : superClass.getDeclaredFields()) {
                            try {
                                if (field.isAnnotationPresent(Temporal.class)) {
                                    listaNomesDatas.add(field.getName());
                                }
                            } catch (SecurityException e) {
                                System.err.println("/// Erro ao recuperar o Field: " + field.getName() + " da superClass: " + superClass.getName());
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (SecurityException e) {
                    System.err.println("/// Erro ao acessar a superClass: " + superClass.getName());
                    e.printStackTrace();
                }
            }
            try {
                if (classe.getDeclaredFields() != null && classe.getDeclaredFields().length > 0) {
                    for (Field field : classe.getDeclaredFields()) {
                        try {
                            if (field.isAnnotationPresent(Temporal.class)) {
                                listaNomesDatas.add(field.getName());
                            }
                        } catch (SecurityException e) {
                            System.err.println("/// Erro ao recuperar o Field: " + field.getName() + " da classe: " + classe.getName());
                            e.printStackTrace();
                        }
                    }
                }

            } catch (SecurityException e) {
                System.err.println("/// Erro ao acessar a classe: " + classe.getName());
                e.printStackTrace();
            }
        } catch (Exception e) {
            System.err.println("/// Erro no metodo: " + getNomeMetodoAtual() + " - " + e.getMessage());
            e.printStackTrace();
        }
        return listaNomesDatas;
        //</editor-fold>
    }

    /**
     * Cria o proximo nomero da sequencia, Exemplo<br>
     * <p>
     * codigo = Cid000001 <br>
     * formatacaoCodigo = 6 <br>
     * prefixo = Cid <br>
     * sufixo = '' <br>
     * <p>
     * ira separar os digitos do codigo adicionar 1 e concatenar com o sufixo e
     * o prefixo
     *
     * @param codigo           codigo atual
     * @param formatacaoCodigo quantidade de zeros que o sistema deve preencher
     * @param prefixo          prefixo do codigo
     * @param sufixo           sufixo do codigo
     * @return
     * @author Luan l domingues
     */
    static public String proximoSequencia(String codigo, int formatacaoCodigo, String prefixo, String sufixo) {
        String proximo = "";
        //Separa os inteiros da string
        String digitos = "";
        //caso o codigo atual for null
        //cria o digito como -1 para na frente ser 0
        //caso contrario pega o codigo atual
        if (codigo == null) {
            digitos = "-1";
        } else {
            char[] letras = codigo.toCharArray();
            for (char letra : letras) {
                if (Character.isDigit(letra)) {
                    digitos += letra;
                }
            }
        }
        Integer proximoNumero = Integer.parseInt(digitos) + 1;

        //SEPARA O NUMEROP DE formatacaoCodigo CASAS
        String proximoCodigo = String.format("%0" + formatacaoCodigo + "d", proximoNumero);
        //adiciona o prefixo
        if (prefixo != null) {
            proximo = prefixo;
        }
        proximo += proximoCodigo;
        //adiciona o sufixo
        if (sufixo != null) {
            proximo += sufixo;
        }
        return proximo;
    }

    /**
     * <pre>
     * Converte uma String em Data quando e utilizado o JSON do C# para o JAVA
     * ,como o json não consegue converter para data tem que passar como string
     * ,então foi criado este metodo para montar a data pela string do DateTime
     *
     * <b>Author</b> : Jerry Adriano
     * <b>Data</b>   : 09/08/2013
     * <b>Param</b>  : String date
     * <b>Return</b> : Date
     * </pre>
     *
     * @param date
     * @return
     */
    public static Date convertJsonDateToDate(String date) {
        try {
            if (date != null) {
                //CONVERSÃO DE DATE EM STRING
                //TIME COMO VEM DO json "/Date(1992-07-16T00:00:00-03:00)/ ou 1376062569595 ou 1992-07-16"
                //TIME DO JAVA               1376062569595
                Date data = new Date();
                if (date.contains("-")) {
                    String[] dataPart = date.split("-");
                    String day = dataPart[2].split("T")[0];
                    String[] horPart = null;
                    if (dataPart[2].contains("T")) {
                        day = dataPart[2].split("T")[0];
                        horPart = dataPart[2].split("T")[1].split(":");
                        if (horPart[2].contains("+")) {
                            //noinspection MalformedRegex
                            horPart[2] = horPart[2].split("\\+")[0];
                        }
                    } else {
                        day = dataPart[2];
                    }
                    GregorianCalendar gregorianCalendar = new GregorianCalendar();
                    gregorianCalendar.set(GregorianCalendar.YEAR, new Integer(dataPart[0]));
                    gregorianCalendar.set(GregorianCalendar.MONTH, new Integer(dataPart[1]) - 1);
                    gregorianCalendar.set(GregorianCalendar.DAY_OF_MONTH, new Integer(day));
                    if (horPart != null) {
                        if (horPart[2].contains(".")) {
                            horPart[2] = horPart[2].split("\\.")[0];
                        }
                        gregorianCalendar.set(GregorianCalendar.HOUR_OF_DAY, new Integer(horPart[0]));
                        gregorianCalendar.set(GregorianCalendar.MINUTE, new Integer(horPart[1]));
                        gregorianCalendar.set(GregorianCalendar.SECOND, new Integer(horPart[2]));
                    }
                    data.setTime(gregorianCalendar.getTimeInMillis());
                } else {
                    try {
                        data.setTime(Long.parseLong(date));
                    } catch (NumberFormatException e) {
                        data = parseData(date);
                    }
                }
                return data;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date getCurrentDateBrasil() {
        return new Date();
        //return getCurrentDate("America/Sao_Paulo");
    }

    public static Date getCurrentDate(String timeZone) {
        DateTime dateTime = DateTime.now(DateTimeZone.forID(timeZone));
        Date date = dateTime.toDate();
        return date;
    }

    public static String gerarUsuarioAuditoria(Object usuario) {
        String usuarioAuditoriaJson = null;

        UsuarioAuditoriaVo usuarioAuditoriaVo = Util.convertTo(usuario, UsuarioAuditoriaVo.class);

        Gson gson = GsonAdapter.GsonBuilder();
        usuarioAuditoriaJson = gson.toJson(usuarioAuditoriaVo);

        return usuarioAuditoriaJson;
    }

    public static <T> T convertTo(Object obj, Class<T> classe) {
        T vo = modelMapper.map(obj, classe);
        //vo = mapperIn.readValue(tipoViatura, ModeloViaturaVO.class);
        return classe.cast(vo);
    }
}
