package br.com.util;
public class AnexoEmail {

    public static final String PDF = "application/pdf";
    public static final String DOC = "application/doc";
    public static final String DOCX = "application/docx";
    public static final String PNG = "application/png";
    public static final String XLS= "application/xls";
    public static final String XLSX= "application/xlsx";
    public static final String PPT= "application/ppt";
    public static final String PPS= "application/pps";
    public static final String JPG= "application/jpg";
    public static final String TXT= "application/txt";

    private String nome;
    private String tipo;
    private byte[] file;

    public AnexoEmail() {

    }

    public AnexoEmail(String nome, String tipo, byte[] file) {
        this.nome = nome;
        this.tipo = tipo;
        this.file = file;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }


}
