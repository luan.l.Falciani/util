/*
 * NextAge License
 * Copyright 2015 - Nextage
 * 
 */
package br.com.util.json;

import br.com.util.json.adapter.DateTypeAdapter;
import br.com.util.json.adapter.IntegerTypeAdapter;
import br.com.util.json.adapter.XMLGregorianCalendarConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Date;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Luan L F Domingues - <b>l.domingues@nextage.com.br<b>
 */
public class GsonAdapter {

    public static Gson GsonBuilder() {
        return new GsonBuilder()
                .registerTypeAdapter(Integer.class, new IntegerTypeAdapter())
                .registerTypeAdapter(int.class, new IntegerTypeAdapter())
                .registerTypeAdapter(XMLGregorianCalendar.class, new XMLGregorianCalendarConverter())
                .registerTypeAdapter(Date.class, new DateTypeAdapter())
                .create();
    }
}
