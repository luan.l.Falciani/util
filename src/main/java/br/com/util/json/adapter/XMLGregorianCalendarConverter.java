/*
 * NextAge License
 * Copyright 2015 - Nextage
 * 
 */
package br.com.util.json.adapter;

import br.com.util.Util;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.Date;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Luan L F Domingues - <b>l.domingues@nextage.com.br<b>
 *
 * Classe responsavel em transformar os valores dos fields
 * XMLGregorianCalendarConverter
 */
public class XMLGregorianCalendarConverter implements JsonSerializer<XMLGregorianCalendar>, JsonDeserializer<XMLGregorianCalendar> {

    @Override
    public JsonElement serialize(XMLGregorianCalendar obj, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(obj.toXMLFormat());
    }

    @Override
    public XMLGregorianCalendar deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        try {
            XMLGregorianCalendar xmlGregCalendar = null;
            if (jsonElement != null) {
                if (jsonElement.isJsonObject()) {
                    JsonObject obj = jsonElement.getAsJsonObject();
                    xmlGregCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(
                            obj.get("year").getAsInt(),
                            obj.get("month").getAsInt(),
                            obj.get("day").getAsInt(),
                            obj.get("hour").getAsInt(),
                            obj.get("minute").getAsInt(),
                            obj.get("second").getAsInt(),
                            0,
                            obj.get("timezone").getAsInt());
                } else if (jsonElement.isJsonPrimitive()) {
                    JsonPrimitive primitivo = jsonElement.getAsJsonPrimitive();
                    if (primitivo.isString()) {
                        Date date = Util.convertJsonDateToDate(primitivo.getAsString());
                        xmlGregCalendar = Util.dateToXMLGregorianCalendar(date);
                    }
                }
            }
            return xmlGregCalendar;
        } catch (DatatypeConfigurationException e) {
            System.err.println("Erro ao deserializar o XMLGregorianCalendar: " + e.getMessage());
            return null;
        }
    }
}
