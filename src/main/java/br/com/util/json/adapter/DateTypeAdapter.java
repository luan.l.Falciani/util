/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.util.json.adapter;

import br.com.util.Util;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.Date;

/**
 *
 * @author Luan L F Domingues - <b>l.domingues@nextage.com.br<b>
 *
 * Classe responsavel em transformar os valores dos fields Date
 */
public class DateTypeAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {

    @Override
    public JsonElement serialize(Date data, Type type, JsonSerializationContext jsonSerializationContext) {
        return data == null ? null : new JsonPrimitive(data.getTime());
    }

    @Override
    public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        try {
            Date data = null;
            if (jsonElement != null && jsonElement.isJsonPrimitive()) {
                JsonPrimitive primitive = jsonElement.getAsJsonPrimitive();
                if (primitive.isNumber()) {
                    data = new Date(primitive.getAsLong());
                }
                if (primitive.isString()) {
                    data = Util.convertJsonDateToDate(primitive.getAsString());
                }
            }
            return data;
        } catch (Exception e) {
            System.err.println("Erro ao deserializar o Date: " + e.getMessage());
            return null;
        }
    }
}
