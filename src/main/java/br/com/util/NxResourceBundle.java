package br.com.util;

import java.util.Enumeration;
import java.util.ResourceBundle;


public class NxResourceBundle {
    
    // atributo
    private ResourceBundle resourceBundle = null;
    
    // construtos
    public NxResourceBundle(String baseName) {
        if (baseName != null && !baseName.isEmpty()) {
            this.resourceBundle = ResourceBundle.getBundle(baseName);
        }
    }
    
    /**
     * Método retorna uma string caso exista a key no ResourceBundle
     * @param key
     * @return 
     */
    public String getString(String key) {
        if (this.resourceBundle != null) {
            if (key != null) {
                if (!this.resourceBundle.containsKey(key)) {
                } else {
                    return this.resourceBundle.getString(key);
                }
            }
            return ("Valor não configurado: " + key);
        }
        return null;
    }
    
    /**
     * Método retorna um object caso exista a key no ResourceBundle 
     * @param key
     * @return 
     */
    public Object getObject(String key) {
        if (this.resourceBundle != null) {
            if (key != null) {
                if (this.resourceBundle.containsKey(key))
                    return this.resourceBundle.getObject(key);
            }
            return ("Valor não configurado: " + key);
        }
        return null;
    }
    
    /**
     * Método retorna um Array de valor caso exista a key no ResourceBundle
     * @param key
     * @return 
     */
    public String[] getStringArray(String key) {
        if (this.resourceBundle != null) {
            if (key != null) {
                if (this.resourceBundle.containsKey(key))
                    return this.resourceBundle.getStringArray(key);
            }
            return (new String[]{("Valor não configurado: " + key)});
        }
        return null;
    }

    /**
     * Método retorna as keys do ResourceBundle
     * @return 
     */
    public Enumeration<String> getKeys() {
        if (this.resourceBundle != null)
            return this.resourceBundle.getKeys();
        return null;
    }
}
