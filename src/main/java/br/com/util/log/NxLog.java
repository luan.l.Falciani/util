package br.com.util.log;

import java.util.Date;

public class NxLog {

    public static int MAX_LENGHT = 4000;

    public static void geraNxLog(String sistema, String tipo, String tabela, Integer registroId, String registroDescricao, String login, String nome, Date dataLog, String caminhoNxLog) {
        try {
            if (caminhoNxLog != null && !caminhoNxLog.equals("")) {
                NxLogThread nxLogRunnable = new NxLogThread(sistema, tipo, tabela, registroId, registroDescricao, login, nome, dataLog, caminhoNxLog, null);
                Thread nxLog = new Thread(nxLogRunnable);
                nxLog.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void geraNxLog(String sistema, String tipo, String tabela, Integer registroId, String registroDescricao, String login, String nome, Date dataLog, String caminhoNxLog, String empresa) {
        try {
            if (caminhoNxLog != null && !caminhoNxLog.equals("")) {
                NxLogThread nxLogRunnable = new NxLogThread(sistema, tipo, tabela, registroId, registroDescricao, login, nome, dataLog, caminhoNxLog, empresa);
                Thread nxLog = new Thread(nxLogRunnable);
                nxLog.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
