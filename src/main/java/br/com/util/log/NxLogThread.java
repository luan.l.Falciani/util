package br.com.util.log;

import flex.messaging.io.amf.client.AMFConnection;
import flex.messaging.io.amf.client.exceptions.ClientStatusException;
import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import java.util.Date;

public class NxLogThread implements Runnable {

    String sistema;
    String tipo;
    String tabela;
    Integer registroId;
    String registroDescricao;
    String login;
    String nome;
    Date dataLog;
    String caminhoNxLog;
    String empresa;

    /**
     * Construtor da classe.
     *
     * sistema: Qual sistema que gerou log. tipo: Tipo de operação
     * SQL(INSERT,DELET,UPDATE). tabela: Tabela alterada. registroId: ID do
     * registo alterado. registroDescricao: informações sobre o registo
     * alterado. login: Login do usuário que realizou a operação. nome: Nome do
     * usuário que realizou a operação. dataLog: Data e hora que foi realizada a
     * operação.
     *
     *
     * @param sistema
     * @param tipo
     * @param tabela
     * @param registroId
     * @param registroDescricao
     * @param login
     * @param nome
     * @param dataLog
     * @param caminhoNxLog
     * @param empresa
     */
    public NxLogThread(String sistema, String tipo, String tabela, Integer registroId, String registroDescricao, String login, String nome, Date dataLog, String caminhoNxLog, String empresa) {
        this.sistema = sistema;
        this.tipo = tipo;
        this.tabela = tabela;
        this.registroId = registroId;
        this.registroDescricao = registroDescricao;
        this.login = login;
        this.nome = nome;
        this.dataLog = dataLog;
        this.caminhoNxLog = caminhoNxLog;
        this.empresa = empresa;
    }

    public Date getDataLog() {
        return dataLog;
    }

    public void setDataLog(Date dataLog) {
        this.dataLog = dataLog;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCaminhoNxLog() {
        return caminhoNxLog;
    }

    public void setCaminhoNxLog(String caminhoNxLog) {
        this.caminhoNxLog = caminhoNxLog;
    }

    public String getRegistroDescricao() {
        return registroDescricao;
    }

    public void setRegistroDescricao(String registroDescricao) {
        this.registroDescricao = registroDescricao;
    }

    public Integer getRegistroId() {
        return registroId;
    }

    public void setRegistroId(Integer registroId) {
        this.registroId = registroId;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getTabela() {
        return tabela;
    }

    public void setTabela(String tabela) {
        this.tabela = tabela;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    @Override
    public void run() {
        //Cria conection com o sistema de authentication.
        AMFConnection amfConnection = new AMFConnection();
        try {
            if (caminhoNxLog != null && !caminhoNxLog.equals("")) {
                amfConnection.connect(caminhoNxLog);
                amfConnection.call("nxLogService.saveNxLog", sistema, tipo, tabela, registroId, registroDescricao, login, nome, dataLog, empresa);
                amfConnection.close();
            }
        } catch (ClientStatusException | ServerStatusException e) {
            e.printStackTrace();
        }
    }

}
