package br.com.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

public class Email {

    /**
     *
     * <p>
     * Método que realiza a chamada de envia de email de acordo com a chave
     * escolhida para o mesmo na configuração, sendo autenticado ou sem
     * autenticação.
     *
     * <p>
     * Caso ocorra alguma exceção a mesma será lançada para tratamento no método
     * em que foi chamado o envio de email.
     *
     * @param enderecosEmail
     * @param tituloEmail
     * @param mensagemEmail
     * @param anexos
     * @param emailHost
     * @param emailPorta
     * @param emailOrigem
     * @param emailUser
     * @param emailPasswd
     * @param emailChave
     * @throws AddressException
     * @throws MessagingException
     */
    public static void enviaEmail(String enderecosEmail, String tituloEmail,
            String mensagemEmail, List<AnexoEmail> anexos,
            String emailHost, String emailPorta, String emailOrigem,
            String emailUser, String emailPasswd, String emailChave) throws AddressException, MessagingException {

        if (emailChave != null) {
            //Verificasse pela chave qual o método de envio de email a ser chamado.
            switch (emailChave) {
                case "NA":
                    //Envia email sem autenticação.
                    enviaEmailSemAutenticacao(enderecosEmail, tituloEmail, mensagemEmail, anexos, emailHost, emailPorta, emailOrigem);
                    break;
                case "A":
                    //envia email com autenticação SSL.
                    enviaEmailAutenticadoSSL(enderecosEmail, tituloEmail, mensagemEmail, anexos, emailHost, emailPorta, emailOrigem, emailUser, emailPasswd);
                    break;
            }
        }

    }

    /**
     * <p>
     * Método que realiza a chamada de envia de email de acordo com a chave
     * escolhida para o mesmo na configuração, sendo autenticado ou sem
     * autenticação.
     *
     * <p>
     * Caso ocorra alguma exceção a mesma será lançada para tratamento no método
     * em que foi chamado o envio de email.
     *
     * @param enderecosEmail
     * @param enderecosEmailCco
     * @param tituloEmail
     * @param mensagemEmail
     * @param anexos
     * @param emailHost
     * @param emailPorta
     * @param emailOrigem
     * @param emailUser
     * @param emailPasswd
     * @param emailChave
     * @param emailHtml
     * @throws AddressException
     * @throws MessagingException
     */
    public static void enviaEmail(String enderecosEmail, String enderecosEmailCco, String tituloEmail,
            String mensagemEmail, List<AnexoEmail> anexos,
            String emailHost, String emailPorta, String emailOrigem,
            String emailUser, String emailPasswd, String emailChave, String emailHtml) throws AddressException, MessagingException {
        //Verificasse pela chave qual o método de envio de email a ser chamado.
        switch (emailChave) {
            case "NA":
                //Envia email sem autenticação.
                enviaEmailSemAutenticacao(enderecosEmail, enderecosEmailCco, tituloEmail, mensagemEmail, anexos, emailHost, emailPorta, emailOrigem, emailHtml);
                break;
            case "A":
                //envia email com autenticação SSL.
                enviaEmailAutenticadoSSL(enderecosEmail, enderecosEmailCco, tituloEmail, mensagemEmail, anexos, emailHost, emailPorta, emailOrigem, emailUser, emailPasswd, emailHtml);
                break;
        }

    }

    /**
     * <PRE>
     * Método que realiza a chamada de envia de email de acordo com a chave
     * escolhida para o mesmo na configuração, sendo autenticado ou sem autenticação.
     *
     * Caso Ocorra alguma erro ao enviar email a Thread irá tentar enviar o email novamente,
     * esse número de vezes pode ser configurado assim como o tempo de espera entre uma tentativa e outra.
     *
     *</PRE>
     *
     * @param enderecosEmail
     * @param tituloEmail
     * @param mensagemEmail
     * @param anexos
     * @param emailHost
     * @param emailPorta
     * @param emailOrigem
     * @param emailUser
     * @param emailPasswd
     * @param emailChave
     * @param emailHtml
     * @param enderecosEmailCco
     * @return
     * @throws AddressException
     * @throws MessagingException
     */
    public static int enviaEmailPorThread(String enderecosEmail, String tituloEmail,
            String mensagemEmail, List<AnexoEmail> anexos,
            String emailHost, String emailPorta, String emailOrigem,
            String emailUser, String emailPasswd, String emailChave, String emailHtml,
            String enderecosEmailCco) throws AddressException, MessagingException {

        int tipoValidacao = validaEmails(enderecosEmail);

        //Executa se alguns ou todos os email forem válidos
        if (tipoValidacao == 3 || tipoValidacao == 4) {
            ThreadEnviaEmail threadEnviaEmail = new ThreadEnviaEmail();
            threadEnviaEmail.setAnexos(anexos);
            threadEnviaEmail.setEnderecosEmail(enderecosEmail);
            threadEnviaEmail.setTituloEmail(tituloEmail);
            threadEnviaEmail.setMensagemEmail(mensagemEmail);
            threadEnviaEmail.setEmailHost(emailHost);
            threadEnviaEmail.setEmailPorta(emailPorta);
            threadEnviaEmail.setEmailOrigem(emailOrigem);
            threadEnviaEmail.setEmailUser(emailUser);
            threadEnviaEmail.setEmailPasswd(emailPasswd);
            threadEnviaEmail.setEmailChave(emailChave);
            threadEnviaEmail.setEmailHtml(emailHtml);
            threadEnviaEmail.setEnderecosEmailCco(enderecosEmailCco);
            threadEnviaEmail.start();
        }

        return tipoValidacao;
    }

    /**
     * <PRE>
     * </PRE>
     *
     * <p>
     * Envia o email sem a necessidade de autenticação, utilizando-se apenas do
     * host, porta email de origem.
     *
     * @param enderecosEmail
     * @param tituloEmail
     * @param mensagemEmail
     * @param anexos
     * @param emailHost
     * @param emailPorta
     * @param emailOrigem
     * @throws AddressException
     * @throws MessagingException
     */
    private static void enviaEmailSemAutenticacao(String enderecosEmail, String tituloEmail,
            String mensagemEmail, List<AnexoEmail> anexos,
            String emailHost, String emailPorta, String emailOrigem) throws AddressException, MessagingException {

        Properties p = new Properties();
        List<Address> listaDestinatarios = new ArrayList<>();

        p.put("mail.host", emailHost);

        if (emailPorta != null & !emailPorta.equals("")) {
            p.put("mail.smtp.port", emailPorta);
        }

        p.put("mail.debug", "false");
        p.put("mail.smtp.debug", "false");
        p.put("mail.mime.charset", "ISO-8859-1");
        //Time out de 10 Segundos setados em Milissegundos.
        p.put("mail.smtp.timeout", "10000");

        Session session = Session.getInstance(p, null);
        MimeMessage msg = new MimeMessage(session);

        if (enderecosEmail.length() > 0) {
            //Separa os emails em um vetor.
            String[] listaEnderecos = enderecosEmail.split("\\;");

            if (listaEnderecos.length > 0) {

                    //Cria destinatários e adiciona em uma lista,
                //de acordo com a string enderecosEmail do parametro
                //que contém os destinos separados por ';' ponto e virgula.
                for (String emailDestino : listaEnderecos) {
                    listaDestinatarios.add(new InternetAddress(emailDestino));
                }

                //De
                msg.setFrom(new InternetAddress(emailOrigem));

                //Para
                Address[] arrayDestinatarios = new Address[listaDestinatarios.size()];
                listaDestinatarios.toArray(arrayDestinatarios);
                msg.setRecipients(Message.RecipientType.BCC, arrayDestinatarios);

                    // nao esqueca da data!
                // ou ira 31/12/1969 !!!
                msg.setSentDate(new Date());

                //titulo email
                msg.setSubject(tituloEmail);

                //Mensagem email.
                msg.setText(mensagemEmail);

                //Adciona anexos a mensagem se houver.
                if (anexos != null && anexos.size() > 0) {
                    msg = adicionaAnexos(msg, mensagemEmail, anexos);
                }

                //Tenta enviar email, caso ocorra erro, cairá no catch.
                Transport.send(msg);

            }
        }
    }

    /**
     * <p>
     * Envia o email sem a necessidade de autenticação, utilizando-se apenas do
     * host, porta email de origem.
     *
     * @param enderecosEmail
     * @param enderecosEmailCco
     * @param tituloEmail
     * @param mensagemEmail
     * @param anexos
     * @param emailHost
     * @param emailPorta
     * @param emailOrigem
     * @param emailHtml
     * @throws AddressException
     * @throws MessagingException
     */
    private static void enviaEmailSemAutenticacao(String enderecosEmail, String enderecosEmailCco, String tituloEmail,
            String mensagemEmail, List<AnexoEmail> anexos,
            String emailHost, String emailPorta, String emailOrigem, String emailHtml) throws AddressException, MessagingException {

        Properties p = new Properties();
        List<Address> listaDestinatarios = new ArrayList<>();
        List<Address> listaDestinatariosCco = new ArrayList<>();

        p.put("mail.host", emailHost);

        if (emailPorta != null & !emailPorta.equals("")) {
            p.put("mail.smtp.port", emailPorta);
        }

        p.put("mail.debug", "false");
        p.put("mail.smtp.debug", "false");
        p.put("mail.mime.charset", "ISO-8859-1");
        //Time out de 10 Segundos setados em Milissegundos.
        p.put("mail.smtp.timeout", "10000");

        Session session = Session.getInstance(p, null);
        MimeMessage msg = new MimeMessage(session);

        if ((enderecosEmail != null && enderecosEmail.length() > 0) || (enderecosEmailCco != null && enderecosEmailCco.length() > 0)) {
            //Separa os emails em um vetor.            
            String[] listaEnderecos = null;
            String[] listaEnderecosCco = null;
            if (enderecosEmail != null && enderecosEmail.length() > 0) {
                listaEnderecos = enderecosEmail.split("\\;");
            }
            if (enderecosEmailCco != null && enderecosEmailCco.length() > 0) {
                listaEnderecosCco = enderecosEmailCco.split("\\;");
            }

            if ((listaEnderecos != null && listaEnderecos.length > 0) || (listaEnderecosCco != null && listaEnderecosCco.length > 0)) {
                //de
                msg.setFrom(new InternetAddress(emailOrigem));

                //destinatáiros
                if (enderecosEmail != null && enderecosEmail.length() > 0) {
                    if (listaEnderecos != null && listaEnderecos.length > 0) {

                        for (String emailDestino : listaEnderecos) {
                            // "de" e "para"!!
                            listaDestinatarios.add(new InternetAddress(emailDestino));
                        }

                        //destinatáiros
                        Address[] arrayDestinatarios = new Address[listaDestinatarios.size()];
                        listaDestinatarios.toArray(arrayDestinatarios);
                        msg.setRecipients(Message.RecipientType.TO, arrayDestinatarios);
                    }
                }

                //destinatáiros(cópia oculta)
                if (enderecosEmailCco != null && enderecosEmailCco.length() > 0) {
                    if (listaEnderecosCco != null && listaEnderecosCco.length > 0) {

                        for (String emailDestino : listaEnderecosCco) {
                            listaDestinatariosCco.add(new InternetAddress(emailDestino));
                        }

                        Address[] arrayDestinatariosCco = new Address[listaDestinatariosCco.size()];
                        listaDestinatariosCco.toArray(arrayDestinatariosCco);
                        msg.setRecipients(Message.RecipientType.BCC, arrayDestinatariosCco);
                    }
                }

                // nao esqueca da data!
                // ou ira 31/12/1969 !!!
                msg.setSentDate(new Date());

                //titulo email
                msg.setSubject(tituloEmail);

                //Mensagem email.
                msg.setText(mensagemEmail);

                if (emailHtml != null && !emailHtml.equals("")) {
                    //Addiciona Mensagem Html na parte 2 no contexto
                    //Adiciona contexto geral
                    Multipart mp = new MimeMultipart();
                    MimeBodyPart textPart = new MimeBodyPart();
                    textPart.setContent(mensagemEmail.replaceAll("\\n", "<BR/>") + emailHtml.replaceAll("\\n", "<BR/>"), "text/html; charset=UTF-8");
                    mp.addBodyPart(textPart);
                    msg.setContent(mp);
                }

                //Adciona anexos a mensagem se houver.
                if (anexos != null && anexos.size() > 0) {
                    msg = adicionaAnexos(msg, mensagemEmail, anexos);
                }

                //Tenta enviar email, caso ocorra erro, cairá no catch.
                Transport.send(msg);

            }
        }
    }

    /**
     *
     * <p>
     * Envia o email sem a necessidade de autenticação, utilizando-se apenas do
     * host, porta email de origem.
     *
     * @param enderecosEmail
     * @param tituloEmail
     * @param mensagemEmail
     * @param anexos
     * @param emailHost
     * @param emailPorta
     * @param emailOrigem
     * @param emailUser
     * @param emailPasswd
     * @throws AddressException
     * @throws MessagingException
     */
    private static void enviaEmailAutenticadoSSL(String enderecosEmail, String tituloEmail,
            String mensagemEmail, List<AnexoEmail> anexos,
            String emailHost, String emailPorta, String emailOrigem,
            String emailUser, String emailPasswd) throws AddressException, MessagingException {

        Properties p = new Properties();
        List<Address> listaDestinatarios = new ArrayList<>();

        p.put("mail.smtp.host", emailHost);
        p.put("mail.smtp.auth", "true");
        p.put("mail.debug", "false");
        p.put("mail.smtp.debug", "false");
        p.put("mail.mime.charset", "ISO-8859-1");
        p.put("mail.smtp.port", emailPorta);
        p.put("mail.smtp.starttls.enable", "true");
        p.put("mail.smtp.socketFactory.port", emailPorta);
        p.put("mail.smtp.socketFactory.fallback", "false");
        p.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        //Time out de 10 Segundos setados em Milissegundos.
        p.put("mail.smtp.timeout", "10000");

        Session session = Session.getInstance(p, null);
        session.setDebug(false);
        MimeMessage msg = new MimeMessage(session);

        if (enderecosEmail.length() > 0) {
            //Separa os emails em um vetor.
            String[] listaEnderecos = enderecosEmail.split("\\;");

            if (listaEnderecos.length > 0) {
                //de
                msg.setFrom(new InternetAddress(emailOrigem));

                for (String emailDestino : listaEnderecos) {
                    // "de" e "para"!!
                    listaDestinatarios.add(new InternetAddress(emailDestino));
                }

                //destinatáiros( cópia oculta )
                Address[] arrayDestinatarios = new Address[listaDestinatarios.size()];
                listaDestinatarios.toArray(arrayDestinatarios);
                msg.setRecipients(Message.RecipientType.BCC, arrayDestinatarios);

                // nao esqueca da data!
                // ou ira 31/12/1969 !!!
                msg.setSentDate(new Date());

                //titulo email
                msg.setSubject(tituloEmail);

                //Mensagem email.
                msg.setText(mensagemEmail);

                //Adciona anexos a mensagem se houver.
                if (anexos != null && anexos.size() > 0) {
                    msg = adicionaAnexos(msg, mensagemEmail, anexos);
                }

                //Envia o e-mail.
                Transport tr = session.getTransport("smtp");
                tr.connect(emailHost, emailUser, emailPasswd);
                msg.saveChanges();// don't forget this
                tr.sendMessage(msg, msg.getAllRecipients());
                tr.close();
            }
        }
    }

    /**
     * <p>
     * Envia o email sem a necessidade de autenticação, utilizando-se apenas do
     * host, porta email de origem.
     *
     * @param enderecosEmail
     * @param enderecosEmailCco
     * @param tituloEmail
     * @param mensagemEmail
     * @param anexos
     * @param emailHost
     * @param emailPorta
     * @param emailOrigem
     * @param emailUser
     * @param emailPasswd
     * @param emailHtml
     * @throws AddressException
     * @throws MessagingException
     */
    private static void enviaEmailAutenticadoSSL(String enderecosEmail, String enderecosEmailCco, String tituloEmail,
            String mensagemEmail, List<AnexoEmail> anexos,
            String emailHost, String emailPorta, String emailOrigem,
            String emailUser, String emailPasswd, String emailHtml) throws AddressException, MessagingException {

        Properties p = new Properties();
        List<Address> listaDestinatarios = new ArrayList<>();
        List<Address> listaDestinatariosCco = new ArrayList<>();

        p.put("mail.smtp.host", emailHost);
        p.put("mail.smtp.auth", "true");
        p.put("mail.debug", "false");
        p.put("mail.smtp.debug", "false");
        p.put("mail.mime.charset", "ISO-8859-1");
        p.put("mail.smtp.port", emailPorta);
        p.put("mail.smtp.starttls.enable", "true");
        p.put("mail.smtp.socketFactory.port", emailPorta);
        p.put("mail.smtp.socketFactory.fallback", "false");
        p.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        //Time out de 10 Segundos setados em Milissegundos.
        p.put("mail.smtp.timeout", "10000");

        Session session = Session.getInstance(p, null);
        session.setDebug(false);
        MimeMessage msg = new MimeMessage(session);

        if ((enderecosEmail != null && enderecosEmail.length() > 0) || (enderecosEmailCco != null && enderecosEmailCco.length() > 0)) {
            //Separa os emails em um vetor.
            String[] listaEnderecos = null;
            String[] listaEnderecosCco = null;
            if (enderecosEmail != null && enderecosEmail.length() > 0) {
                listaEnderecos = enderecosEmail.split("\\;");
            }
            if (enderecosEmailCco != null && enderecosEmailCco.length() > 0) {
                listaEnderecosCco = enderecosEmailCco.split("\\;");
            }

            if ((listaEnderecos != null && listaEnderecos.length > 0) || (listaEnderecosCco != null && listaEnderecosCco.length > 0)) {
                //de
                msg.setFrom(new InternetAddress(emailOrigem));

                //destinatáiros
                if (enderecosEmail != null && enderecosEmail.length() > 0) {
                    if (listaEnderecos != null && listaEnderecos.length > 0) {

                        for (String emailDestino : listaEnderecos) {
                            // "de" e "para"!!
                            listaDestinatarios.add(new InternetAddress(emailDestino));
                        }

                        //destinatáiros
                        Address[] arrayDestinatarios = new Address[listaDestinatarios.size()];
                        listaDestinatarios.toArray(arrayDestinatarios);
                        msg.setRecipients(Message.RecipientType.TO, arrayDestinatarios);
                    }
                }

                //destinatáiros(cópia oculta)
                if (enderecosEmailCco != null && enderecosEmailCco.length() > 0) {
                    if (listaEnderecosCco != null && listaEnderecosCco.length > 0) {

                        for (String emailDestino : listaEnderecosCco) {
                            listaDestinatariosCco.add(new InternetAddress(emailDestino));
                        }

                        Address[] arrayDestinatariosCco = new Address[listaDestinatariosCco.size()];
                        listaDestinatariosCco.toArray(arrayDestinatariosCco);
                        msg.setRecipients(Message.RecipientType.BCC, arrayDestinatariosCco);
                    }
                }

                // nao esqueca da data!
                // ou ira 31/12/1969 !!!
                msg.setSentDate(new Date());

                //titulo email
                msg.setSubject(tituloEmail);

                //Mensagem email.
                msg.setText(mensagemEmail);

                if (emailHtml != null && !emailHtml.equals("")) {
                    //Addiciona Mensagem Html na parte 2 no contexto
                    //Adiciona contexto geral
                    Multipart mp = new MimeMultipart();
                    MimeBodyPart textPart = new MimeBodyPart();
                    textPart.setContent(mensagemEmail.replaceAll("\\n", "<BR/>") + emailHtml.replaceAll("\\n", "<BR/>"), "text/html; charset=UTF-8");
                    mp.addBodyPart(textPart);
                    msg.setContent(mp);
                }

                //Adciona anexos a mensagem se houver.
                if (anexos != null && anexos.size() > 0) {
                    msg = adicionaAnexos(msg, mensagemEmail, anexos);
                }

                //Envia o e-mail.
                Transport tr = session.getTransport("smtp");
                tr.connect(emailHost, emailUser, emailPasswd);
                msg.saveChanges();// don't forget this
                tr.sendMessage(msg, msg.getAllRecipients());
                tr.close();
            }
        }
    }

    /**
     * <p>
     * Método que faz a adição de anexos a mensagem passada como parametro,
     * retornando a mesma com os anexos.
     *
     * @param msg
     * @param mensagemEmail
     * @param anexos
     * @return
     */
    private static MimeMessage adicionaAnexos(MimeMessage msg, String mensagemEmail, List<AnexoEmail> anexos) {
        try {

            //Adiciona contexto geral
            Multipart mp = new MimeMultipart();

            //Addiciona Mensagem na parte 1 no contexto
            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setContent(mensagemEmail.replaceAll("\\n", "<BR/>"), "text/html; charset=UTF-8");
            mp.addBodyPart(textPart);

            //Adiciona Anexos
            if (anexos != null && anexos.size() > 0) {
                MimeBodyPart attachFilePart;
                DataSource ds;
                for (AnexoEmail anexo : anexos) {
                    //Cria uma nova parte com o anexo
                    attachFilePart = new MimeBodyPart();
                    ds = new ByteArrayDataSource(anexo.getFile(), anexo.getTipo());
                    attachFilePart.setDataHandler(new DataHandler(ds));
                    attachFilePart.setFileName(anexo.getNome());

                    //Adiciona o mensagem no contexto
                    mp.addBodyPart(attachFilePart);
                }
            }

            //Adiciona a MSG de Texto e ANEXOS
            msg.setContent(mp);

        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

        //Retorno a mensagem com os anexos.
        return msg;
    }

    /**
     *
     * @param email
     * @return
     */
    public static boolean validaEmail(String email) {
        boolean emailValido = false;
        if (email != null && email.length() > 0) {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            if (matcher.matches()) {
                emailValido = true;
            }
        }
        return emailValido;
    }

    /**
     * <PRE>
     *Verifica se o email é valido.
     * 1 - Não existe e-mails.
     * 2 - Todos os e-mails são inválidos.
     * 3 - Existe alguns e-mails inválidos.
     * 4 - E-mails válidos.
     *
     *
     * @param enderecosEmail
     * @return
     */
    public static int validaEmails(String enderecosEmail) {
        int contInvalido = 0;
        String emails = "";
        String[] listaEnderecos = null;
        if (enderecosEmail != null && enderecosEmail.length() > 0) {
            listaEnderecos = enderecosEmail.split("\\;");

            for (String emailDestino : listaEnderecos) {
                if (emailDestino != null && emailDestino.length() > 0) {
                    String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
                    Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
                    Matcher matcher = pattern.matcher(emailDestino);
                    if (matcher.matches()) {
                        emails = emails + emailDestino + ";";
                    } else {
                        contInvalido++;
                    }

                }
            }
        }
        if (listaEnderecos == null) {
            return 1;
        } else if (contInvalido == listaEnderecos.length) {
            return 2;
        } else if (contInvalido > 0) {
            return 3;
        } else {
            return 4;
        }
    }

}
