package br.com.util;

import br.com.persistence_2.util.HibernateUtil;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.metadata.ClassMetadata;

public class ResourceLogUtil {

    private static String className = null;
    public static final String DELETE = "delete";
    public static final String PERSISTIR = "persistir";
    private static final String INSERIR = "inclusão";
    private static final String ALTERAR = "alteração";

    /**
     * 
     * @param sLogger
     * @param aliasClasse
     * @param theClassName
     * @param userSystem
     * @param operacao
     * @param obj 
     */
    public static void provideAuditor(String sLogger, String aliasClasse, String theClassName, String userSystem, String operacao,
            final Object obj) {
        className = theClassName;

        Log logger = LogFactory.getLog(sLogger);

        if (logger.isInfoEnabled()) {
            logger.info(createMessageAuditor(aliasClasse, className, userSystem, operacao, obj));
        }
    }

    /**
     * 
     * @param aliasClasse
     * @param theClassName
     * @param userSystem
     * @param operacao
     * @param obj
     * @return 
     */
    private static String createMessageAuditor(String aliasClasse,
            String theClassName, String userSystem, String operacao, Object obj) {
        StringBuilder sb = new StringBuilder();

        try {

            if (userSystem != null && !userSystem.equals("")) {
                sb.append("Usuário: ");
                sb.append(userSystem);
                sb.append("\t");
            }

            if (aliasClasse != null && !aliasClasse.equals("")) {
                sb.append("Entidade: ");
                sb.append(aliasClasse.toUpperCase());
                sb.append("\t");
            }

            if (operacao != null && !operacao.equals("")) {
                sb.append("Operação: ");
                // Verifica se a operação é de DELETE, INCLUSÃO ou ALTERAÇÃO.
                if (operacao.equalsIgnoreCase(DELETE)) {
                    sb.append(DELETE);
                } else {
                    ClassMetadata cm = HibernateUtil.getSessionFactory().getClassMetadata(theClassName);
                    if (cm != null) {
                        Integer id = (Integer) PropertyUtils.getProperty(obj, cm.getIdentifierPropertyName());
                        if (id != null && id.intValue() > 0) {
                            // ALTERAÇÃO.
                            sb.append(ALTERAR);
                        } else {
                            // INCLUSÃO.
                            sb.append(INSERIR);
                        }
                    }
                }
                sb.append("\t");
            }

            if (obj != null) {
                Class cls = Class.forName(theClassName);
                cls.cast(obj);

                Method methods[] = cls.getDeclaredMethods();
                sb.append("DADOS: ");
                for (Method m : methods) {
                    if (m.getName().contains("get") && MethodUtils.getAccessibleMethod(m) != null) {
                        sb.append(m.getName().split("get")[1]);
                        sb.append(": ");
                        sb.append(MethodUtils.invokeMethod(obj, m.getName(), null));
                        sb.append("; ");
                    }
                }
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException | SecurityException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    /**
     * Cria mensagem de Erro que será gravada no arquivo de log
     * Usado nos catch's.
     * 
     * @param userSystem
     * @param nomeClasse
     * @param metodo
     * @param e
     * @return 
     */
    public static String createMessageError(String userSystem, String nomeClasse,
            String metodo, Exception e) {

        StringBuilder sb = new StringBuilder();

        try {

            if (userSystem != null && !userSystem.equals("")) {
                sb.append("Usuário: ");
                sb.append(userSystem);
                sb.append("\t");
            }

            if (nomeClasse != null && !nomeClasse.equals("")) {
                sb.append("Entidade: ");
                sb.append(nomeClasse.toUpperCase());
                sb.append("\t");
            }

            if (metodo != null && !metodo.equals("")) {
                sb.append("Método: ");
                sb.append(metodo);
                sb.append("\t");
            }

            if (e.getMessage() != null && !e.getMessage().equals("")) {
                sb.append("Exceção: ");
                sb.append(e.getMessage());
                sb.append("\t").append('\r').append('\n');
            }

            if (e.getStackTrace() != null && e.getStackTrace().length > 0) {
                for (StackTraceElement stackTrace : e.getStackTrace()) {
                    sb.append(stackTrace.toString());
                    sb.append(" \t").append('\r').append('\n');
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sb.toString();
    }

    /**
     * 
     * @param classe
     * @param metodo
     * @param msg
     * @return 
     */
    public static String createMessageInfo(String classe, String metodo, String msg) {
        return createMessageInfo("", classe, metodo, msg);
    }
    /**
     * 
     * @param usuario
     * @param classe
     * @param metodo
     * @param msg
     * @return 
     */
    public static String createMessageInfo(String usuario, String classe, String metodo, String msg) {
        StringBuilder sb = new StringBuilder();

        try {

            if (usuario != null && !usuario.isEmpty()) {
                sb.append("Usuário: ");
                sb.append(usuario);
                sb.append("\t");
            }
            if (classe != null && !classe.isEmpty()) {
                sb.append("Classe: ");
                sb.append(classe);
                sb.append("\t");
            }

            if (metodo != null && !metodo.isEmpty()) {
                sb.append("Metodo: ");
                sb.append(metodo.toUpperCase());
                sb.append("\t");
            }

            if (msg != null && !msg.equals("")) {
                sb.append("Mensagem: ");
                sb.append(msg);
                sb.append("\t");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    /**
     * Cria mensagem de Info que será gravada no arquivo de log
     * Usado em métodos de salvar e excluir
     * 
     * @param userSystem
     * @param classeErro
     * @param nomeClasse
     * @param metodo
     * @param obj
     * @return 
     */
    public static String createMessageInfo(String userSystem, String classeErro, String nomeClasse, String metodo, final Object obj) {

        StringBuilder sb = new StringBuilder();

        try {

            if (userSystem != null && !userSystem.equals("")) {
                sb.append("Usuário: ");
                sb.append(userSystem);
                sb.append("\t");
            }

            if (classeErro != null && !classeErro.equals("")) {
                sb.append("Classe: ");
                sb.append(classeErro.toUpperCase());
                sb.append("\t");
            }

            if (metodo != null && !metodo.equals("")) {
                sb.append("Método: ");
                sb.append(metodo);
                sb.append("\t");
            }

            if (obj != null) {
                Class cls = Class.forName(nomeClasse);
                cls.cast(obj);

                sb.append("DADOS: ");
                if (obj instanceof String) {
                    sb.append(obj);
                    sb.append("; ");
                } else {
                    Method methods[] = cls.getDeclaredMethods();
                    for (Method m : methods) {
                        if (m.getName().contains("get") && MethodUtils.getAccessibleMethod(m) != null) {
                            sb.append(m.getName().split("get")[1]);
                            sb.append("==");
                            sb.append(MethodUtils.invokeMethod(obj, m.getName(), null));
                            sb.append("; ");
                        }
                    }
                }
            }

        } catch (ClassNotFoundException | SecurityException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            ex.printStackTrace();
        }
        return sb.toString();
    }

    /**
     * Cria mensagem de Debug que será gravada no arquivo de log
     * Usado em métodos de Listar
     * 
     * @param userSystem
     * @param nomeClasse
     * @param metodo
     * @param obj
     * @param lista
     * @return 
     */
    public static String createMessageDebug(String userSystem, Class<?> nomeClasse, String metodo, final Object obj, List lista) {
        return createMessageDebug(userSystem, nomeClasse.getName(), nomeClasse.getName(), metodo, obj, lista);
    }

    public static String createMessageDebug(String userSystem, String classeErro, String nomeClasse, String metodo, final Object obj, List lista) {

        StringBuilder sb = new StringBuilder();

        try {

            if (userSystem != null && !userSystem.equals("")) {
                sb.append("Usuário: ");
                sb.append(userSystem);
                sb.append("\t");
            }

            if (classeErro != null && !classeErro.equals("")) {
                sb.append("Classe: ");
                sb.append(classeErro.toUpperCase());
                sb.append("\t");
            }

            if (metodo != null && !metodo.equals("")) {
                sb.append("Método: ");
                sb.append(metodo);
                sb.append("\t");
            }

            if (obj != null) {
                Class cls = Class.forName(nomeClasse);
                cls.cast(obj);

                Method methods[] = cls.getDeclaredMethods();
                sb.append("FILTROS: ");
                for (Method m : methods) {
                    if (m.getName().contains("get") && MethodUtils.getAccessibleMethod(m) != null) {
                        sb.append(m.getName().split("get")[1]);
                        sb.append("==");
                        sb.append(MethodUtils.invokeMethod(obj, m.getName(), null));
                        sb.append("; ");
                    }
                }
            }

            if (lista != null) {
                sb.append("Tamanho da Lista: ");
                sb.append(lista.size());
                sb.append("\t");
            } else {
                sb.append("Lista Nula! ");
                sb.append("\t");
            }

        } catch (ClassNotFoundException | SecurityException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            ex.printStackTrace();
        }
        return sb.toString();
    }

}
