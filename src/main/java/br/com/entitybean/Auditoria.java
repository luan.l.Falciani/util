package br.com.entitybean;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public class Auditoria implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String IS_EXCLUIDO = "isExcluido";
	public static final String USUARIO_INC = "usuarioInc";
	public static final String USUARIO_ALT = "usuarioAlt";
	public static final String USUARIO_DEL = "usuarioDel";
	public static final String DATA_HORA_USUARIO_INC = "dataHoraUsuarioInc";
	public static final String DATA_HORA_USUARIO_ALT = "dataHoraUsuarioAlt";
	public static final String DATA_HORA_USUARIO_DEL = "dataHoraUsuarioDel";

	@Column(name = "IS_EXCLUIDO", length = 1, nullable = false)
	private String isExcluido;

	@Column(name = "USUARIO_INC", length = 500, nullable = false)
	private String usuarioInc;

	@Column(name = "USUARIO_ALT", length = 500, nullable = false)
	private String usuarioAlt;

	@Column(name = "USUARIO_DEL", length = 500)
	private String usuarioDel;

	@Column(name = "DATA_HORA_USUARIO_INC", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHoraUsuarioInc;

	@Column(name = "DATA_HORA_USUARIO_ALT", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHoraUsuarioAlt;

	@Column(name = "DATA_HORA_USUARIO_DEL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHoraUsuarioDel;

	public String getIsExcluido() {
		return isExcluido;
	}

	public void setIsExcluido(String isExcluido) {
		this.isExcluido = isExcluido;
	}

	public String getUsuarioInc() {
		return usuarioInc;
	}

	public void setUsuarioInc(String usuarioInc) {
		this.usuarioInc = usuarioInc;
	}

	public String getUsuarioAlt() {
		return usuarioAlt;
	}

	public void setUsuarioAlt(String usuarioAlt) {
		this.usuarioAlt = usuarioAlt;
	}

	public String getUsuarioDel() {
		return usuarioDel;
	}

	public void setUsuarioDel(String usuarioDel) {
		this.usuarioDel = usuarioDel;
	}

	public Date getDataHoraUsuarioInc() {
		return dataHoraUsuarioInc;
	}

	public void setDataHoraUsuarioInc(Date dataHoraUsuarioInc) {
		this.dataHoraUsuarioInc = dataHoraUsuarioInc;
	}

	public Date getDataHoraUsuarioAlt() {
		return dataHoraUsuarioAlt;
	}

	public void setDataHoraUsuarioAlt(Date dataHoraUsuarioAlt) {
		this.dataHoraUsuarioAlt = dataHoraUsuarioAlt;
	}

	public Date getDataHoraUsuarioDel() {
		return dataHoraUsuarioDel;
	}

	public void setDataHoraUsuarioDel(Date dataHoraUsuarioDel) {
		this.dataHoraUsuarioDel = dataHoraUsuarioDel;
	}
}
