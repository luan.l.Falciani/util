/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.entitybean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author nextage
 */
@Entity
@Table(name = "TB_CONFIG_LISTENER")
public class ConfigListener implements Serializable {

    private static final long serialVersionUID = 1L;

    // Constantes com os nomes da classe
    public static final String ALIAS_CLASSE = "configListener";
    public static final String ALIAS_CLASSE_UNDERLINE = "configListener_";

    public static final String ID = "id";
    public static final String CHAVE = "chave";
    public static final String HORA = "hora";
    public static final String MINUTO = "minuto";
    public static final String FREQUENCIA_HORA = "frequenciaHora";
    public static final String FREQUENCIA_MINUTO = "frequenciaMinuto";

    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ")
    @SequenceGenerator(name = "SEQ", sequenceName = "GEN_TB_CONFIG_IMPORTACAO_ID", allocationSize = 1)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "CHAVE", length = 50)
    private String chave;
    @Basic(optional = false)
    @Column(name = "HORA")
    private Integer hora;
    @Basic(optional = false)
    @Column(name = "MINUTO")
    private Integer minuto;
    @Basic(optional = true)
    @Column(name = "FREQUENCIA_HORA")
    private Integer frequenciaHora;
    @Basic(optional = true)
    @Column(name = "FREQUENCIA_MINUTO")
    private Integer frequenciaMinuto;

    public ConfigListener() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public Integer getHora() {
        return hora;
    }

    public void setHora(Integer hora) {
        this.hora = hora;
    }

    public Integer getMinuto() {
        return minuto;
    }

    public void setMinuto(Integer minuto) {
        this.minuto = minuto;
    }

    public Integer getFrequenciaHora() {
        return frequenciaHora;
    }

    public void setFrequenciaHora(Integer frequenciaHora) {
        this.frequenciaHora = frequenciaHora;
    }

    public Integer getFrequenciaMinuto() {
        return frequenciaMinuto;
    }

    public void setFrequenciaMinuto(Integer frequenciaMinuto) {
        this.frequenciaMinuto = frequenciaMinuto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfigListener)) {
            return false;
        }
        ConfigListener other = (ConfigListener) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "br.com.rmaweb.entitybean.ConfigListener[id=" + id + "]";
    }

}
